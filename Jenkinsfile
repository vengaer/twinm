pipeline {
    agent { dockerfile true }
    environment {
        AS='yasm'
        CC='gcc'
        CFLAGS='-Werror'
        ARTIFACT_DIR='artifacts'
        TEST_DIR='tests'
    }
    stages {
        stage('Gitlab Pending') {
            steps {
                echo 'Notifying Gitlab'
                updateGitlabCommitStatus name: 'build', state: 'pending'
            }
        }
        stage('Build') {
            steps {
                echo '-- Starting build --'

                echo 'Creating artifact directory'
                sh 'mkdir -p ${ARTIFACT_DIR}'

                echo 'Build: CC=gcc AS=nasm SIMD=y default configuration'
                sh '''
                    export AS=nasm
                    export CC=gcc
                    make TARGET=${ARTIFACT_DIR}/twinm-gcc-nasm-${BUILD_NUMBER} -j$(nproc) -B
                '''

                echo 'Build: CC=clang AS=nasm SIMD=y default configuration'
                sh '''
                    export AS=nasm
                    export CC=clang
                    make TARGET=${ARTIFACT_DIR}/twinm-clang-nasm-${BUILD_NUMBER} -j$(nproc) -B
                '''

                echo 'Build: CC=gcc AS=nasm SIMD=y release configuration'
                sh '''
                    export AS=nasm
                    export CC=gcc
                    make TARGET=${ARTIFACT_DIR}/twinm-gcc-nasm-release-${BUILD_NUMBER} -j$(nproc) release -B
                '''

                echo 'Build: CC=clang AS=nasm SIMD=y release configuration'
                sh '''
                    export CC=clang
                    export AS=nasm
                    make TARGET=${ARTIFACT_DIR}/twinm-clang-nasm-release-${BUILD_NUMBER} -j$(nproc) release -B
                '''

                echo 'Build: CC=gcc AS=nasm SIMD=n default configuration'
                sh '''
                    export CC=gcc
                    export AS=nasm
                    make TARGET=${ARTIFACT_DIR}/twinm-gcc-nasm-no-simd-${BUILD_NUMBER} -j$(nproc) SIMD=n -B
                '''

                echo 'Build: CC=clang AS=nasm SIMD=n default configuration'
                sh '''
                    export CC=clang
                    export AS=nasm
                    make TARGET=${ARTIFACT_DIR}/twinm-clang-nasm-no-simd-${BUILD_NUMBER} -j$(nproc) SIMD=no -B
                '''

                echo 'Build: CC=gcc AS=yasm SIMD=y default configuration'
                sh '''
                    export AS='yasm'
                    export CC='gcc'
                    make TARGET=${ARTIFACT_DIR}/twinm-gcc-yasm-${BUILD_NUMBER} -j$(nproc) -B
                '''

                echo 'Build: CC=clang AS=yasm SIMD=y default configuration'
                sh '''
                    export AS='yasm'
                    export CC='clang'
                    make TARGET=${ARTIFACT_DIR}/twinm-clang-yasm-${BUILD_NUMBER} -j$(nproc) -B
                '''

                echo 'Build: CC=gcc AS=yasm SIMD=y release configuration'
                sh '''
                    export AS='yasm'
                    export CC='gcc'
                    make TARGET=${ARTIFACT_DIR}/twinm-gcc-yasm-release-${BUILD_NUMBER} -j$(nproc) release -B
                '''

                echo 'Build: CC=clang AS=yasm SIMD=y release configuration'
                sh '''
                    export CC=clang
                    export AS=yasm
                    make TARGET=${ARTIFACT_DIR}/twinm-clang-yasm-release-${BUILD_NUMBER} -j$(nproc) release -B
                '''

                echo 'Build: CC=gcc AS=yasm SIMD=n default configuration'
                sh '''
                    export CC=gcc
                    export AS=yasm
                    make TARGET=${ARTIFACT_DIR}/twinm-gcc-yasm-no-simd-${BUILD_NUMBER} -j$(nproc) SIMD=n -B
                '''

                echo 'Build: CC=clang AS=yasm SIMD=n default configuration'
                sh '''
                    export CC=clang
                    export AS=yasm
                    make TARGET=${ARTIFACT_DIR}/twinm-clang-yasm-no-simd-${BUILD_NUMBER} -j$(nproc) SIMD=n -B
                '''

                echo 'Creating test directory'
                sh 'mkdir -p ${TEST_DIR}'

                echo 'Build: CC=gcc AS=nasm SIMD=y test configuration'
                sh '''
                    export CC=gcc
                    export AS=nasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-gcc-nasm-${BUILD_NUMBER} -j$(nproc) test -B
                '''

                echo 'Build: CC=clang AS=nasm SIMD=y test configuration'
                sh '''
                    export CC=clang
                    export AS=nasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-clang-nasm-${BUILD_NUMBER} -j$(nproc) test -B
                '''

                echo 'Build: CC=gcc AS=nasm SIMD=n test configuration'
                sh '''
                    export CC=gcc
                    export AS=nasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-gcc-nasm-no-simd-${BUILD_NUMBER} SIMD=n -j$(nproc) test -B
                '''

                echo 'Build: CC=clang AS=nasm SIMD=n test configuration'
                sh '''
                    export CC=clang
                    export AS=nasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-clang-nasm-no-simd-${BUILD_NUMBER} SIMD=n -j$(nproc) test -B
                '''

                echo 'Build: CC=gcc AS=yasm SIMD=y test configuration'
                sh '''
                    export CC=gcc
                    export AS=yasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-gcc-yasm-${BUILD_NUMBER} -j$(nproc) test -B
                '''

                echo 'Build: CC=clang AS=yasm SIMD=y test configuration'
                sh '''
                    export CC=clang
                    export AS=yasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-clang-yasm-${BUILD_NUMBER} -j$(nproc) test -B
                '''

                echo 'Build: CC=gcc AS=yasm SIMD=n test configuration'
                sh '''
                    export CC=gcc
                    export AS=yasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-gcc-yasm-no-simd-${BUILD_NUMBER} SIMD=n -j$(nproc) test -B
                '''

                echo 'Build: CC=clang AS=yasm SIMD=n test configuration'
                sh '''
                    export CC=clang
                    export AS=yasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-clang-yasm-no-simd-${BUILD_NUMBER} SIMD=n -j$(nproc) test -B
                '''

                echo 'Build: CC=gcc AS=nasm SIMD=y test configuration with asan'
                sh '''
                    export CC=gcc
                    export AS=nasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-gcc-nasm-asan-${BUILD_NUMBER} -j$(nproc) test_asan -B
                '''

                echo 'Build: CC=clang AS=nasm SIMD=y test configuration with asan'
                sh '''
                    export CC=clang
                    export AS=nasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-clang-nasm-asan-${BUILD_NUMBER} -j$(nproc) test_asan -B
                '''

                echo 'Build: CC=gcc AS=nasm SIMD=n test configuration with asan'
                sh '''
                    export CC=gcc
                    export AS=nasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-gcc-nasm-asan-no-simd-${BUILD_NUMBER} SIMD=n -j$(nproc) test_asan -B
                '''

                echo 'Build: CC=clang AS=nasm SIMD=n test configuration with asan'
                sh '''
                    export CC=clang
                    export AS=nasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-clang-nasm-asan-no-simd-${BUILD_NUMBER} SIMD=n -j$(nproc) test_asan -B
                '''

                echo 'Build: CC=gcc AS=yasm SIMD=y test configuration with asan'
                sh '''
                    export CC=gcc
                    export AS=yasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-gcc-yasm-asan-${BUILD_NUMBER} -j$(nproc) test_asan -B
                '''

                echo 'Build: CC=clang AS=yasm SIMD=y test configuration with asan'
                sh '''
                    export CC=clang
                    export AS=yasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-clang-yasm-asan-${BUILD_NUMBER} -j$(nproc) test_asan -B
                '''

                echo 'Build: CC=gcc AS=yasm SIMD=n test configuration with asan'
                sh '''
                    export CC=gcc
                    export AS=yasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-gcc-yasm-asan-no-simd-${BUILD_NUMBER} SIMD=n -j$(nproc) test_asan -B
                '''

                echo 'Build: CC=clang AS=yasm SIMD=n test configuration with asan'
                sh '''
                    export CC=clang
                    export AS=yasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-clang-yasm-asan-no-simd-${BUILD_NUMBER} SIMD=n -j$(nproc) test_asan -B
                '''

                echo 'Build: CC=gcc AS=nasm SIMD=y test configuration with ubsan'
                sh '''
                    export CC=gcc
                    export AS=nasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-gcc-nasm-ubsan-${BUILD_NUMBER} -j$(nproc) test_ubsan -B
                '''

                echo 'Build: CC=clang AS=nasm SIMD=y test configuration with ubsan'
                sh '''
                    export CC=clang
                    export AS=nasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-clang-nasm-ubsan-${BUILD_NUMBER} -j$(nproc) test_ubsan -B
                '''

                echo 'Build: CC=gcc AS=nasm SIMD=n test configuration with ubsan'
                sh '''
                    export CC=gcc
                    export AS=nasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-gcc-nasm-ubsan-no-simd-${BUILD_NUMBER} SIMD=n -j$(nproc) test_ubsan -B
                '''

                echo 'Build: CC=clang AS=nasm SIMD=n test configuration with ubsan'
                sh '''
                    export CC=clang
                    export AS=nasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-clang-nasm-ubsan-no-simd-${BUILD_NUMBER} SIMD=n -j$(nproc) test_ubsan -B
                '''

                echo 'Build: CC=gcc AS=yasm SIMD=y test configuration with ubsan'
                sh '''
                    export CC=gcc
                    export AS=yasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-gcc-yasm-ubsan-${BUILD_NUMBER} -j$(nproc) test_ubsan -B
                '''

                echo 'Build: CC=clang AS=yasm SIMD=y test configuration with ubsan'
                sh '''
                    export CC=clang
                    export AS=yasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-clang-yasm-ubsan-${BUILD_NUMBER} -j$(nproc) test_ubsan -B
                '''

                echo 'Build: CC=gcc AS=yasm SIMD=n test configuration with ubsan'
                sh '''
                    export CC=gcc
                    export AS=yasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-gcc-yasm-ubsan-no-simd-${BUILD_NUMBER} SIMD=n -j$(nproc) test_ubsan -B
                '''

                echo 'Build: CC=clang AS=yasm SIMD=n test configuration with ubsan'
                sh '''
                    export CC=clang
                    export AS=yasm
                    make TEST_TARGET=${TEST_DIR}/twinm-test-clang-yasm-ubsan-no-simd-${BUILD_NUMBER} SIMD=n -j$(nproc) test_ubsan -B
                '''
            }
        }
        stage('Test') {
            steps {
                echo '-- Starting tests --'

                echo 'Test: CC=gcc AS=nasm SIMD=y'
                sh '${TEST_DIR}/twinm-test-gcc-nasm-${BUILD_NUMBER}'

                echo 'Test: CC=clang AS=nasm SIMD=y'
                sh '${TEST_DIR}/twinm-test-clang-nasm-${BUILD_NUMBER}'

                echo 'Test: CC=gcc AS=nasm SIMD=n'
                sh '${TEST_DIR}/twinm-test-gcc-nasm-no-simd-${BUILD_NUMBER}'

                echo 'Test: CC=clang AS=nasm SIMD=n'
                sh '${TEST_DIR}/twinm-test-clang-nasm-no-simd-${BUILD_NUMBER}'

                echo 'Test: CC=gcc AS=yasm SIMD=y'
                sh '${TEST_DIR}/twinm-test-gcc-yasm-${BUILD_NUMBER}'

                echo 'Test: CC=clang AS=yasm SIMD=y'
                sh '${TEST_DIR}/twinm-test-clang-yasm-${BUILD_NUMBER}'

                echo 'Test: CC=gcc AS=yasm SIMD=n'
                sh '${TEST_DIR}/twinm-test-gcc-yasm-no-simd-${BUILD_NUMBER}'

                echo 'Test: CC=clang AS=yasm SIMD=n'
                sh '${TEST_DIR}/twinm-test-clang-yasm-no-simd-${BUILD_NUMBER}'

                echo 'Test: CC=gcc AS=nasm SIMD=y valgrind'
                sh 'valgrind ${TEST_DIR}/twinm-test-gcc-nasm-${BUILD_NUMBER}'

                echo 'Test: CC=clang AS=nasm SIMD=y valgrind'
                sh 'valgrind ${TEST_DIR}/twinm-test-clang-nasm-${BUILD_NUMBER}'

                echo 'Test: CC=gcc AS=nasm SIMD=n valgrind'
                sh 'valgrind ${TEST_DIR}/twinm-test-gcc-nasm-no-simd-${BUILD_NUMBER}'

                echo 'Test: CC=clang AS=nasm SIMD=n valgrind'
                sh 'valgrind ${TEST_DIR}/twinm-test-clang-nasm-no-simd-${BUILD_NUMBER}'

                echo 'Test: CC=gcc AS=yasm SIMD=y valgrind'
                sh 'valgrind ${TEST_DIR}/twinm-test-gcc-yasm-${BUILD_NUMBER}'

                echo 'Test: CC=clang AS=yasm SIMD=y valgrind'
                sh 'valgrind ${TEST_DIR}/twinm-test-clang-yasm-${BUILD_NUMBER}'

                echo 'Test: CC=gcc AS=yasm SIMD=n valgrind'
                sh 'valgrind ${TEST_DIR}/twinm-test-gcc-yasm-no-simd-${BUILD_NUMBER}'

                echo 'Test: CC=clang AS=yasm SIMD=n valgrind'
                sh 'valgrind ${TEST_DIR}/twinm-test-clang-yasm-no-simd-${BUILD_NUMBER}'

                echo 'Test: CC=gcc AS=nasm SIMD=y asan'
                sh '${TEST_DIR}/twinm-test-gcc-nasm-asan-${BUILD_NUMBER}'

                echo 'Test: CC=clang AS=nasm SIMD=y asan'
                sh '${TEST_DIR}/twinm-test-clang-nasm-asan-${BUILD_NUMBER}'

                echo 'Test: CC=gcc AS=nasm SIMD=n asan'
                sh '${TEST_DIR}/twinm-test-gcc-nasm-asan-no-simd-${BUILD_NUMBER}'

                echo 'Test: CC=clang AS=nasm SIMD=n asan'
                sh '${TEST_DIR}/twinm-test-clang-nasm-asan-no-simd-${BUILD_NUMBER}'

                echo 'Test: CC=gcc AS=yasm SIMD=y asan'
                sh '${TEST_DIR}/twinm-test-gcc-yasm-asan-${BUILD_NUMBER}'

                echo 'Test: CC=clang AS=yasm SIMD=y asan'
                sh '${TEST_DIR}/twinm-test-clang-yasm-asan-${BUILD_NUMBER}'

                echo 'Test: CC=gcc AS=yasm SIMD=n asan'
                sh '${TEST_DIR}/twinm-test-gcc-yasm-asan-no-simd-${BUILD_NUMBER}'

                echo 'Test: CC=clang AS=yasm SIMD=n asan'
                sh '${TEST_DIR}/twinm-test-clang-yasm-asan-no-simd-${BUILD_NUMBER}'

                echo 'Test: CC=gcc AS=nasm SIMD=y ubsan'
                sh '${TEST_DIR}/twinm-test-gcc-nasm-ubsan-${BUILD_NUMBER}'

                echo 'Test: CC=clang AS=nasm SIMD=y ubsan'
                sh '${TEST_DIR}/twinm-test-clang-nasm-ubsan-${BUILD_NUMBER}'

                echo 'Test: CC=gcc AS=nasm SIMD=n ubsan'
                sh '${TEST_DIR}/twinm-test-gcc-nasm-ubsan-no-simd-${BUILD_NUMBER}'

                echo 'Test: CC=clang AS=nasm SIMD=n ubsan'
                sh '${TEST_DIR}/twinm-test-clang-nasm-ubsan-no-simd-${BUILD_NUMBER}'

                echo 'Test: CC=gcc AS=yasm SIMD=y ubsan'
                sh '${TEST_DIR}/twinm-test-gcc-yasm-ubsan-${BUILD_NUMBER}'

                echo 'Test: CC=clang AS=yasm SIMD=y ubsan'
                sh '${TEST_DIR}/twinm-test-clang-yasm-ubsan-${BUILD_NUMBER}'

                echo 'Test: CC=gcc AS=yasm SIMD=n ubsan'
                sh '${TEST_DIR}/twinm-test-gcc-yasm-ubsan-no-simd-${BUILD_NUMBER}'

                echo 'Test: CC=clang AS=yasm SIMD=n ubsan'
                sh '${TEST_DIR}/twinm-test-clang-yasm-ubsan-no-simd-${BUILD_NUMBER}'

            }
        }
        stage('Gitlab Success') {
            steps {
                echo 'Notifying Gitlab'
                updateGitlabCommitStatus name: 'build', state: 'success'
            }
        }
    }
    post {
        success {
            echo 'No errors encountered'
        }
        always {
            echo 'Cleaning up'
            deleteDir()
        }
    }
}
