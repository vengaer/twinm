FROM archlinux:latest
LABEL maintainer="vilhelm.engstrom@tuta.io"

RUN pacman -Syu --needed --noconfirm make clang gcc nasm yasm valgrind

ENV AS=yasm
ENV CC=gcc

COPY . /twinm
WORKDIR /twinm
