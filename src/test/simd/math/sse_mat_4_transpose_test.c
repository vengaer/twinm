#include "sse_mat_4_transpose_test.h"
#include "macro.h"
#include "simd.h"
#include "twinm_test.h"
#include "twinm_types.h"

#include <math.h>

void test_sse_mat_4_transpose(void) {
    float const matrix[] = {
        1.f,  2.f,  3.f,  4.f,
        5.f,  6.f,  7.f,  8.f,
        9.f,  10.f, 11.f, 12.f,
        13.f, 14.f, 15.f, 16.f
    };
    float const ref[] = {
        1.f,  5.f,  9.f,  13.f,
        2.f,  6.f,  10.f, 14.f,
        3.f,  7.f,  11.f, 15.f,
        4.f,  8.f,  12.f, 16.f
    };
    float dst[array_size(matrix)];

    sse_mat_4_transpose(dst, matrix);

    for(unsigned i = 0; i < array_size(matrix); i++) {
        twinm_assert_lt(fabsf(dst[i] - ref[i]), EPSILON);
    }
}
