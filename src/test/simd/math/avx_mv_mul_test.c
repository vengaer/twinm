#include "avx_mv_mul_test.h"
#include "linalg.h"
#include "simd.h"
#include "twinm_test.h"
#include "twinm_types.h"

#include <math.h>

void test_avx_mv_mul_4(void) {
    float matrix[] = {
         2.f,   12.f, -3.f,  22.f,
        -18.f,  2.f,  -23.f, 1.f,
         3.f,   7.f,   1.f,  0.f,
         12.f,  2.f,   1.f, -1.f,
    };

    float vec[] = {
        23.f,
        12.f,
        1.f,
        4.f
    };

    float res[4];
    float dst[4];

    seq_mv_mul_4(res, matrix, vec);
    avx_mv_mul_4(dst, matrix, vec);
    for(unsigned i = 0; i < 4; i++) {
        twinm_assert_lt(fabsf(res[i] - dst[i]), EPSILON);
    }
}
