#include "avx_memzero_test.h"
#include "simd.h"
#include "twinm_test.h"

#include <stdbool.h>
#include <stddef.h>
#include <string.h>

enum {
    BUFSIZE = 1024,
    TESTSIZE = 256
};

static bool nbytes_zero(char *buf, unsigned nbytes) {
    for(unsigned i = 0; i < nbytes; i++) {
        if(buf[i]) {
            return false;
        }
    }
    return true;
}

static inline char *align(char *ptr, size_t alignment) {
    return (char *)(((ptrdiff_t)ptr + (alignment - 1)) & -alignment);
}

static inline bool is_aligned_to_boundary(char *ptr, size_t alignment) {
    return !((ptrdiff_t)ptr & (alignment - 1));
}

void test_avx_memzero(void) {
    char buf[BUFSIZE];

    memset(buf, 1, sizeof(buf));

    /* Sanity check */
    twinm_assert_eq(avx_memzero(buf, 0), buf);
    twinm_assert(!nbytes_zero(buf, 1));

    for(unsigned i = 1; i < TESTSIZE; i++) {
        avx_memzero(buf, i);
        twinm_assert(nbytes_zero(buf, i));
        twinm_assert(buf[i+1]);

        memset(buf, 1, i);
    }
}

void test_avx_memzero_align1(void) {
    char buf[BUFSIZE];

    memset(buf, 1, sizeof(buf));

    char *pbuf = align(buf, 1);

    twinm_assert(is_aligned_to_boundary(pbuf, 1));

    for(unsigned i = 1; i < TESTSIZE; i++) {
        avx_memzero(pbuf, i);
        twinm_assert(nbytes_zero(pbuf, i));
        twinm_assert(pbuf[i+1]);

        memset(pbuf, 1, i);
    }
}

void test_avx_memzero_align2(void) {
    char buf[BUFSIZE];

    memset(buf, 1, sizeof(buf));

    char *pbuf = align(buf, 2);

    twinm_assert(is_aligned_to_boundary(pbuf, 2));

    for(unsigned i = 1; i < TESTSIZE; i++) {
        avx_memzero(pbuf, i);
        twinm_assert(nbytes_zero(pbuf, i));
        twinm_assert(pbuf[i+1]);

        memset(pbuf, 1, i);
    }
}

void test_avx_memzero_align4(void) {
    char buf[BUFSIZE];

    memset(buf, 1, sizeof(buf));

    char *pbuf = align(buf, 4);

    twinm_assert(is_aligned_to_boundary(pbuf, 4));

    for(unsigned i = 1; i < TESTSIZE; i++) {
        avx_memzero(pbuf, i);
        twinm_assert(nbytes_zero(pbuf, i));
        twinm_assert(pbuf[i+1]);

        memset(pbuf, 1, i);
    }
}

void test_avx_memzero_align8(void) {
    char buf[BUFSIZE];

    memset(buf, 1, sizeof(buf));

    char *pbuf = align(buf, 8);

    twinm_assert(is_aligned_to_boundary(pbuf, 8));

    for(unsigned i = 1; i < TESTSIZE; i++) {
        avx_memzero(pbuf, i);
        twinm_assert(nbytes_zero(pbuf, i));
        twinm_assert(pbuf[i+1]);

        memset(pbuf, 1, i);
    }
}

void test_avx_memzero_align16(void) {
    char buf[BUFSIZE];

    memset(buf, 1, sizeof(buf));

    char *pbuf = align(buf, 16);

    twinm_assert(is_aligned_to_boundary(pbuf, 16));

    for(unsigned i = 1; i < TESTSIZE; i++) {
        avx_memzero(pbuf, i);
        twinm_assert(nbytes_zero(pbuf, i));
        twinm_assert(pbuf[i+1]);

        memset(pbuf, 1, i);
    }
}

void test_avx_memzero_align32(void) {
    char buf[BUFSIZE];

    memset(buf, 1, sizeof(buf));

    char *pbuf = align(buf, 32);

    twinm_assert(is_aligned_to_boundary(pbuf, 32));

    for(unsigned i = 1; i < TESTSIZE; i++) {
        avx_memzero(pbuf, i);
        twinm_assert(nbytes_zero(pbuf, i));
        twinm_assert(pbuf[i+1]);

        memset(pbuf, 1, i);
    }
}
