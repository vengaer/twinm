#ifndef AVX_MEMZERO_TEST_H
#define AVX_MEMZERO_TEST_H

void test_avx_memzero(void);
void test_avx_memzero_align1(void);
void test_avx_memzero_align2(void);
void test_avx_memzero_align4(void);
void test_avx_memzero_align8(void);
void test_avx_memzero_align16(void);
void test_avx_memzero_align32(void);

#endif /* AVX_MEMZERO_TEST_H */
