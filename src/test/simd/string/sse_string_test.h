#ifndef SSE_STRING_TEST_H
#define SSE_STRING_TEST_H

void test_sse_string_tolower(void);
void test_sse_string_toupper(void);

#endif /* SSE_STRING_TEST_H */
