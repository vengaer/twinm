#ifndef AVX_STRING_TEST_H
#define AVX_STRING_TEST_H

void test_avx_string_tolower(void);
void test_avx_string_toupper(void);

#endif /* AVX_STRING_TEST_H */
