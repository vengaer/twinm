#ifndef SSE_MEMZERO_TEST_H
#define SSE_MEMZERO_TEST_H

void test_sse_memzero(void);
void test_sse_memzero_align1(void);
void test_sse_memzero_align2(void);
void test_sse_memzero_align4(void);
void test_sse_memzero_align8(void);
void test_sse_memzero_align16(void);

#endif /* SSE_MEMZERO_TEST_H */
