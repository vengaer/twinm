#include "simd.h"
#include "sse_string_test.h"
#include "twinm_test.h"

#include <ctype.h>
#include <stdio.h>
#include <string.h>


static inline void tolower_seq_inplace(char *str, unsigned len) {
    for(unsigned i = 0; i < len; i++) {
        str[i] = tolower(str[i]);
    }
}

static inline void toupper_seq_inplace(char *str, unsigned len) {
    for(unsigned i = 0; i < len; i++) {
        str[i] = toupper(str[i]);
    }
}

void test_sse_string_tolower(void) {
    enum { SIZE = 64 };

    char buffer[SIZE];
    char input[SIZE];

    /* Empty string */
    input[0] = '\0';
    twinm_assert_eq(avx_string_tolower(buffer, input, sizeof buffer, strlen(input)), 0);
    twinm_assert_eq(strcmp(buffer, input), 0);

    /* String where only null-terminator doesn't fit */
    snprintf(input, sizeof input, "No null terminator");
    twinm_assert_lt(avx_string_tolower(buffer, input, strlen(input), strlen(input)), 0);

    /* Residuals only */
    snprintf(input, sizeof input, "RESIDUAL only");
    twinm_assert_lt(strlen(input), 16);
    twinm_assert_lt(avx_string_tolower(buffer, input, 4, strlen(input)), 0);
    twinm_assert_eq(avx_string_tolower(buffer, input, sizeof buffer, strlen(input)), 0);
    tolower_seq_inplace(input, strlen(input));
    twinm_assert_eq(strcmp(buffer, input), 0);

    /* SSE only */
    snprintf(input, sizeof input, "SSE ONLY no Res ");
    twinm_assert_eq(strlen(input), 16);
    twinm_assert_eq(avx_string_tolower(buffer, input, sizeof buffer, strlen(input)), 0);
    tolower_seq_inplace(input, strlen(input));
    twinm_assert_eq(strcmp(buffer, input), 0);

    /* SSE with residuals */
    snprintf(input, sizeof input, "SSE with RESIduals");
    twinm_assert_gt(strlen(input), 16);
    twinm_assert_lt(strlen(input), 32);
    twinm_assert_eq(avx_string_tolower(buffer, input, sizeof buffer, strlen(input)), 0);
    tolower_seq_inplace(input, strlen(input));
    twinm_assert_eq(strcmp(buffer, input), 0);
}

void test_sse_string_toupper(void) {
    enum { SIZE = 64 };

    char buffer[SIZE];
    char input[SIZE];

    /* Empty string */
    input[0] = '\0';
    twinm_assert_eq(avx_string_toupper(buffer, input, sizeof buffer, strlen(input)), 0);
    twinm_assert_eq(strcmp(buffer, input), 0);

    /* String where only null-terminator doesn't fit */
    snprintf(input, sizeof input, "No null terminator");
    twinm_assert_lt(avx_string_toupper(buffer, input, strlen(input), strlen(input)), 0);

    /* Residuals only */
    snprintf(input, sizeof input, "RESIDUAL only");
    twinm_assert_lt(strlen(input), 16);
    twinm_assert_lt(avx_string_toupper(buffer, input, 4, strlen(input)), 0);
    twinm_assert_eq(avx_string_toupper(buffer, input, sizeof buffer, strlen(input)), 0);
    toupper_seq_inplace(input, strlen(input));
    twinm_assert_eq(strcmp(buffer, input), 0);

    /* SSE only */
    snprintf(input, sizeof input, "SSE ONLY no Res ");
    twinm_assert_eq(strlen(input), 16);
    twinm_assert_eq(avx_string_toupper(buffer, input, sizeof buffer, strlen(input)), 0);
    toupper_seq_inplace(input, strlen(input));
    twinm_assert_eq(strcmp(buffer, input), 0);

    /* SSE with residuals */
    snprintf(input, sizeof input, "SSE with RESIduals");
    twinm_assert_gt(strlen(input), 16);
    twinm_assert_lt(strlen(input), 32);
    twinm_assert_eq(avx_string_toupper(buffer, input, sizeof buffer, strlen(input)), 0);
    toupper_seq_inplace(input, strlen(input));
    twinm_assert_eq(strcmp(buffer, input), 0);
}
