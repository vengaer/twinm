#include "macro.h"
#include "msgbus.h"
#include "msgbus_test.h"
#include "mutex.h"
#include "thread.h"
#include "twinm_test.h"
#include "twinm_types.h"

#include <string.h>

enum { MSG_SIZE = 128 };

static char const *messages[] = {
    "Message 0",  "Message 1",  "Message 2",  "Message 3",
    "Message 4",  "Message 5",  "Message 6",  "Message 7",
    "Message 8",  "Message 9",  "Message 10", "Message 11",
    "Message 12", "Message 13", "Message 14", "Message 15",
    "Message 16", "Message 17", "Message 18", "Message 19",
    "Message 20", "Message 21", "Message 22", "Message 23",
    "Message 24", "Message 25", "Message 26", "Message 27",
    "Message 28", "Message 29", "Message 30", "Message 31",
    "Message 32", "Message 33", "Message 34", "Message 35",
    "Message 36", "Message 37", "Message 38", "Message 39",
    "Message 40", "Message 41", "Message 42", "Message 43",
    "Message 44", "Message 45", "Message 46", "Message 47",
    "Message 48", "Message 49", "Message 50", "Message 51",
    "Message 52", "Message 53", "Message 54", "Message 55",
    "Message 56", "Message 57", "Message 58", "Message 59",
    "Message 60", "Message 61", "Message 62", "Message 63",
    "Message 64", "Message 65", "Message 66", "Message 67",
    "Message 68", "Message 69", "Message 70", "Message 71",
    "Message 72", "Message 73", "Message 74", "Message 75",
    "Message 76", "Message 77", "Message 78", "Message 79"
};

static mutex_handle assertion_mutex;

static void *concurrent_append(void *args) {
    struct msgbus *bus = args;
    int append_status;
    for(unsigned i = 0; i < array_size(messages); i++) {
        append_status = msgbus_append(bus, messages[i]);
        lock_guard(&assertion_mutex) {
            twinm_assert_eq(append_status, 0);
        }
    }
    return 0;
}

static void *concurrent_read(void *args) {
    char buffer[MSG_SIZE];

    struct msgbus_subscription *sub = (struct msgbus_subscription *) args;
    int val;

    for(unsigned i = 0; i < array_size(messages); i++) {
        while((val = msgbus_try_get(buffer, sub, sizeof(buffer))) == -EEMPTY) {
            sleep(1);
        }
        lock_guard(&assertion_mutex) {
            twinm_assert_eq(strcmp(buffer, messages[i]), 0);
        }
    }

    return 0;
}

void test_msgbus_init_destroy(void) {
    struct msgbus bus;

    twinm_assert_eq(msgbus_init(&bus), 0);
    twinm_assert_eq(bus.current, bus.head);
    twinm_assert_eq(bus.nsubscribers, 0);
    twinm_assert_eq(bus.msg_index, 0);
    twinm_assert_eq(msgbus_destroy(&bus), 0);
}

void test_msgbus_append(void) {
    struct msgbus bus;
    struct msgbus_subscription sub;
    char buffer[MSG_SIZE];

    char const *msg0 = "Message to store";
    char const *msg1 = "Another message to store";
    char const *msg2 = "Third message";

    twinm_assert_eq(msgbus_init(&bus), 0);
    twinm_assert_eq(msgbus_append(&bus, "Message to discard"), 0);
    twinm_assert_eq(bus.msg_index, 0);
    twinm_assert_eq(msgbus_subscribe(&bus, &sub), 0);
    twinm_assert_eq(msgbus_append(&bus, msg0), 0);
    twinm_assert_eq(bus.msg_index, 1);
    twinm_assert_eq(msgbus_append(&bus, msg1), 0);
    twinm_assert_eq(bus.msg_index, 2);
    twinm_assert_eq(msgbus_try_get(buffer, &sub, sizeof(buffer)), 0);
    twinm_assert_eq(strcmp(buffer, msg0), 0);
    twinm_assert_eq(msgbus_try_get(buffer, &sub, sizeof(buffer)), 0);
    twinm_assert_eq(strcmp(buffer, msg1), 0);
    twinm_assert_eq(msgbus_try_get(buffer, &sub, sizeof(buffer)), -EEMPTY);
    twinm_assert_eq(msgbus_append(&bus, msg2), 0);
    twinm_assert_eq(msgbus_try_get(buffer, &sub, sizeof(buffer)), 0);
    twinm_assert_eq(strcmp(buffer, msg2), 0);
    twinm_assert_eq(msgbus_destroy(&bus), 0);
}

void test_msgbus_concurrency(void) {
    thread_handle writer;
    thread_handle reader;

    struct msgbus bus;
    struct msgbus_subscription sub0;
    struct msgbus_subscription sub1;

    twinm_assert_eq(mutex_init(&assertion_mutex), 0);
    twinm_assert_eq(msgbus_init(&bus), 0);
    twinm_assert_eq(msgbus_subscribe(&bus, &sub0), 0);
    twinm_assert_eq(msgbus_subscribe(&bus, &sub1), 0);

    int forkres = thread_fork(&writer, concurrent_append, &bus);
    lock_guard(&assertion_mutex) {
        twinm_assert_eq(forkres, 0);
    }
    forkres = thread_fork(&reader, concurrent_read, &sub0);

    lock_guard(&assertion_mutex) {
        twinm_assert_eq(forkres, 0);
    }

    concurrent_read(&sub1);

    int joinres = thread_join(writer, 0);
    lock_guard(&assertion_mutex) {
        twinm_assert_eq(joinres, 0);
    }
    joinres = thread_join(reader, 0);

    lock_guard(&assertion_mutex) {
        twinm_assert_eq(joinres, 0);
    }

    twinm_assert_eq(msgbus_destroy(&bus), 0);
    twinm_assert_eq(mutex_destroy(&assertion_mutex), 0);
}

void test_msgbus_queued_messages(void) {
    struct msgbus bus;
    struct msgbus_subscription sub;

    char buffer[MSG_SIZE];

    twinm_assert_eq(msgbus_init(&bus), 0);
    twinm_assert_eq(msgbus_subscribe(&bus, &sub), 0);

    for(unsigned i = 0; i < array_size(messages); i++) {
        twinm_assert_eq(msgbus_append(&bus, messages[i]), 0);
    }

    unsigned i;
    for(i = 0; msgbus_try_get(buffer, &sub, sizeof(buffer)) != -EEMPTY; i++) {
        twinm_assert_eq(strcmp(buffer, messages[i]), 0);
    }

    twinm_assert_eq(i, array_size(messages));
    twinm_assert_eq(msgbus_destroy(&bus), 0);
}

void test_msgbus_unsubscribe(void) {
    struct msgbus bus;
    struct msgbus_subscription sub;

    char buffer[MSG_SIZE];
    unsigned i;

    twinm_assert_eq(msgbus_init(&bus), 0);
    twinm_assert_eq(msgbus_subscribe(&bus, &sub), 0);

    for(i = 0; i < array_size(messages) / 3; i++) {
        twinm_assert_eq(msgbus_append(&bus, messages[i]), 0);
    }

    twinm_assert_eq(msgbus_unsubscribe(&sub), 0);

    for(; i < array_size(messages) * 2 / 3; i++) {
        twinm_assert_eq(msgbus_append(&bus, messages[i]), 0);
    }

    twinm_assert_eq(msgbus_subscribe(&bus, &sub), 0);

    for(; i < array_size(messages); i++) {
        twinm_assert_eq(msgbus_append(&bus, messages[i]), 0);
    }

    for(i = array_size(messages) * 2 / 3; i < array_size(messages); i++) {
        twinm_assert_eq(msgbus_try_get(buffer, &sub, sizeof(buffer)), 0);
        twinm_assert_eq(strcmp(buffer, messages[i]), 0);
    }
    twinm_assert_eq(msgbus_destroy(&bus), 0);
}
