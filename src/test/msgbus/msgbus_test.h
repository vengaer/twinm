#ifndef MSGBUS_TEST_H
#define MSGBUS_TEST_H

void test_msgbus_init_destroy(void);
void test_msgbus_append(void);
void test_msgbus_concurrency(void);
void test_msgbus_queued_messages(void);
void test_msgbus_unsubscribe(void);

#endif /* MSGBUS_TEST_H */
