#include "bitop_test.h"
#include "cond_var_test.h"
#include "dfa_test.h"
#include "file_test.h"
#include "list_test.h"
#include "macro_test.h"
#include "msgbus_test.h"
#include "linalg_test.h"
#include "mutex_test.h"
#include "nfa_test.h"
#include "queue_test.h"
#include "rbtree_test.h"
#include "regex_test.h"
#include "stack_test.h"
#include "strutils_test.h"
#include "thread_test.h"
#include "twinm_test.h"
#include "vector_test.h"

#ifdef TWINM_SIMD_AVX
#include "avx_memzero_test.h"
#include "avx_string_test.h"
#include "avx_mv_mul_test.h"
#include "avx_mat_4_transpose_test.h"
#endif

#if defined TWINM_SIMD_AVX || defined TWINM_SIMD_SSE
#include "sse_memzero_test.h"
#include "sse_string_test.h"
#include "sse_mv_mul_test.h"
#include "sse_mat_4_transpose_test.h"
#endif

int main(void) {
    twinm_test_section(list);
    twinm_test(test_list_push_front);
    twinm_test(test_list_push_back);
    twinm_test(test_list_push_front_back);
    twinm_test(test_list_extract);
    twinm_test(test_list_empty);
    twinm_test(test_list_size);
    twinm_test(test_list_for_each_zip);
    twinm_test(test_list_find);

    twinm_test_section(stack);
    twinm_test(test_stack_push_pop);
    twinm_test(test_stack_dynamic_allocation_behavior);
    twinm_test(test_stack_peek);

    twinm_test_section(queue);
    twinm_test(test_queue_enqueue_dequeue);
    twinm_test(test_queue_dynamic_allocation_behavior);
    twinm_test(test_queue_indexing);

    twinm_test_section(rbtree);
    twinm_test(test_rbtree_init);
    twinm_test(test_rbtree_find);
    twinm_test(test_rbtree_insert);
    twinm_test(test_rbtree_remove);

    twinm_test_section(vector);
    twinm_test(test_vec_push);
    twinm_test(test_vec_pop);
    twinm_test(test_vec_reserve);
    twinm_test(test_vec_resize);
    twinm_test(test_vec_guard);
    twinm_test(test_vec_access);

    twinm_test_section(strutils);
    twinm_test(test_strscpy_result);
    twinm_test(test_strscpy_return_value);
    twinm_test(test_strscpy_null_termination);
    twinm_test(test_strsncpy_result);
    twinm_test(test_strsncpy_return_value);
    twinm_test(test_strsncpy_null_termination);
    twinm_test(test_seq_string_tolower);
    twinm_test(test_strlower);
    twinm_test(test_seq_string_toupper);
    twinm_test(test_strupper);
    twinm_test(test_strrepl);

    twinm_test_section(bitop);
    twinm_test(test_nand);
    twinm_test(test_nor);
    twinm_test(test_xnor);
    twinm_test(test_max);
    twinm_test(test_min);

    twinm_test_section(macro);
    twinm_test(test_array_index);
    twinm_test(test_array_size);
    twinm_test(test_nargs);
    twinm_test(test_is_immediate);

    twinm_test_section(nfa);
    twinm_test(test_nfa_to_regex);
    twinm_test(test_nfa_to_regex_overflow);
    twinm_test(test_nfa_to_regex_nested_parens);
    twinm_test(test_regex_to_nfa_rep_qm);
    twinm_test(test_regex_to_nfa_rep_ps);
    twinm_test(test_regex_to_nfa_rep_ax);
    twinm_test(test_regex_to_nfa_alternation);
    twinm_test(test_regex_to_nfa_nested_alternation);
    twinm_test(test_regex_to_nfa_repetition_in_alternation);
    twinm_test(test_regex_to_nfa_escaped_char);
    twinm_test(test_regex_to_nfa);
    twinm_test(test_regex_to_nfa_repeat_n);
    twinm_test(test_regex_to_nfa_repeat_n_to_m);
    twinm_test(test_regex_to_nfa_repeat_n_to_any);
    twinm_test(test_regex_to_nfa_digit_metachar);
    twinm_test(test_regex_to_nfa_alnum_metachar);
    twinm_test(test_regex_to_nfa_whitespace_metachar);
    twinm_test(test_regex_to_nfa_non_digit_metachar);
    twinm_test(test_regex_to_nfa_non_alnum_metachar);
    twinm_test(test_regex_to_nfa_non_whitespace_metachar);
    twinm_test(test_nfa_epsilon_closure);

    twinm_test_section(dfa);
    twinm_test(test_regex_to_dfa);
    twinm_test(test_regex_to_dfa_match_any);
    twinm_test(test_regex_to_dfa_escaped_char);
    twinm_test(test_dfa_anchor_start);
    twinm_test(test_dfa_anchor_end);

    twinm_test_section(regex);
    twinm_test(test_regex_compile);
    twinm_test(test_regex_compile_non_negated_char_class);
    twinm_test(test_regex_compile_negated_char_class);
    twinm_test(test_regex_match);
    twinm_test(test_regex_match_capture);
    twinm_test(test_regex_match_repeat_n);
    twinm_test(test_regex_match_repeat_n_to_m);
    twinm_test(test_regex_match_repeat_n_to_any);
    twinm_test(test_regex_match_back_to_back_captures);
    twinm_test(test_regex_match_capture_after_repetition);
    twinm_test(test_regex_match_repetition_in_capture);
    twinm_test(test_regex_match_empty_capture);
    twinm_test(test_regex_match_digit);
    twinm_test(test_regex_match_alnum);
    twinm_test(test_regex_match_whitespace);
    twinm_test(test_regex_match_non_digit);
    twinm_test(test_regex_match_non_alnum);
    twinm_test(test_regex_match_non_whitespace);
    twinm_test(test_regex_match_non_negated_char_class);
    twinm_test(test_regex_match_negated_char_class);
    twinm_test(test_regex_match_capture_char_class_repetition);
    twinm_test(test_regex_match_case_insensitive);
    twinm_test(test_regex_match_inverted);
    twinm_test(test_regex_match_combined);
    twinm_test(test_regex_match_nested_repetition);
    twinm_test(test_regex_guard);

    twinm_test_section(mutex);
    twinm_test(test_mutex_lock_unlock);
    twinm_test(test_mutex_lock_guard);

    twinm_test_section(thread);
    twinm_test(test_thread_fork_join);
    twinm_test(test_thread_exit);
    twinm_test(test_thread_guard);

    twinm_test_section(cond_var);
    twinm_test(test_cond_var_wake);
    twinm_test(test_cond_var_wait_timeout);
    twinm_test(test_cond_var_wake_all);

    twinm_test_section(msgbus);
    twinm_test(test_msgbus_init_destroy);
    twinm_test(test_msgbus_append);
    twinm_test(test_msgbus_concurrency);
    twinm_test(test_msgbus_queued_messages);
    twinm_test(test_msgbus_unsubscribe);

    twinm_test_section(linalg);
    twinm_test(test_seq_mv_mul_4);
    twinm_test(test_seq_mat_4_transpose);

    twinm_test_section(filesystem);
    twinm_test(test_file_guard);
    twinm_test(test_file_exists);
    twinm_test(test_file_unlink);

#ifdef TWINM_SIMD_AVX
    twinm_test_section(avx_string);
    twinm_test(test_avx_string_tolower);
    twinm_test(test_avx_string_toupper);
    twinm_test(test_avx_memzero);
    twinm_test(test_avx_memzero_align1);
    twinm_test(test_avx_memzero_align2);
    twinm_test(test_avx_memzero_align4);
    twinm_test(test_avx_memzero_align8);
    twinm_test(test_avx_memzero_align16);
    twinm_test(test_avx_memzero_align32);

    twinm_test_section(avx_math);
    twinm_test(test_avx_mv_mul_4);
    twinm_test(test_avx_mat_4_transpose);
#endif

#if defined TWINM_SIMD_AVX || defined TWINM_SIMD_SSE
    twinm_test_section(sse_string);
    twinm_test(test_sse_string_tolower);
    twinm_test(test_sse_string_toupper);
    twinm_test(test_sse_memzero);
    twinm_test(test_sse_memzero_align1);
    twinm_test(test_sse_memzero_align2);
    twinm_test(test_sse_memzero_align4);
    twinm_test(test_sse_memzero_align8);
    twinm_test(test_sse_memzero_align16);

    twinm_test_section(sse_math);
    twinm_test(test_sse_mv_mul_4);
    twinm_test(test_sse_mat_4_transpose);
#endif

    return twinm_test_summary();
}
