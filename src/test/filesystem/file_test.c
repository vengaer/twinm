#include "file.h"
#include "file_test.h"
#include "twinm_test.h"

#include <stdio.h>

void test_file_guard(void) {
    char const *file = ".twinm_file_guard_test";
    FILE *fp = 0;
    int status;

    twinm_assert_eq(fp, 0);
    file_guard(fp, file, "w", &status) {
        twinm_assert_neq(fp, 0);
        twinm_assert_eq(status, -EOPEN);
    }
    twinm_assert_eq(status, 0);
    twinm_assert_eq(file_unlink(file), 0);
}

void test_file_exists(void) {
    char const *file = ".twinm_file_exists_test";
    FILE *fp;
    int status;

    /* Ensure file exists */
    file_guard(fp, file, "w", &status);

    twinm_assert_eq(status, 0);
    twinm_assert(file_exists(file));

    twinm_assert_eq(file_unlink(file), 0);
    twinm_assert(!file_exists(file));
}

void test_file_unlink(void) {
    char const *file = ".twinm_file_unlink_test";
    FILE *fp;
    int status;

    file_guard(fp, file, "w", &status);

    twinm_assert_eq(status, 0);
    twinm_assert(file_exists(file));

    twinm_assert_eq(file_unlink(file), 0);
    twinm_assert(!file_exists(file));
}
