#ifndef FILE_TEST_H
#define FILE_TEST_H

void test_file_guard(void);
void test_file_exists(void);
void test_file_unlink(void);

#endif /* FILE_TEST_H */
