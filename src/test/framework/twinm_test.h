#ifndef TWINM_TEST_H
#define TWINM_TEST_H

#include "macro.h"

#include <stdbool.h>

#ifdef NDEBUG

#define twinm_assert(expr) \
    do { } while(0)
#define twinm_assert_eq(lhs, rhs) \
    do {} while(0)
#define twinm_assert_neq(lhs, rhs) \
    do {} while(0)
#define twinm_assert_lt(lhs, rhs) \
    do {} while(0)
#define twinm_assert_gt(lhs, rhs) \
    do {} while(0)
#define twinm_assert_leq(lhs, rhs) \
    do {} while(0)
#define twinm_assert_geq(lhs, rhs) \
    do {} while(0)

#else

#define twinm_assert(expr) \
    twinm_assert_internal(expr, #expr, __FILE__, __LINE__)

#define twinm_assert_eq(lhs, rhs)                       \
    _Generic((lhs),                                     \
        bool: twinm_assert_eq_unsigned,                 \
        char: twinm_assert_eq_char,                     \
        signed char: twinm_assert_eq_signed,            \
        short: twinm_assert_eq_signed,                  \
        int: twinm_assert_eq_signed,                    \
        long: twinm_assert_eq_signed,                   \
        long long: twinm_assert_eq_signed,              \
        unsigned char: twinm_assert_eq_unsigned,        \
        unsigned short: twinm_assert_eq_unsigned,       \
        unsigned: twinm_assert_eq_unsigned,             \
        unsigned long: twinm_assert_eq_unsigned,        \
        unsigned long long: twinm_assert_eq_unsigned,   \
        default: twinm_assert_eq_ptr                    \
    )(lhs, rhs, #lhs, #rhs, __FILE__, __LINE__)

#define twinm_assert_neq(lhs, rhs)                      \
    _Generic((lhs),                                     \
        bool: twinm_assert_neq_unsigned,                \
        char: twinm_assert_neq_char,                    \
        signed char: twinm_assert_neq_signed,           \
        short: twinm_assert_neq_signed,                 \
        int: twinm_assert_neq_signed,                   \
        long: twinm_assert_neq_signed,                  \
        long long: twinm_assert_neq_signed,             \
        unsigned char: twinm_assert_neq_unsigned,       \
        unsigned short: twinm_assert_neq_unsigned,      \
        unsigned: twinm_assert_neq_unsigned,            \
        unsigned long: twinm_assert_neq_unsigned,       \
        unsigned long long: twinm_assert_neq_unsigned,  \
        default: twinm_assert_neq_ptr                   \
    )(lhs, rhs, #lhs, #rhs, __FILE__, __LINE__)

#define twinm_assert_lt(lhs, rhs)                       \
    _Generic((lhs),                                     \
        char: twinm_assert_lt_char,                     \
        signed char: twinm_assert_lt_signed,            \
        short: twinm_assert_lt_signed,                  \
        int: twinm_assert_lt_signed,                    \
        long: twinm_assert_lt_signed,                   \
        long long: twinm_assert_lt_signed,              \
        unsigned char: twinm_assert_lt_unsigned,        \
        unsigned short: twinm_assert_lt_unsigned,       \
        unsigned: twinm_assert_lt_unsigned,             \
        unsigned long: twinm_assert_lt_unsigned,        \
        unsigned long long: twinm_assert_lt_unsigned,   \
        float: twinm_assert_lt_double,                  \
        double: twinm_assert_lt_double,                 \
        default: twinm_assert_lt_ptr                    \
    )(lhs, rhs, #lhs, #rhs, __FILE__, __LINE__)

#define twinm_assert_gt(lhs, rhs)                       \
    _Generic((lhs),                                     \
        char: twinm_assert_gt_char,                     \
        signed char: twinm_assert_gt_signed,            \
        short: twinm_assert_gt_signed,                  \
        int: twinm_assert_gt_signed,                    \
        long: twinm_assert_gt_signed,                   \
        long long: twinm_assert_gt_signed,              \
        unsigned char: twinm_assert_gt_unsigned,        \
        unsigned short: twinm_assert_gt_unsigned,       \
        unsigned: twinm_assert_gt_unsigned,             \
        unsigned long: twinm_assert_gt_unsigned,        \
        unsigned long long: twinm_assert_gt_unsigned,   \
        float: twinm_assert_gt_double,                  \
        double: twinm_assert_gt_double,                 \
        default: twinm_assert_gt_ptr                    \
    )(lhs, rhs, #lhs, #rhs, __FILE__, __LINE__)

#define twinm_assert_leq(lhs, rhs)                      \
    _Generic((lhs),                                     \
        char: twinm_assert_leq_char,                    \
        signed char: twinm_assert_leq_signed,           \
        short: twinm_assert_leq_signed,                 \
        int: twinm_assert_leq_signed,                   \
        long: twinm_assert_leq_signed,                  \
        long long: twinm_assert_leq_signed,             \
        unsigned char: twinm_assert_leq_unsigned,       \
        unsigned short: twinm_assert_leq_unsigned,      \
        unsigned: twinm_assert_leq_unsigned,            \
        unsigned long: twinm_assert_leq_unsigned,       \
        unsigned long long: twinm_assert_leq_unsigned,  \
        default: twinm_assert_leq_ptr                   \
    )(lhs, rhs, #lhs, #rhs, __FILE__, __LINE__)

#define twinm_assert_geq(lhs, rhs)                      \
    _Generic((lhs),                                     \
        char: twinm_assert_geq_char,                    \
        signed char: twinm_assert_geq_signed,           \
        short: twinm_assert_geq_signed,                 \
        int: twinm_assert_geq_signed,                   \
        long: twinm_assert_geq_signed,                  \
        long long: twinm_assert_geq_signed,             \
        unsigned char: twinm_assert_geq_unsigned,       \
        unsigned short: twinm_assert_geq_unsigned,      \
        unsigned: twinm_assert_geq_unsigned,            \
        unsigned long: twinm_assert_geq_unsigned,       \
        unsigned long long: twinm_assert_geq_unsigned,  \
        default: twinm_assert_geq_ptr                   \
    )(lhs, rhs, #lhs, #rhs, __FILE__, __LINE__)

#endif

#define GENERATE_ASSERTION_PROTOTYPE2(opname, suffix, type)                 \
    void twinm_assert_##opname##_##suffix(type lhs, type rhs,               \
                                      char const *lhss, char const *rhss,   \
                                      char const *file, unsigned line)

#define GENERATE_ASSERTION_PROTOTYPE4(opname, suffix, type, ...)    \
    GENERATE_ASSERTION_PROTOTYPE2(opname, suffix, type);            \
    GENERATE_ASSERTION_PROTOTYPE2(opname, __VA_ARGS__)

#define GENERATE_ASSERTION_PROTOTYPE6(opname, suffix, type, ...)    \
    GENERATE_ASSERTION_PROTOTYPE2(opname, suffix, type);            \
    GENERATE_ASSERTION_PROTOTYPE4(opname, __VA_ARGS__)

#define GENERATE_ASSERTION_PROTOTYPE8(opname, suffix, type, ...)    \
    GENERATE_ASSERTION_PROTOTYPE2(opname, suffix, type);            \
    GENERATE_ASSERTION_PROTOTYPE6(opname, __VA_ARGS__)

#define GENERATE_ASSERTION_PROTOTYPE10(opname, suffix, type, ...)    \
    GENERATE_ASSERTION_PROTOTYPE2(opname, suffix, type);            \
    GENERATE_ASSERTION_PROTOTYPE8(opname, __VA_ARGS__)

/* Generate function prototypes for internal assertion functions
 *
 * @opname: name of the comparison operation
 * The variadic arguments should be given in pairs of function name
 * suffix and the corresponding type
 *
 * As an example, GENERATE_ASSERTION_PROTOTYPES(eq, char, char, ptr, void *);
 * will generate the prototypes
 * void twinm_assert_eq_char(char lhs, char rhs, char const *lhss, char const *rhss,
 *                           char const *file, unsigned line);
 * void twinm_assert_eq_ptr(void *lhs, void *rhs, char const *lhss, char const *rhss,
 *                          char const *file, unsigned line);
 * where
 * @lhs:  value on the left of the comparison
 * @rhs:  value on the right of the comparison operator
 * @lhss: the expression generating lhs, as a string
 * @rhss: the expression generating rhs, as a string
 * @file: the name of the file
 * @line: the line number
 */
#define GENERATE_ASSERTION_PROTOTYPES(opname, ...) \
    CAT_EXPAND(GENERATE_ASSERTION_PROTOTYPE,nargs(__VA_ARGS__))(opname, __VA_ARGS__)

#define twinm_test(test)                            \
    do {                                            \
        twinm_test_register(#test);                 \
        test();                                     \
        twinm_test_print_outcome();                 \
    } while (0)

#define twinm_test_section(name)                    \
    twinm_print_heading(#name)

/* Register a test. Increments the test counter and
 * sets the name of the current test
 */
void twinm_test_register(char const *test_name);

/* Print a heading on the form
 *
 * Heading text
 * ============
 *
 * */
void twinm_print_heading(char const *heading);

/* Print whether the test failed or succeeded */
void twinm_test_print_outcome(void);

/* Print summary and any tests that failed.
 *
 * Returns non-zero if any tests failed
 */
int twinm_test_summary(void);

/* Internal assertion function
 *
 * @eval: The evaluation of the expression
 * @expr: The expression as a string
 * @file: The file name
 * @line: The line number
 */
void twinm_assert_internal(bool eval, char const *expr, char const *file, unsigned line);

GENERATE_ASSERTION_PROTOTYPES(eq, char, char,
                                  ptr, void const *,
                                  signed, long long,
                                  unsigned, unsigned long long);

GENERATE_ASSERTION_PROTOTYPES(neq, char, char,
                                   ptr, void const *,
                                   signed, long long,
                                   unsigned, unsigned long long);

GENERATE_ASSERTION_PROTOTYPES(lt, char, char,
                                  ptr, void const *,
                                  signed, long long,
                                  unsigned, unsigned long long,
                                  double, double);

GENERATE_ASSERTION_PROTOTYPES(gt, char, char,
                                  ptr, void const *,
                                  signed, long long,
                                  unsigned, unsigned long long,
                                  double, double);

GENERATE_ASSERTION_PROTOTYPES(leq, char, char,
                                   ptr, void const *,
                                   signed, long long,
                                   unsigned, unsigned long long);

GENERATE_ASSERTION_PROTOTYPES(geq, char, char,
                                   ptr, void const *,
                                   signed, long long,
                                   unsigned, unsigned long long);
#endif /* TWINM_TEST_H */
