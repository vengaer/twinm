#include "bitop.h"
#include "strutils.h"
#include "twinm_test.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define GENERIC_CMP_ASSERTION(lhs, rhs, lhss, rhss, file, line, fmt, cmp)           \
    do {                                                                            \
        ++nasserts;                                                                 \
        if(!(lhs cmp rhs)) {                                                        \
            ensure_failure_log_capacity();                                          \
            char path[MAX_PATH_SIZE];                                               \
            strscpy(path, file, sizeof(path));                                      \
            char const *basename = strrchr(path, '/') + 1;                          \
            if(basename == (char const *)1) {                                       \
                basename = path;                                                    \
            }                                                                       \
            snprintf(info[nfailed].summary, sizeof(info[nfailed].summary),          \
                        "| %s:%u: Assertion failure in %s:\n"                       \
                        "| '%s "#cmp" %s' with expansion\n"                         \
                        "| '"#fmt" "#cmp" "#fmt"'\n",                               \
                        basename, line, current_test, lhss, rhss, lhs, rhs);        \
            twinm_set_test_failure();                                               \
        }                                                                           \
    } while (0)

#define GENERATE_ASSERTION_DEFINITION3(opname, op, suffix, type, fmt)       \
    void twinm_assert_##opname##_##suffix(type lhs, type rhs,               \
                                      char const *lhss, char const *rhss,   \
                                      char const *file, unsigned line)      \
{ GENERIC_CMP_ASSERTION(lhs, rhs, lhss, rhss, file, line, fmt, op); }

#define GENERATE_ASSERTION_DEFINITION6(opname, op, suffix, type, fmt, ...)  \
    GENERATE_ASSERTION_DEFINITION3(opname, op, suffix, type, fmt)           \
    GENERATE_ASSERTION_DEFINITION3(opname, op, __VA_ARGS__)

#define GENERATE_ASSERTION_DEFINITION9(opname, op, suffix, type, fmt, ...)  \
    GENERATE_ASSERTION_DEFINITION3(opname, op, suffix, type, fmt)           \
    GENERATE_ASSERTION_DEFINITION6(opname, op, __VA_ARGS__)

#define GENERATE_ASSERTION_DEFINITION12(opname, op, suffix, type, fmt, ...) \
    GENERATE_ASSERTION_DEFINITION3(opname, op, suffix, type, fmt)           \
    GENERATE_ASSERTION_DEFINITION9(opname, op, __VA_ARGS__)

#define GENERATE_ASSERTION_DEFINITION15(opname, op, suffix, type, fmt, ...) \
    GENERATE_ASSERTION_DEFINITION3(opname, op, suffix, type, fmt)           \
    GENERATE_ASSERTION_DEFINITION12(opname, op, __VA_ARGS__)

/* Generate function definitions for internal assertion functions
 *
 * @opname: name of the comparison operation
 * @op:     the actual comparison operator
 * The variadic arguments should be given in pairs of identifier
 * for the name and the corresponding type
 *
 * As an example, GENERATE_ASSERTION_DEFINITIONS(eq, ==, char, char, ptr, void *);
 * will the prototypes
 * void twinm_assert_eq_char(char lhs, char rhs, char const *lhss, char const *rhss,
 *                           char const *file, unsigned line)
 * { GENERIC_CMP_ASSERTION(lhs, rhs, lhss, rhss, file, line, fmt, ==); }

 * void twinm_assert_eq_ptr(void *lhs, void *rhs, char const *lhss, char const *rhss,
 *                          char const *file, unsigned line)
 * { GENERIC_CMP_ASSERTION(lhs, rhs, lhss, rhss, file, line, fmt, ==); }

 * where
 * @lhs:  value on the left of the comparison
 * @rhs:  value on the right of the comparison operator
 * @lhss: the expression generating lhs, as a string
 * @rhss: the expression generating rhs, as a string
 * @file: the name of the file
 * @line: the line number
 */
#define GENERATE_ASSERTION_DEFINITIONS(opname, op, ...)                                         \
    CAT_EXPAND(GENERATE_ASSERTION_DEFINITION,nargs(__VA_ARGS__))(opname, op, __VA_ARGS__)    \
    void opname##_trailing_function_for_semicolon()

enum { TEST_INFO_ENTRY_SIZE = 256 };
enum { TEST_NAME_MAX_SIZE = 64 };
enum { FAILED_TEST_CAP_INC = 32 };
enum { MAX_PATH_SIZE = 128 };
enum { HEADING_UNDERLINE_LENGTH = 64 };

struct test_info {
    char summary[TEST_INFO_ENTRY_SIZE];
};

static unsigned nasserts = 0;
static unsigned nfailed = 0;
static unsigned ntests = 0;

static unsigned failed_capacity = 0;

static int current_status = 0;

static struct test_info *info = 0;

static char current_test[TEST_NAME_MAX_SIZE];

static void ensure_failure_log_capacity(void) {
    if(failed_capacity < nfailed + 1) {
        failed_capacity += FAILED_TEST_CAP_INC;
        info = realloc(info, failed_capacity * sizeof(*info));

        if(!info) {
            exit(1);
        }
    }
}

static void twinm_log_assertion_failure(char const *expr, char const *file, unsigned line) {
    ensure_failure_log_capacity();

    char path[MAX_PATH_SIZE];
    strscpy(path, file, sizeof(path));

    char const *basename = strrchr(path, '/') + 1;

    if(basename == (char const *)1) {
        basename = path;
    }

    snprintf(info[nfailed].summary, sizeof(info[nfailed].summary),
                "| %s:%u: Assertion failure in %s:\n"
                "| '%s'", basename, line, current_test, expr);
}

static void twinm_set_test_failure(void) {
    ++nfailed;
    current_status = 1;
}

void twinm_assert_internal(bool eval, char const *expr, char const *file, unsigned line) {
    ++nasserts;
    if(!eval) {
        twinm_log_assertion_failure(expr, file, line);
        twinm_set_test_failure();
    }
}

void twinm_test_register(char const *test_name) {
    ++ntests;
    current_status = 0;
    (void) strscpy(current_test, test_name, sizeof(current_test));
    printf("Running %-48s", test_name);
}

void twinm_print_heading(char const *heading) {
    char heading_underline[HEADING_UNDERLINE_LENGTH];
    size_t length = min(strlen(heading), HEADING_UNDERLINE_LENGTH - 1);

    memset(&heading_underline[0], '=', length);
    memset(&heading_underline[length], '\0', 1);
    printf("\n%s\n%s\n", heading, heading_underline);
}


void twinm_test_print_outcome(void) {
    if(!current_status) {
        puts("--> OK");
    }
    else {
        puts("--> FAIL");
    }
}

int twinm_test_summary(void) {
    char heading[TEST_NAME_MAX_SIZE];
    snprintf(heading, sizeof(heading), "Finished %u tests", ntests);
    twinm_print_heading(heading);

    if(nfailed) {
        printf("[%u/%u] assertions failed:\n", nfailed, nasserts);
        for(unsigned i = 0; i < nfailed; i++) {
            printf("%s\n\n", info[i].summary);
        }
    }
    else {
        printf("%u assertions passed\n", nasserts);
    }

    if(info) {
        free(info);
    }

    return nfailed;
}

GENERATE_ASSERTION_DEFINITIONS(eq, ==,
                               char, char, %c,
                               ptr, void const *, %p,
                               signed, long long, %lld,
                               unsigned, unsigned long long, %llu);

GENERATE_ASSERTION_DEFINITIONS(neq, !=,
                               char, char, %c,
                               ptr, void const *, %p,
                               signed, long long, %lld,
                               unsigned, unsigned long long, %llu);

GENERATE_ASSERTION_DEFINITIONS(lt, <,
                               char, char, %c,
                               ptr, void const *, %p,
                               signed, long long, %lld,
                               unsigned, unsigned long long, %llu,
                               double, double, %f);

GENERATE_ASSERTION_DEFINITIONS(gt, >,
                               char, char, %c,
                               ptr, void const *, %p,
                               signed, long long, %lld,
                               unsigned, unsigned long long, %llu,
                               double, double, %f);

GENERATE_ASSERTION_DEFINITIONS(leq, <=,
                               char, char, %c,
                               ptr, void const *, %p,
                               signed, long long, %lld,
                               unsigned, unsigned long long, %llu);

GENERATE_ASSERTION_DEFINITIONS(geq, >=,
                               char, char, %c,
                               ptr, void const *, %p,
                               signed, long long, %lld,
                               unsigned, unsigned long long, %llu);
