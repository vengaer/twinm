#ifndef REGEX_TEST_H
#define REGEX_TEST_H

void test_regex_compile(void);
void test_regex_compile_non_negated_char_class(void);
void test_regex_compile_negated_char_class(void);

void test_regex_match(void);
void test_regex_match_capture(void);
void test_regex_match_repeat_n(void);
void test_regex_match_repeat_n_to_m(void);
void test_regex_match_repeat_n_to_any(void);

void test_regex_match_back_to_back_captures(void);
void test_regex_match_capture_after_repetition(void);
void test_regex_match_repetition_in_capture(void);
void test_regex_match_empty_capture(void);

void test_regex_match_digit(void);
void test_regex_match_alnum(void);
void test_regex_match_whitespace(void);

void test_regex_match_non_digit(void);
void test_regex_match_non_alnum(void);
void test_regex_match_non_whitespace(void);

void test_regex_match_non_negated_char_class(void);
void test_regex_match_negated_char_class(void);

void test_regex_match_capture_char_class_repetition(void);
void test_regex_match_case_insensitive(void);
void test_regex_match_inverted(void);

void test_regex_match_combined(void);

void test_regex_match_nested_repetition(void);

void test_regex_guard(void);

#endif /* REGEX_TEST_H */
