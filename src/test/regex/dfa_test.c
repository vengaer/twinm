#include "dfa.h"
#include "dfa_test.h"
#include "macro.h"
#include "nfa.h"
#include "parser.h"
#include "twinm_test.h"

#include <string.h>

void test_regex_to_dfa(void) {
    char const *regex = "abcc?d*e(f|g)?h(aa|b)*";

    struct nfa nfa;
    struct dfa dfa;

    twinm_assert(nfa_init(&nfa));
    twinm_assert(dfa_init(&dfa));

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    twinm_assert_eq(nfa_to_dfa(&dfa, &nfa), 0);

    twinm_assert_eq(dfa_simulate(&dfa, "abcddefhaaaab", 0, 0), 0);
    twinm_assert_eq(dfa_simulate(&dfa, "abcceh", 0, 0), 0);
    twinm_assert_eq(dfa_simulate(&dfa, "abccddddddddddefhbbb", 0, 0), 0);
    twinm_assert_eq(dfa_simulate(&dfa, "abcdddehaaa", 0, 0), 0);
    twinm_assert_eq(dfa_simulate(&dfa, "gggggabcehaaaab", 0, 0), 0);
    twinm_assert_eq(dfa_simulate(&dfa, "abcehagghj", 0, 0), 0);

    twinm_assert(dfa_simulate(&dfa, "abcccddeh", 0, 0));

    nfa_free(&nfa);
    dfa_free(&dfa);
}

void test_regex_to_dfa_match_any(void) {
    char const *regex = "aab.+c.*";

    struct nfa nfa;
    struct dfa dfa;

    twinm_assert(nfa_init(&nfa));
    twinm_assert(dfa_init(&dfa));

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    twinm_assert_eq(nfa_to_dfa(&dfa, &nfa), 0);

    twinm_assert_eq(dfa_simulate(&dfa, "aabdc", 0, 0), 0);
    twinm_assert_eq(dfa_simulate(&dfa, "aabdasdfssccdfasdfasdfasdfec", 0, 0), 0);
    twinm_assert_eq(dfa_simulate(&dfa, "aabdddcasdf", 0, 0), 0);

    twinm_assert(dfa_simulate(&dfa, "aabc", 0, 0));
    twinm_assert(dfa_simulate(&dfa, "aabcasdf", 0, 0));

    nfa_free(&nfa);
    dfa_free(&dfa);
}

void test_regex_to_dfa_escaped_char(void) {
    char const *regex = "aab\\+d\\\\.+";

    struct nfa nfa;
    struct dfa dfa;

    twinm_assert(nfa_init(&nfa));
    twinm_assert(dfa_init(&dfa));

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    twinm_assert_eq(nfa_to_dfa(&dfa, &nfa), 0);

    twinm_assert_eq(dfa_simulate(&dfa, "aab+d\\aa\\\\", 0, 0), 0);

    nfa_free(&nfa);
    dfa_free(&dfa);
}

void test_dfa_anchor_start(void) {
    struct nfa nfa;
    struct dfa dfa;

    twinm_assert(nfa_init(&nfa));
    twinm_assert(dfa_init(&dfa));

    twinm_assert_eq(regex_to_nfa(&nfa, "^aaba", 0), 0);
    twinm_assert_eq(nfa_to_dfa(&dfa, &nfa), 0);

    twinm_assert_eq(dfa_simulate(&dfa, "aabasdf", 0, 0), 0);

    twinm_assert(dfa_simulate(&dfa, "caabasdf", 0, 0));

    twinm_assert_lt(regex_to_nfa(&nfa, "aa^ba", 0), 0);

    nfa_free(&nfa);
    dfa_free(&dfa);
}

void test_dfa_anchor_end(void) {
    struct nfa nfa;
    struct dfa dfa;

    twinm_assert(nfa_init(&nfa));
    twinm_assert(dfa_init(&dfa));

    twinm_assert_eq(regex_to_nfa(&nfa, "aaba$", 0), 0);
    twinm_assert_eq(nfa_to_dfa(&dfa, &nfa), 0);

    twinm_assert_eq(dfa_simulate(&dfa, "aaba", 0, 0), 0);

    twinm_assert(dfa_simulate(&dfa, "asdfaabaasd", 0, 0));

    twinm_assert_lt(regex_to_nfa(&nfa, "aa$ba", 0), 0);

    nfa_free(&nfa);
    dfa_free(&dfa);

}
