#include "macro.h"
#include "regex.h"
#include "regex_test.h"
#include "twinm_test.h"

#include <stdio.h>

enum { SIZE = 128 };

void test_regex_compile(void) {
    regex_handle handle;
    twinm_assert_eq(regex_compile(&handle, "ab*.+(aa|dfw)?a?ghc.*", 0), 0);
    regex_free(&handle);

    twinm_assert_lt(regex_compile(&handle, "ab*.+((aa|d)?a", 0), 0);
    twinm_assert_lt(regex_compile(&handle, "?asdf", 0), 0);
    twinm_assert_lt(regex_compile(&handle, ")asdfss?", 0), 0);
}

void test_regex_compile_non_negated_char_class(void) {
    regex_handle handle;

    twinm_assert_eq(regex_compile(&handle, "ab[a-zA-Z0-9]", 0), 0);
    regex_free(&handle);

    twinm_assert_eq(regex_compile(&handle, "ab[g-hR-Z1-2]", 0), 0);
    regex_free(&handle);

    twinm_assert(regex_compile(&handle, "ab[z-a]", 0));
    twinm_assert(regex_compile(&handle, "ab[a-zA-Z0-0]", 0));
    twinm_assert(regex_compile(&handle, "ab[]", 0));
}

void test_regex_compile_negated_char_class(void) {
    regex_handle handle;

    twinm_assert_eq(regex_compile(&handle, "ab[^a-zA-Z0-9]", 0), 0);
    regex_free(&handle);

    twinm_assert_eq(regex_compile(&handle, "ab[^g-hR-Z1-2]", 0), 0);
    regex_free(&handle);

    twinm_assert(regex_compile(&handle, "ab[^z-a]", 0));
    twinm_assert(regex_compile(&handle, "ab[^a-zA-Z0-0]", 0));
    twinm_assert(regex_compile(&handle, "ab[^]", 0));
    twinm_assert(regex_compile(&handle, "ab[a-z", 0));
}

void test_regex_match(void) {
    regex_handle handle;
    twinm_assert_eq(regex_compile(&handle, "ab.+(aa|dfw)?df*c\\.+", 0), 0);

    twinm_assert_eq(regex_match(&handle, "abcdefgdfwdffffc....", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abcaadc.", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abadfc...", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abcaadfwdc..", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abcdfwdfwdffc.............", 0, 0), 0);

    twinm_assert_neq(regex_match(&handle, "abadffcd", 0, 0), 0);
    twinm_assert_neq(regex_match(&handle, "abadffcc", 0, 0), 0);
    twinm_assert_neq(regex_match(&handle, "abasdfaadfffc", 0, 0), 0);

    regex_free(&handle);
}

void test_regex_match_capture(void) {
    char regex[SIZE];
    char input[SIZE];

    regex_handle handle;
    regex_capture captures[3];

    snprintf(regex, sizeof(regex), "a(bc)*a");
    snprintf(input, sizeof(input), "abcbca");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);
    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 6);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 3);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "a(b(c))+d");
    snprintf(input, sizeof(input), "abcbcd");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);
    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 6);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 3);

    twinm_assert_eq(captures[2].start, input + 2);
    twinm_assert_eq(captures[2].end, input + 3);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "a(((b)))c");
    snprintf(input, sizeof(input), "abc");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);
    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 3);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 2);

    twinm_assert_eq(captures[2].start, input + 1);
    twinm_assert_eq(captures[2].end, input + 2);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "a(bc)+a");
    snprintf(input, sizeof(input), "abcbca");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);
    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 6);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 3);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "a(b*|c*)d(e?f+)g");
    snprintf(input, sizeof(input), "abdeffg");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 7);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 2);

    twinm_assert_eq(captures[2].start, input + 3);
    twinm_assert_eq(captures[2].end, input + 6);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "a(bc)d(efg)");
    snprintf(input, sizeof(input), "abcdefg");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 7);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 3);

    twinm_assert_eq(captures[2].start, input + 4);
    twinm_assert_eq(captures[2].end, input + 7);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "a(b(cd)*e)f");
    snprintf(input, sizeof(input), "abcdef");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 6);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 5);

    twinm_assert_eq(captures[2].start, input + 2);
    twinm_assert_eq(captures[2].end, input + 4);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "a(bc)*d");
    snprintf(input, sizeof(input), "abcd");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 4);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 3);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "a(b(c)d)e");
    snprintf(input, sizeof(input), "abcde");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 5);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 4);

    twinm_assert_eq(captures[2].start, input + 2);
    twinm_assert_eq(captures[2].end, input + 3);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "a(bc)+d(efg)+");
    snprintf(input, sizeof(input), "abcdefg");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 7);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 3);

    twinm_assert_eq(captures[2].start, input + 4);
    twinm_assert_eq(captures[2].end, input + 7);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "a(bc)+d");
    snprintf(input, sizeof(input), "abcd");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 4);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 3);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "a(b*|c*)d(e?f+)g");
    snprintf(input, sizeof(input), "adfg");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 4);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 1);

    twinm_assert_eq(captures[2].start, input + 2);
    twinm_assert_eq(captures[2].end, input + 3);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "a(b|c*)d(e?f+ijk)g(lm)");
    snprintf(input, sizeof(input), "abdeffijkglm");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 12);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 2);

    twinm_assert_eq(captures[2].start, input + 3);
    twinm_assert_eq(captures[2].end, input + 9);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "d(e?f+)g");
    snprintf(input, sizeof(input), "deffg");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 5);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 4);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "a(bc)?d");
    snprintf(input, sizeof(input), "ad");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 2);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 1);

    regex_free(&handle);


    snprintf(regex, sizeof(regex), "a(bc)?d");
    snprintf(input, sizeof(input), "abcd");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 4);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 3);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "a(bc)d");
    snprintf(input, sizeof(input), "abcd");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 4);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 3);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "a(c*|b)a");
    snprintf(input, sizeof(input), "aba");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);
    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 3);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 2);

    regex_free(&handle);
}

void test_regex_match_repeat_n(void) {
    regex_handle handle;

    twinm_assert_eq(regex_compile(&handle, "aba{3}", 0), 0);
    twinm_assert_eq(regex_match(&handle, "abaaa", 0, 0), 0);

    regex_free(&handle);

    twinm_assert_eq(regex_compile(&handle, "ab(ab){3}", 0), 0);
    twinm_assert_eq(regex_match(&handle, "abababab", 0, 0), 0);

    regex_free(&handle);

    twinm_assert_eq(regex_compile(&handle, "ab(a|b){3}", 0), 0);
    twinm_assert_eq(regex_match(&handle, "abaaa", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "ababa", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abbbb", 0, 0), 0);

    regex_free(&handle);

    /* Malformed expressions */
    twinm_assert(regex_compile(&handle, "ab{}", 0));
    twinm_assert(regex_compile(&handle, "{2}", 0));
    twinm_assert(regex_compile(&handle, "b{0}", 0));
}

void test_regex_match_repeat_n_to_m(void) {
    regex_handle handle;

    twinm_assert_eq(regex_compile(&handle, "aba{3,5}", 0), 0);
    twinm_assert_eq(regex_match(&handle, "abaaa", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abaaaaa", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abaaaa", 0, 0), 0);

    twinm_assert(regex_match(&handle, "abaa", 0, 0));

    regex_free(&handle);

    twinm_assert_eq(regex_compile(&handle, "ab(ab){1,5}", 0), 0);
    twinm_assert_eq(regex_match(&handle, "abab", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "ababab", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abababab", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abababababab", 0, 0), 0);

    twinm_assert(regex_match(&handle, "ab", 0, 0));

    regex_free(&handle);

    twinm_assert_eq(regex_compile(&handle, "ab(a|b){1,3}", 0), 0);
    twinm_assert_eq(regex_match(&handle, "abaaa", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "ababa", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abbbb", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abb", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abab", 0, 0), 0);

    twinm_assert(regex_match(&handle, "ab", 0, 0));

    regex_free(&handle);

    twinm_assert_eq(regex_compile(&handle, "ab(df){0,2}$", 0), 0);
    twinm_assert_eq(regex_match(&handle, "ab", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abdf", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abdfdf", 0, 0), 0);

    twinm_assert(regex_match(&handle, "abdfdfdf", 0, 0));

    regex_free(&handle);

    twinm_assert(regex_compile(&handle, "abd{2,1}", 0));
}

void test_regex_match_repeat_n_to_any(void) {
    regex_handle handle;

    twinm_assert_eq(regex_compile(&handle, "aba{1,}$", 0), 0);
    twinm_assert_eq(regex_match(&handle, "aba", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abaaa", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abaaaaaaaaaaaaaaaaaaaaa", 0, 0), 0);

    twinm_assert(regex_match(&handle, "ab", 0, 0));
    twinm_assert(regex_match(&handle, "abaaaab", 0, 0));

    regex_free(&handle);

    twinm_assert_eq(regex_compile(&handle, "ab(ab){1,}$", 0), 0);
    twinm_assert_eq(regex_match(&handle, "abab", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "ababababababababab", 0, 0), 0);

    twinm_assert(regex_match(&handle, "ababa", 0, 0));

    regex_free(&handle);

    twinm_assert_eq(regex_compile(&handle, "ab(a|b){1,}$", 0), 0);
    twinm_assert_eq(regex_match(&handle, "abaaaaabbba", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "ababababababababab", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "aba", 0, 0), 0);

    regex_free(&handle);

    /* Identical to "ab(a|b)*$" */
    twinm_assert_eq(regex_compile(&handle, "ab(a|b){,}$", 0), 0);

    twinm_assert_eq(regex_match(&handle, "abaaaaabbba", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "ababababababababab", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "aba", 0, 0), 0);

    regex_free(&handle);

    twinm_assert(regex_compile(&handle, "ab{-2}", 0));
}

void test_regex_match_back_to_back_captures(void) {
    char input[SIZE];
    char regex[SIZE];

    regex_handle handle;
    regex_capture captures[3];

    snprintf(input, sizeof(input), "ab");
    snprintf(regex, sizeof(regex), "(a|c)(b)");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 2);

    twinm_assert_eq(captures[1].start, input);
    twinm_assert_eq(captures[1].end, input + 1);

    twinm_assert_eq(captures[2].start, input + 1);
    twinm_assert_eq(captures[2].end, input + 2);

    regex_free(&handle);
}

void test_regex_match_capture_after_repetition(void) {
    char input[SIZE];
    char regex[SIZE];

    regex_handle handle;
    regex_capture captures[2];

    snprintf(input, sizeof(input), "ab");
    snprintf(regex, sizeof(regex), "a?(b)");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 2);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 2);

    regex_free(&handle);

    snprintf(input, sizeof(input), "aab");
    snprintf(regex, sizeof(regex), "a*(b)");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 3);

    twinm_assert_eq(captures[1].start, input + 2);
    twinm_assert_eq(captures[1].end, input + 3);

    regex_free(&handle);

    snprintf(input, sizeof(input), "aab");
    snprintf(regex, sizeof(regex), "a+(b)");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 3);

    twinm_assert_eq(captures[1].start, input + 2);
    twinm_assert_eq(captures[1].end, input + 3);

    regex_free(&handle);
}

void test_regex_match_repetition_in_capture(void) {
    char input[SIZE];
    char regex[SIZE];

    regex_handle handle;
    regex_capture captures[3];

    snprintf(input, sizeof(input), "abbbb");
    snprintf(regex, sizeof(regex), "a((b{2}){2})");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 5);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 5);

    twinm_assert_eq(captures[2].start, input + 3);
    twinm_assert_eq(captures[2].end, input + 5);

    regex_free(&handle);

    snprintf(input, sizeof(input), "abbbb");
    snprintf(regex, sizeof(regex), "a((bb)+)");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 5);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 5);

    twinm_assert_eq(captures[2].start, input + 1);
    twinm_assert_eq(captures[2].end, input + 3);

    regex_free(&handle);

    snprintf(input, sizeof(input), "abbbbc");
    snprintf(regex, sizeof(regex), "a((bb)+)c");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 6);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 5);

    twinm_assert_eq(captures[2].start, input + 1);
    twinm_assert_eq(captures[2].end, input + 3);

    regex_free(&handle);
}

void test_regex_match_empty_capture(void) {
    char input[SIZE];
    char regex[SIZE];

    regex_handle handle;
    regex_capture captures[2];

    snprintf(regex, sizeof(regex), "ab(c?)d");
    snprintf(input, sizeof(input), "abd");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);
    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);
    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 3);
    twinm_assert_eq(captures[1].start, input + 2);
    twinm_assert_eq(captures[1].end, input + 2);

    regex_free(&handle);
}

void test_regex_match_digit(void) {
    regex_handle handle;

    twinm_assert_eq(regex_compile(&handle, "aba\\d+a", 0), 0);
    twinm_assert_eq(regex_match(&handle, "aba14360a", 0, 0), 0);

    twinm_assert(regex_match(&handle, "ababa", 0, 0));

    regex_free(&handle);
}

void test_regex_match_alnum(void) {
    regex_handle handle;

    twinm_assert_eq(regex_compile(&handle, "aba \\w+ a", 0), 0);
    twinm_assert_eq(regex_match(&handle, "aba asdfasdf993_ a", 0, 0), 0);

    twinm_assert(regex_match(&handle, "aba  aa a", 0, 0));

    regex_free(&handle);
}

void test_regex_match_whitespace(void) {
    regex_handle handle;

    twinm_assert_eq(regex_compile(&handle, "aba\\s+a", 0), 0);
    twinm_assert_eq(regex_match(&handle, "aba asdfasdf993_ a", 0, 0), 0);

    twinm_assert(regex_match(&handle, "abaa", 0, 0));

    regex_free(&handle);
}

void test_regex_match_non_digit(void) {
    regex_handle handle;

    twinm_assert_eq(regex_compile(&handle, "ab0\\D+1a", 0), 0);
    twinm_assert_eq(regex_match(&handle, "ab0aab#__!1a", 0, 0), 0);

    twinm_assert(regex_match(&handle, "ab0bbd1nb1a", 0, 0));

    regex_free(&handle);
}

void test_regex_match_non_alnum(void) {
    regex_handle handle;

    twinm_assert_eq(regex_compile(&handle, "ab0\\W+1a", 0), 0);
    twinm_assert_eq(regex_match(&handle, "ab0##!\"1a", 0, 0), 0);

    twinm_assert(regex_match(&handle, "ab0##a1#1a", 0, 0));

    regex_free(&handle);
}

void test_regex_match_non_whitespace(void) {
    regex_handle handle;

    twinm_assert_eq(regex_compile(&handle, "ab0\\S+1a", 0), 0);
    twinm_assert_eq(regex_match(&handle, "ab0##!\"1a", 0, 0), 0);

    twinm_assert(regex_match(&handle, "ab0b c1a", 0, 0));

    regex_free(&handle);
}

void test_regex_match_non_negated_char_class(void) {
    char regex[SIZE];

    regex_handle handle;

    snprintf(regex, sizeof(regex), "ab[c-zA-Zab!#_\\-98c]");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, "abc", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abG", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "ab!", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "ab-", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "ab8", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abd", 0, 0), 0);

    twinm_assert(regex_match(&handle, "ab ", 0, 0));
    twinm_assert(regex_match(&handle, "ab&", 0, 0));
    twinm_assert(regex_match(&handle, "ab*", 0, 0));
    twinm_assert(regex_match(&handle, "Gbc", 0, 0));

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "ab[d-uA-G0-9]+");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, "abd", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abdd932a", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "abABCZDFgh1", 0, 0), 0);

    twinm_assert(regex_match(&handle, "ab_ABCZDFgh1", 0, 0));
    twinm_assert(regex_match(&handle, "abaABCZDFgh1", 0, 0));

    regex_free(&handle);
}

void test_regex_match_negated_char_class(void) {
    char regex[SIZE];

    regex_handle handle;

    snprintf(regex, sizeof(regex), "ab[^c-zA-Zab!#_\\-98c]");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, "ab ", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "ab&", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "ab*", 0, 0), 0);

    twinm_assert(regex_match(&handle, "abc", 0, 0));
    twinm_assert(regex_match(&handle, "abG", 0, 0));
    twinm_assert(regex_match(&handle, "ab!", 0, 0));
    twinm_assert(regex_match(&handle, "ab-", 0, 0));
    twinm_assert(regex_match(&handle, "ab8", 0, 0));
    twinm_assert(regex_match(&handle, "abd", 0, 0));
    twinm_assert(regex_match(&handle, "Gbc", 0, 0));

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "ab[^d-uA-G0-9]+");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, "ab_ZXYa", 0, 0), 0);
    twinm_assert_eq(regex_match(&handle, "ababcz", 0, 0), 0);

    twinm_assert(regex_match(&handle, "abd", 0, 0));
    twinm_assert(regex_match(&handle, "abdd932a", 0, 0));
    twinm_assert(regex_match(&handle, "abABCZDFgh1", 0, 0));

    regex_free(&handle);
}

void test_regex_match_capture_char_class_repetition(void) {
    char regex[SIZE];
    char input[SIZE];

    regex_handle handle;
    regex_capture captures[2];

    snprintf(regex, sizeof(regex), "Ab(c[d-h]{2,3})");
    snprintf(input, sizeof(input), "zAbcdef");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 7);

    twinm_assert_eq(captures[1].start, input + 3);
    twinm_assert_eq(captures[1].end, input + 7);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "Ab(c[d-h]{2,3})i");
    snprintf(input, sizeof(input), "Abcdefgi");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);

    twinm_assert(regex_match(&handle, input, captures, array_size(captures)));

    regex_free(&handle);
}

void test_regex_match_case_insensitive(void) {
    char regex[SIZE];
    char input[SIZE];

    regex_handle handle;

    snprintf(regex, sizeof(regex), "AbcE(gh)+Z");
    snprintf(input, sizeof(input), "abCeGhghz");

    twinm_assert_eq(regex_compile(&handle, regex, REGEX_FLAG_IGNORE_CASE), 0);

    twinm_assert_eq(regex_match(&handle, input, 0, 0), 0);

    regex_free(&handle);
}

void test_regex_match_inverted(void) {
    char regex[SIZE];
    char input[SIZE];

    regex_handle handle;

    snprintf(regex, sizeof(regex), "abce(gh)*z");
    snprintf(input, sizeof(input), "abcez");

    twinm_assert_eq(regex_compile(&handle, regex, REGEX_FLAG_INVERT_MATCH), 0);

    twinm_assert(regex_match(&handle, input, 0, 0));

    snprintf(input, sizeof(input), "abceaz");

    twinm_assert_eq(regex_match(&handle, input, 0, 0), 0);

    regex_free(&handle);
}

void test_regex_match_combined(void) {
    char regex[SIZE];
    char input[SIZE];

    regex_handle handle;
    regex_capture captures[4];

    snprintf(input, sizeof(input), "[2020-09-05 WARNING]: !some warning");
    snprintf(regex, sizeof(regex), "\\[\\d+((-\\d{2}){2})\\s+\\w+\\]:\\s+!(.*)$");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);
    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 35);

    twinm_assert_eq(captures[1].start, input + 5);
    twinm_assert_eq(captures[1].end, input + 11);

    twinm_assert_eq(captures[2].start, input + 8);
    twinm_assert_eq(captures[2].end, input + 11);

    twinm_assert_eq(captures[3].start, input + 23);
    twinm_assert_eq(captures[3].end, input + 35);

    regex_free(&handle);

    snprintf(input, sizeof(input), "[2020-09-05 WARNING]: !some warning");
    snprintf(regex, sizeof(regex), "\\[\\d+((-\\d{1,2}){2})\\s+\\w+\\]:\\s+!(.*)$");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);
    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 35);

    twinm_assert_eq(captures[1].start, input + 5);
    twinm_assert_eq(captures[1].end, input + 11);

    twinm_assert_eq(captures[2].start, input + 8);
    twinm_assert_eq(captures[2].end, input + 11);

    twinm_assert_eq(captures[3].start, input + 23);
    twinm_assert_eq(captures[3].end, input + 35);

    regex_free(&handle);

}

void test_regex_match_nested_repetition(void) {
    char regex[SIZE];
    char input[SIZE];

    regex_handle handle;
    regex_capture captures[3];

    snprintf(regex, sizeof(regex), "a((b+){2})");
    snprintf(input, sizeof(input), "abbbb");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);
    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 5);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 5);

    twinm_assert_eq(captures[2].start, input + 4);
    twinm_assert_eq(captures[2].end, input + 5);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "a((b*){2})");
    snprintf(input, sizeof(input), "abbbb");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);
    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 5);

    twinm_assert_eq(captures[1].start, input + 4);
    twinm_assert_eq(captures[1].end, input + 5);

    twinm_assert_eq(captures[2].start, input + 4);
    twinm_assert_eq(captures[2].end, input + 5);

    regex_free(&handle);

    snprintf(regex, sizeof(regex), "a((b{1,}){2})");
    snprintf(input, sizeof(input), "abbbb");

    twinm_assert_eq(regex_compile(&handle, regex, 0), 0);
    twinm_assert_eq(regex_match(&handle, input, captures, array_size(captures)), 0);

    twinm_assert_eq(captures[0].start, input);
    twinm_assert_eq(captures[0].end, input + 5);

    twinm_assert_eq(captures[1].start, input + 1);
    twinm_assert_eq(captures[1].end, input + 5);

    twinm_assert_eq(captures[2].start, input + 4);
    twinm_assert_eq(captures[2].end, input + 5);

    regex_free(&handle);
}

void test_regex_guard(void) {
    regex_handle handle;
    int status = 0;

    twinm_assert_eq(status, 0);
    regex_guard(&handle, "abc+d", 0, &status) {
        twinm_assert_neq(status, 0);

        twinm_assert_eq(regex_match(&handle, "abccccd", 0, 0), 0);
    }

    twinm_assert_eq(status, 0);
}
