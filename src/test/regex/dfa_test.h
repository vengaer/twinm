#ifndef DFA_TEST_H
#define DFA_TEST_H

void test_regex_to_dfa(void);
void test_regex_to_dfa_match_any(void);
void test_regex_to_dfa_escaped_char(void);

void test_dfa_anchor_start(void);
void test_dfa_anchor_end(void);

#endif /* DFA_TEST_H */
