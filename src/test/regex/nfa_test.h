#ifndef NFA_TEST_H
#define NFA_TEST_H

void test_nfa_to_regex(void);
void test_nfa_to_regex_overflow(void);
void test_nfa_to_regex_nested_parens(void);
void test_regex_to_nfa_rep_qm(void);
void test_regex_to_nfa_rep_ps(void);
void test_regex_to_nfa_rep_ax(void);
void test_regex_to_nfa_alternation(void);
void test_regex_to_nfa_nested_alternation(void);
void test_regex_to_nfa_repetition_in_alternation(void);
void test_regex_to_nfa_escaped_char(void);
void test_regex_to_nfa(void);

void test_regex_to_nfa_repeat_n(void);
void test_regex_to_nfa_repeat_n_to_m(void);
void test_regex_to_nfa_repeat_n_to_any(void);

void test_regex_to_nfa_digit_metachar(void);
void test_regex_to_nfa_alnum_metachar(void);
void test_regex_to_nfa_whitespace_metachar(void);

void test_regex_to_nfa_non_digit_metachar(void);
void test_regex_to_nfa_non_alnum_metachar(void);
void test_regex_to_nfa_non_whitespace_metachar(void);

void test_nfa_epsilon_closure(void);

#endif /* NFA_TEST_H */
