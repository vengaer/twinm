#include "charcodes.h"
#include "list.h"
#include "nfa.h"
#include "nfa_test.h"
#include "parser.h"
#include "twinm_test.h"
#include "twinm_types.h"

#include <stddef.h>
#include <stdio.h>
#include <string.h>

void test_nfa_to_regex(void) {
    enum { SIZE = 256 };

    char buffer[SIZE];

    struct nfa automaton;
    struct nfa_state state_pool[64];
    automaton.state_pool = &state_pool[0];

    automaton.state_pool[0].charcode = 'a';
    automaton.state_pool[1].charcode = regex_charcode_repeat_one_any;
    automaton.state_pool[2].charcode = 'b';
    automaton.state_pool[3].charcode = regex_charcode_split;
    automaton.state_pool[4].charcode = regex_charcode_repeat_zero_one;
    automaton.state_pool[5].charcode = 'c';
    automaton.state_pool[6].charcode = regex_charcode_repeat_zero_any;
    automaton.state_pool[7].charcode = 'd';
    automaton.state_pool[8].charcode = regex_charcode_alt;
    automaton.state_pool[9].charcode = 'e';
    automaton.state_pool[10].charcode = 'f';
    automaton.state_pool[11].charcode = '+';
    automaton.state_pool[12].charcode = regex_charcode_repeat_zero_any;
    automaton.state_pool[13].charcode = regex_charcode_match_any;
    automaton.state_pool[14].charcode = regex_charcode_repeat_zero_one;
    automaton.state_pool[15].charcode = regex_charcode_alt;
    automaton.state_pool[16].charcode = 'a';
    automaton.state_pool[17].charcode = 'b';
    automaton.state_pool[18].charcode = regex_charcode_repeat_one_any;
    automaton.state_pool[19].charcode = 'a';
    automaton.state_pool[20].charcode = 'b';
    automaton.state_pool[21].charcode = 'a';
    automaton.state_pool[22].charcode = regex_charcode_split;
    automaton.state_pool[23].charcode = regex_charcode_repeat_zero_any;
    automaton.state_pool[24].charcode = regex_charcode_alt;
    automaton.state_pool[25].charcode = regex_charcode_alt;
    automaton.state_pool[26].charcode = 'a';
    automaton.state_pool[27].charcode = 'a';
    automaton.state_pool[28].charcode = 'c';
    automaton.state_pool[29].charcode = 'd';
    automaton.state_pool[30].charcode = regex_charcode_alt;
    automaton.state_pool[31].charcode = regex_charcode_repeat_one_any;
    automaton.state_pool[32].charcode = 'a';
    automaton.state_pool[33].charcode = regex_charcode_split;
    automaton.state_pool[34].charcode = 'b';
    automaton.state_pool[35].charcode = regex_charcode_accept;

    automaton.start_index = 0;

    for(unsigned i = 0; i < 36; i++) {
        memset(&automaton.state_pool[i].edges[0], 0, sizeof(automaton.state_pool[i].edges));
    }

    list_push_back(&automaton.state_pool[0].edges[0],  &automaton.state_pool[1].edges[0]);
    list_push_back(&automaton.state_pool[1].edges[0],  &automaton.state_pool[2].edges[0]);
    list_push_back(&automaton.state_pool[2].edges[0],  &automaton.state_pool[3].edges[0]);
    list_push_back(&automaton.state_pool[3].edges[0],  &automaton.state_pool[4].edges[0]);
    list_push_back(&automaton.state_pool[3].edges[1],  &automaton.state_pool[2].edges[0]);
    list_push_back(&automaton.state_pool[4].edges[0],  &automaton.state_pool[5].edges[0]);
    list_push_back(&automaton.state_pool[4].edges[1],  &automaton.state_pool[6].edges[0]);
    list_push_back(&automaton.state_pool[5].edges[0],  &automaton.state_pool[6].edges[0]);
    list_push_back(&automaton.state_pool[6].edges[0],  &automaton.state_pool[8].edges[0]);
    list_push_back(&automaton.state_pool[6].edges[1],  &automaton.state_pool[7].edges[0]);
    list_push_back(&automaton.state_pool[7].edges[0],  &automaton.state_pool[6].edges[0]);
    list_push_back(&automaton.state_pool[8].edges[0],  &automaton.state_pool[9].edges[0]);
    list_push_back(&automaton.state_pool[8].edges[1],  &automaton.state_pool[10].edges[0]);
    list_push_back(&automaton.state_pool[9].edges[0],  &automaton.state_pool[11].edges[0]);
    list_push_back(&automaton.state_pool[10].edges[0], &automaton.state_pool[11].edges[0]);
    list_push_back(&automaton.state_pool[11].edges[0], &automaton.state_pool[12].edges[0]);
    list_push_back(&automaton.state_pool[12].edges[0], &automaton.state_pool[14].edges[0]);
    list_push_back(&automaton.state_pool[12].edges[1], &automaton.state_pool[13].edges[0]);
    list_push_back(&automaton.state_pool[13].edges[0], &automaton.state_pool[12].edges[0]);
    list_push_back(&automaton.state_pool[14].edges[0], &automaton.state_pool[15].edges[0]);
    list_push_back(&automaton.state_pool[14].edges[1], &automaton.state_pool[18].edges[0]);
    list_push_back(&automaton.state_pool[15].edges[0], &automaton.state_pool[16].edges[0]);
    list_push_back(&automaton.state_pool[15].edges[1], &automaton.state_pool[17].edges[0]);
    list_push_back(&automaton.state_pool[16].edges[0], &automaton.state_pool[18].edges[0]);
    list_push_back(&automaton.state_pool[17].edges[0], &automaton.state_pool[18].edges[0]);
    list_push_back(&automaton.state_pool[18].edges[0], &automaton.state_pool[19].edges[0]);
    list_push_back(&automaton.state_pool[19].edges[0], &automaton.state_pool[20].edges[0]);
    list_push_back(&automaton.state_pool[20].edges[0], &automaton.state_pool[21].edges[0]);
    list_push_back(&automaton.state_pool[21].edges[0], &automaton.state_pool[22].edges[0]);
    list_push_back(&automaton.state_pool[22].edges[0], &automaton.state_pool[23].edges[0]);
    list_push_back(&automaton.state_pool[22].edges[1], &automaton.state_pool[19].edges[0]);
    list_push_back(&automaton.state_pool[23].edges[0], &automaton.state_pool[30].edges[0]);
    list_push_back(&automaton.state_pool[23].edges[1], &automaton.state_pool[24].edges[0]);
    list_push_back(&automaton.state_pool[24].edges[0], &automaton.state_pool[25].edges[0]);
    list_push_back(&automaton.state_pool[24].edges[1], &automaton.state_pool[29].edges[0]);
    list_push_back(&automaton.state_pool[25].edges[0], &automaton.state_pool[26].edges[0]);
    list_push_back(&automaton.state_pool[25].edges[1], &automaton.state_pool[28].edges[0]);
    list_push_back(&automaton.state_pool[26].edges[0], &automaton.state_pool[27].edges[0]);
    list_push_back(&automaton.state_pool[27].edges[0], &automaton.state_pool[23].edges[0]);
    list_push_back(&automaton.state_pool[28].edges[0], &automaton.state_pool[23].edges[0]);
    list_push_back(&automaton.state_pool[29].edges[0], &automaton.state_pool[23].edges[0]);
    list_push_back(&automaton.state_pool[30].edges[0], &automaton.state_pool[31].edges[0]);
    list_push_back(&automaton.state_pool[30].edges[1], &automaton.state_pool[34].edges[0]);
    list_push_back(&automaton.state_pool[31].edges[0], &automaton.state_pool[32].edges[0]);
    list_push_back(&automaton.state_pool[32].edges[0], &automaton.state_pool[33].edges[0]);
    list_push_back(&automaton.state_pool[33].edges[0], &automaton.state_pool[35].edges[0]);
    list_push_back(&automaton.state_pool[33].edges[1], &automaton.state_pool[32].edges[0]);
    list_push_back(&automaton.state_pool[34].edges[0], &automaton.state_pool[35].edges[0]);

    ssize_t size = nfa_to_regex(buffer, &automaton, sizeof buffer);
    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(buffer));
    twinm_assert_eq(strcmp(buffer, "ab+c?d*(e|f)\\+.*(a|b)?(aba)+((aa|c)|d)*(a+|b)"), 0);
}

void test_nfa_to_regex_overflow(void) {
    enum { SIZE = 4 };
    char buffer[SIZE];

    struct nfa automaton;
    struct nfa_state state_pool[16];
    automaton.state_pool = &state_pool[0];

    automaton.state_pool[0].charcode = 'a';
    automaton.state_pool[1].charcode = 'b';
    automaton.state_pool[2].charcode = 'c';
    automaton.state_pool[3].charcode = 'd';
    automaton.state_pool[4].charcode = regex_charcode_accept;

    automaton.start_index = 0;

    for(unsigned i = 0; i < 5; i++) {
        memset(&automaton.state_pool[i].edges[0], 0, sizeof(automaton.state_pool[i].edges));
    }

    list_push_back(&automaton.state_pool[0].edges[0], &automaton.state_pool[1].edges[0]);
    list_push_back(&automaton.state_pool[1].edges[0], &automaton.state_pool[2].edges[0]);
    list_push_back(&automaton.state_pool[2].edges[0], &automaton.state_pool[3].edges[0]);
    list_push_back(&automaton.state_pool[3].edges[0], &automaton.state_pool[4].edges[0]);

    twinm_assert_lt(nfa_to_regex(buffer, &automaton, sizeof buffer), 0);
    twinm_assert_eq(buffer[0], '\0');
}

void test_nfa_to_regex_nested_parens(void) {
    enum { SIZE = 64 };
    char buffer[SIZE];

    struct nfa automaton;
    struct nfa_state state_pool[16];
    automaton.state_pool = &state_pool[0];

    automaton.state_pool[0].charcode = regex_charcode_alt;
    automaton.state_pool[1].charcode = 'a';
    automaton.state_pool[2].charcode = regex_charcode_alt;
    automaton.state_pool[3].charcode = 'a';
    automaton.state_pool[4].charcode = 'b';
    automaton.state_pool[5].charcode = 'a';
    automaton.state_pool[6].charcode = regex_charcode_accept;

    automaton.start_index = 0;

    for(unsigned i = 0; i < 7; i++) {
        memset(&automaton.state_pool[i].edges[0], 0, sizeof(automaton.state_pool[i].edges));
    }

    list_push_back(&automaton.state_pool[0].edges[0], &automaton.state_pool[1].edges[0]);
    list_push_back(&automaton.state_pool[0].edges[1], &automaton.state_pool[2].edges[0]);
    list_push_back(&automaton.state_pool[1].edges[0], &automaton.state_pool[6].edges[0]);
    list_push_back(&automaton.state_pool[2].edges[0], &automaton.state_pool[3].edges[0]);
    list_push_back(&automaton.state_pool[2].edges[1], &automaton.state_pool[4].edges[0]);
    list_push_back(&automaton.state_pool[3].edges[0], &automaton.state_pool[5].edges[0]);
    list_push_back(&automaton.state_pool[4].edges[0], &automaton.state_pool[5].edges[0]);
    list_push_back(&automaton.state_pool[5].edges[0], &automaton.state_pool[6].edges[0]);

    ssize_t size = nfa_to_regex(buffer, &automaton, sizeof buffer);
    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(buffer));
    twinm_assert_eq(strcmp(buffer, "(a|(a|b)a)"), 0);
}

#define generic_regex_to_nfa_test(regex)                                \
do {                                                                    \
    enum { SIZE = 128 };                                                \
                                                                        \
    struct nfa automaton;                                               \
    char buffer[SIZE];                                                  \
    twinm_assert(nfa_init(&automaton));                                 \
                                                                        \
    twinm_assert_eq(regex_to_nfa(&automaton, regex, 0), 0);             \
    ssize_t size = nfa_to_regex(buffer, &automaton, sizeof buffer);     \
    nfa_free(&automaton);                                               \
    twinm_assert_gt(size, 0);                                           \
    twinm_assert_eq((size_t)size, strlen(regex));                       \
    twinm_assert_eq(strcmp(buffer, regex), 0);                          \
} while(0)

void test_regex_to_nfa_rep_qm(void) {
    generic_regex_to_nfa_test("a?aaa?(ab)?");
}

void test_regex_to_nfa_rep_ps(void) {
    generic_regex_to_nfa_test("a+aaa+(ab)+");
}

void test_regex_to_nfa_rep_ax(void) {
    generic_regex_to_nfa_test("a*bcd*(ef)*");
}

void test_regex_to_nfa_alternation(void) {
    generic_regex_to_nfa_test("aa(a|b)aa(aaa|b)");
}

void test_regex_to_nfa_nested_alternation(void) {
    generic_regex_to_nfa_test("ab(c(de|f)|g)(h|(i|j)k(l|m)(no|p))(q|r(s|t))(u|(v|w))xy((z|1)234|5)");
}

void test_regex_to_nfa_repetition_in_alternation(void) {
    generic_regex_to_nfa_test("((a|b)?|b)");
    generic_regex_to_nfa_test("(ab+|b)");
    generic_regex_to_nfa_test("(ab*|b)");
}

void test_regex_to_nfa_escaped_char(void) {
    enum { SIZE = 64 };

    struct nfa nfa;
    char regex[SIZE];
    char buffer[SIZE];

    twinm_assert(nfa_init(&nfa));

    snprintf(regex, sizeof regex, "abc\\+\\.?\\\\");
    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    ssize_t size = nfa_to_regex(buffer, &nfa, sizeof buffer);

    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(regex));
    twinm_assert_eq(strcmp(buffer, regex), 0);

    snprintf(regex, sizeof regex, "abc\\");
    twinm_assert_lt(regex_to_nfa(&nfa, regex, 0), 0);

    snprintf(regex, sizeof regex, "abc\\g");
    twinm_assert_lt(regex_to_nfa(&nfa, regex, 0), 0);

    nfa_free(&nfa);
}

void test_regex_to_nfa(void) {
    enum { SIZE = 128 };

    struct nfa automaton;
    char buffer[SIZE];

    twinm_assert(nfa_init(&automaton));

    char const *regex = "bac?(ac)+a+(abs)*(.a)?aabc(awg(a|d)|d)b((a|b)|a)+(a|b)*";

    twinm_assert_eq(regex_to_nfa(&automaton, regex, 0), 0);
    ssize_t size = nfa_to_regex(buffer, &automaton, sizeof buffer);
    nfa_free(&automaton);
    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(regex));
    twinm_assert_eq(strcmp(buffer, regex), 0);
}

void test_regex_to_nfa_repeat_n(void) {
    enum { SIZE = 128 };

    struct nfa nfa;
    char buffer[SIZE];

    twinm_assert(nfa_init(&nfa));

    char regex[SIZE];
    char cregex[SIZE];

    snprintf(regex, sizeof regex, "bab{3}");
    snprintf(cregex, sizeof cregex, "babbb");

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    ssize_t size = nfa_to_regex(buffer, &nfa, sizeof buffer);

    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(cregex));
    twinm_assert_eq(strcmp(buffer, cregex), 0);

    snprintf(regex, sizeof regex, "ba(ab){3}");
    snprintf(cregex, sizeof cregex, "baababab");

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    size = nfa_to_regex(buffer, &nfa, sizeof buffer);

    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(cregex));
    twinm_assert_eq(strcmp(buffer, cregex), 0);

    snprintf(regex, sizeof regex, "ba(a|b){3}");
    snprintf(cregex, sizeof cregex, "ba(a|b)(a|b)(a|b)");

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    size = nfa_to_regex(buffer, &nfa, sizeof buffer);

    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(cregex));
    twinm_assert_eq(strcmp(buffer, cregex), 0);

    nfa_free(&nfa);
}

void test_regex_to_nfa_repeat_n_to_m(void) {
    enum { SIZE = 128 };

    struct nfa nfa;
    char buffer[SIZE];

    twinm_assert(nfa_init(&nfa));

    char regex[SIZE];
    char cregex[SIZE];

    snprintf(regex, sizeof regex, "bab{2,4}");
    snprintf(cregex, sizeof cregex, "babbb?b?");

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    ssize_t size = nfa_to_regex(buffer, &nfa, sizeof buffer);

    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(cregex));
    twinm_assert_eq(strcmp(buffer, cregex), 0);

    snprintf(regex, sizeof regex, "ba(ab){1,3}");
    snprintf(cregex, sizeof cregex, "baab(ab)?(ab)?");

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    size = nfa_to_regex(buffer, &nfa, sizeof buffer);

    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(cregex));
    twinm_assert_eq(strcmp(buffer, cregex), 0);

    snprintf(regex, sizeof regex, "ba(a|b){2,4}");
    snprintf(cregex, sizeof cregex, "ba(a|b)(a|b)(a|b)?(a|b)?");

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    size = nfa_to_regex(buffer, &nfa, sizeof buffer);

    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(cregex));
    twinm_assert_eq(strcmp(buffer, cregex), 0);

    snprintf(regex, sizeof regex, "bad{0,2}");
    snprintf(cregex, sizeof cregex, "bad?d?");

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    size = nfa_to_regex(buffer, &nfa, sizeof buffer);

    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(cregex));
    twinm_assert_eq(strcmp(buffer, cregex), 0);

    nfa_free(&nfa);
}

void test_regex_to_nfa_repeat_n_to_any(void) {
    enum { SIZE = 128 };

    struct nfa nfa;
    char buffer[SIZE];

    twinm_assert(nfa_init(&nfa));

    char regex[SIZE];
    char cregex[SIZE];

    snprintf(regex, sizeof regex, "bab{2,}");
    snprintf(cregex, sizeof cregex, "babbb*");

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    ssize_t size = nfa_to_regex(buffer, &nfa, sizeof buffer);

    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(cregex));
    twinm_assert_eq(strcmp(buffer, cregex), 0);

    snprintf(regex, sizeof regex, "ba(ab){1,}");
    snprintf(cregex, sizeof cregex, "baab(ab)*");

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    size = nfa_to_regex(buffer, &nfa, sizeof buffer);

    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(cregex));
    twinm_assert_eq(strcmp(buffer, cregex), 0);

    snprintf(regex, sizeof regex, "ba(a|b){2,}");
    snprintf(cregex, sizeof cregex, "ba(a|b)(a|b)(a|b)*");

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    size = nfa_to_regex(buffer, &nfa, sizeof buffer);

    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(cregex));
    twinm_assert_eq(strcmp(buffer, cregex), 0);

    nfa_free(&nfa);
}

void test_regex_to_nfa_digit_metachar(void) {
    enum { SIZE = 128 };
    char const *regex = "ab\\d+";

    struct nfa nfa;
    char buffer[SIZE];

    twinm_assert(nfa_init(&nfa));

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    ssize_t size = nfa_to_regex(buffer, &nfa, sizeof buffer);

    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(regex));
    twinm_assert_eq(strcmp(buffer, regex), 0);

    nfa_free(&nfa);
}

void test_regex_to_nfa_alnum_metachar(void) {
    enum { SIZE = 128 };
    char const *regex = "ab\\w+";

    struct nfa nfa;
    char buffer[SIZE];

    twinm_assert(nfa_init(&nfa));

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    ssize_t size = nfa_to_regex(buffer, &nfa, sizeof buffer);

    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(regex));
    twinm_assert_eq(strcmp(buffer, regex), 0);

    nfa_free(&nfa);
}

void test_regex_to_nfa_whitespace_metachar(void) {
    enum { SIZE = 128 };
    char const *regex = "ab\\s+";

    struct nfa nfa;
    char buffer[SIZE];

    twinm_assert(nfa_init(&nfa));

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    ssize_t size = nfa_to_regex(buffer, &nfa, sizeof buffer);

    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(regex));
    twinm_assert_eq(strcmp(buffer, regex), 0);

    nfa_free(&nfa);
}

void test_regex_to_nfa_non_digit_metachar(void) {
    enum { SIZE = 128 };
    char const *regex = "ab\\D+";

    struct nfa nfa;
    char buffer[SIZE];

    twinm_assert(nfa_init(&nfa));

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    ssize_t size = nfa_to_regex(buffer, &nfa, sizeof buffer);

    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(regex));
    twinm_assert_eq(strcmp(buffer, regex), 0);

    nfa_free(&nfa);
}

void test_regex_to_nfa_non_alnum_metachar(void) {
    enum { SIZE = 128 };
    char const *regex = "ab\\W+";

    struct nfa nfa;
    char buffer[SIZE];

    twinm_assert(nfa_init(&nfa));

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    ssize_t size = nfa_to_regex(buffer, &nfa, sizeof buffer);

    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(regex));
    twinm_assert_eq(strcmp(buffer, regex), 0);

    nfa_free(&nfa);
}

void test_regex_to_nfa_non_whitespace_metachar(void) {
    enum { SIZE = 128 };
    char const *regex = "ab\\S+";

    struct nfa nfa;
    char buffer[SIZE];

    twinm_assert(nfa_init(&nfa));

    twinm_assert_eq(regex_to_nfa(&nfa, regex, 0), 0);
    ssize_t size = nfa_to_regex(buffer, &nfa, sizeof buffer);

    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(regex));
    twinm_assert_eq(strcmp(buffer, regex), 0);

    nfa_free(&nfa);
}

static int verify_closure(struct list_head *list, struct nfa *nfa, int *closure_reference, unsigned size) {
    struct nfa_state *state;
    struct list_head *iter;
    size_t index;

    int status = 0;

    list_for_each(iter, list) {
        state = container(iter, struct nfa_closure_list_node, list_head)->state;
        index = array_index(nfa->state_pool, state);
        closure_reference[index] = 0;
    }

    for(unsigned i = 0; i < size; i++) {
        status |= closure_reference[i];
    }

    return status;
}

void test_nfa_epsilon_closure(void) {
    enum { SIZE = 32 };

    char buffer[SIZE];

    struct nfa_state state_pool[SIZE];

    struct nfa nfa;
    regex_flag_type flags;
    struct regex_capture_info capture;

    nfa.state_pool = &state_pool[0];
    nfa.start_index = 0;

    nfa.state_pool[0].charcode = regex_charcode_alt;
    nfa.state_pool[1].charcode = 'a';
    nfa.state_pool[2].charcode = 'b';
    nfa.state_pool[3].charcode = regex_charcode_repeat_zero_one;
    nfa.state_pool[4].charcode = 'c';
    nfa.state_pool[5].charcode = regex_charcode_repeat_one_any;
    nfa.state_pool[6].charcode = 'd';
    nfa.state_pool[7].charcode = regex_charcode_split;
    nfa.state_pool[8].charcode = regex_charcode_repeat_zero_any;
    nfa.state_pool[9].charcode = 'e';
    nfa.state_pool[10].charcode = regex_charcode_accept;

    for(int i = 0; i < 11; i++) {
        memset(&nfa.state_pool[i].edges[0], 0, sizeof(nfa.state_pool[i].edges));
    }

    list_push_back(&nfa.state_pool[0].edges[0], &nfa.state_pool[1].edges[0]);
    list_push_back(&nfa.state_pool[0].edges[1], &nfa.state_pool[2].edges[0]);
    list_push_back(&nfa.state_pool[1].edges[0], &nfa.state_pool[3].edges[0]);
    list_push_back(&nfa.state_pool[2].edges[0], &nfa.state_pool[3].edges[0]);
    list_push_back(&nfa.state_pool[3].edges[0], &nfa.state_pool[4].edges[0]);
    list_push_back(&nfa.state_pool[3].edges[1], &nfa.state_pool[5].edges[0]);
    list_push_back(&nfa.state_pool[4].edges[0], &nfa.state_pool[5].edges[0]);
    list_push_back(&nfa.state_pool[5].edges[0], &nfa.state_pool[6].edges[0]);
    list_push_back(&nfa.state_pool[6].edges[0], &nfa.state_pool[7].edges[0]);
    list_push_back(&nfa.state_pool[7].edges[0], &nfa.state_pool[8].edges[0]);
    list_push_back(&nfa.state_pool[7].edges[1], &nfa.state_pool[6].edges[0]);
    list_push_back(&nfa.state_pool[8].edges[0], &nfa.state_pool[10].edges[0]);
    list_push_back(&nfa.state_pool[8].edges[1], &nfa.state_pool[9].edges[0]);
    list_push_back(&nfa.state_pool[9].edges[0], &nfa.state_pool[8].edges[0]);

    ssize_t size = nfa_to_regex(buffer, &nfa, sizeof buffer);
    twinm_assert_gt(size, 0);
    twinm_assert_eq((size_t)size, strlen(buffer));
    twinm_assert_eq(strcmp(buffer, "(a|b)c?d+e*"), 0);

    struct nfa_closure_list_node list_node_pool[SIZE];

    int closure_reference[SIZE] = { 0 };

    list_init(list);

    /* alternation */
    twinm_assert_eq(nfa_epsilon_closure(&list, &nfa.state_pool[0], list_node_pool, SIZE, &flags, &capture), 2);

    closure_reference[1] = 1;
    closure_reference[2] = 1;

    twinm_assert_eq(verify_closure(&list, &nfa, closure_reference, SIZE), 0);

    list_clear(&list);

    /* repeat zero one */
    twinm_assert_eq(nfa_epsilon_closure(&list, &nfa.state_pool[3], list_node_pool, SIZE, &flags, &capture), 2);

    closure_reference[4] = 1;
    closure_reference[6] = 1;

    twinm_assert_eq(verify_closure(&list, &nfa, closure_reference, SIZE), 0);

    list_clear(&list);

    /* repeat one any */
    twinm_assert_eq(nfa_epsilon_closure(&list, &nfa.state_pool[5], list_node_pool, SIZE, &flags, &capture), 1);

    closure_reference[6] = 1;

    twinm_assert_eq(verify_closure(&list, &nfa, closure_reference, SIZE), 0);

    list_clear(&list);

    /* split */
    twinm_assert_eq(nfa_epsilon_closure(&list, &nfa.state_pool[7], list_node_pool, SIZE, &flags, &capture), 3);

    closure_reference[6] = 1;
    closure_reference[9] = 1;
    closure_reference[10] = 1;

    twinm_assert_eq(verify_closure(&list, &nfa, closure_reference, SIZE), 0);

    list_clear(&list);

    twinm_assert_eq(nfa_epsilon_closure(&list, &nfa.state_pool[8], list_node_pool, SIZE, &flags, &capture), 2);

    closure_reference[9] = 1;
    closure_reference[10] = 1;

    twinm_assert_eq(verify_closure(&list, &nfa, closure_reference, SIZE), 0);
}
