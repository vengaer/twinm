#include "macro.h"
#include "queue.h"
#include "queue_test.h"
#include "twinm_test.h"

struct int_node {
    int value;
    struct queue_node node;
};

void test_queue_enqueue_dequeue(void) {
    enum { SIZE = 4 };

    int value;

    queue_init(queue);

    struct int_node nodes[SIZE];

    for(int i = 0; i < SIZE; i++) {
        nodes[i].value = i;
        queue_enqueue(&queue, &nodes[i].node);
    }

    for(int i = 0; i < SIZE; i++) {
        value = container(queue_dequeue(&queue), struct int_node, node)->value;
        twinm_assert_eq(nodes[i].value, value);
    }

    queue_cleanup(&queue);
}

void test_queue_dynamic_allocation_behavior(void) {
    enum { SIZE = QUEUE_STATIC_SIZE + 1 };

    int value;

    queue_init(queue);

    struct int_node nodes[SIZE];

    for(int i = 0; i < SIZE; i++) {
        nodes[i].value = i;
        queue_enqueue(&queue, &nodes[i].node);
    }

    for(int i = 0; i < SIZE; i++) {
        value = container(queue_dequeue(&queue), struct int_node, node)->value;
        twinm_assert_eq(nodes[i].value, value);
    }

    queue_cleanup(&queue);
}

void test_queue_indexing(void) {
    enum { SIZE = QUEUE_STATIC_SIZE + 10 };

    int value;

    queue_init(queue);

    struct int_node nodes[SIZE];

    for(int i = 0; i < SIZE; i++) {
        nodes[i].value = i;
        queue_enqueue(&queue, &nodes[i].node);
        value = container(queue_dequeue(&queue), struct int_node, node)->value;
        twinm_assert_eq(value, i);
    }
    queue_cleanup(&queue);
}
