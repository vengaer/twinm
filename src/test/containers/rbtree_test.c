#include "macro.h"
#include "rbtree.h"
#include "rbtree_test.h"
#include "twinm_test.h"

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

enum {
    RB_RED_VIOLATION = -1,
    RB_LEFT_CHILD_ORDER_VIOLATION = -2,
    RB_RIGHT_CHILD_ORDER_VIOLATION = -3,
    RB_HEIGHT_VIOLATION = -4
};

struct int_node {
    int value;
    struct rbnode node;
};

static inline int rand_in_range(int low, int high) {
    double dec = (double)(rand() / RAND_MAX);

    return low + dec * (high - low);
}

static int rbnode_get_value(struct rbnode const *node) {
    return container(node, struct int_node, node, const)->value;
}

static int compare_int_nodes(struct rbnode const *n0, struct rbnode const *n1) {
    return rbnode_get_value(n1) - rbnode_get_value(n0);
}

static inline int rbnode_is_leaf(struct rbnode const *node) {
    return node->left == 0 && node->right == 0;
}

static inline int rbnode_is_red(struct rbnode const *node) {
    return node->color == rbnode_color_red;
}

static inline int rbnode_is_black(struct rbnode const *node) {
    return node->color == rbnode_color_black;
}

static inline int rbnode_is_red_safe(struct rbnode const *node) {
    return node && rbnode_is_red(node);
}

static int adheres_to_rb_properties(struct rbnode const *node) {
    if(!node) {
        return 0;
    }
    int rec_value;
    struct rbnode *rec_node;

    if(rbnode_is_red(node) && (rbnode_is_red_safe(node->left) || rbnode_is_red_safe(node->right))) {
        return RB_RED_VIOLATION;
    }

    if(node->left && compare_int_nodes(node->left, node) <= 0) {
        return RB_LEFT_CHILD_ORDER_VIOLATION;
    }

    if(node->right && compare_int_nodes(node, node->right) <= 0) {
        return RB_RIGHT_CHILD_ORDER_VIOLATION;
    }

    int height_contrib = rbnode_is_black(node);

    if(node->left && node->right) {
        int left_height = adheres_to_rb_properties(node->left);
        int right_height = adheres_to_rb_properties(node->right);

        if(left_height < 0) {
            return left_height;
        }
        if(right_height < 0) {
            return right_height;
        }

        if(left_height != right_height) {
            return RB_HEIGHT_VIOLATION;
        }

        return left_height + height_contrib;
    }
    else if(!rbnode_is_leaf(node)) {

        if(node->left) {
            rec_node = node->left;

        }
        else {
            rec_node = node->right;
        }

        rec_value = adheres_to_rb_properties(rec_node);

        if(rec_value < 0) {
            return rec_value;
        }
        else if(rec_value > 0) {
            return RB_HEIGHT_VIOLATION;
        }
    }

    return height_contrib;
}

void test_rbtree_init(void) {
    rbtree_init(tree);
    twinm_assert_eq(tree.left, 0);
    twinm_assert_eq(tree.right, 0);
}

void test_rbtree_find(void) {
    enum { SIZE = 40 };

    struct int_node vals[SIZE];

    for(int i = 0; i < SIZE; i++) {
        vals[i].value = i * 2 - SIZE / 2;
    }

    rbtree_init(tree);

    for(int i = 1; i < SIZE - 1; i++) {
        rbtree_insert(&tree, &vals[i].node, compare_int_nodes);
    }

    for(int i = 1; i < SIZE - 1; i++) {
        twinm_assert(rbtree_find(&tree, &vals[i].node, compare_int_nodes));
    }

    twinm_assert(!rbtree_find(&tree, &vals[0].node, compare_int_nodes));
    twinm_assert(!rbtree_find(&tree, &vals[SIZE - 1].node, compare_int_nodes));
}

void test_rbtree_insert(void) {
    enum { SIZE = 2000 };

    struct int_node nodes[SIZE];

    srand(time(0));

    for(int i = 0; i < SIZE; i++) {
        nodes[i].value = rand_in_range(-30, 30);
    }

    rbtree_init(tree);

    for(int i = 0; i < SIZE; i++) {
        rbtree_insert(&tree, &nodes[i].node, compare_int_nodes);
        twinm_assert_geq(adheres_to_rb_properties(tree.left), 0);
    }
}

void test_rbtree_remove(void) {
    enum { SIZE = 2000 };

    struct int_node nodes[SIZE];

    srand(time(0));

    for(int i = 0; i < SIZE; i++) {
        nodes[i].value = i - SIZE / 2;
    }

    rbtree_init(tree);

    for(int i = 0; i < SIZE - 1; i++) {
        rbtree_insert(&tree, &nodes[i].node, compare_int_nodes);
    }

    twinm_assert(!rbtree_remove(&tree, &nodes[SIZE - 1].node, compare_int_nodes));
    twinm_assert_geq(adheres_to_rb_properties(tree.left), 0);

    for(int i = 0; i < SIZE - 1; i++) {
        twinm_assert(rbtree_remove(&tree, &nodes[i].node, compare_int_nodes));
        twinm_assert_geq(adheres_to_rb_properties(tree.left), 0);
    }
}
