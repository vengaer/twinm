#include "twinm_test.h"
#include "vector.h"
#include "vector_test.h"

void test_vec_push(void) {
    int *vec = 0;
    twinm_assert(vec_push(vec, 0));
    twinm_assert_eq(vec_size(vec), 1);
    twinm_assert_eq(vec_capacity(vec), VEC_INITIAL_CAPACITY);
    twinm_assert_eq(vec[0], 0);
    twinm_assert(vec_push(vec, 1));
    twinm_assert_eq(vec_size(vec), 2);
    twinm_assert_eq(vec_capacity(vec), VEC_INITIAL_CAPACITY);
    twinm_assert_eq(vec[1], 1);

    for(unsigned i = 2; i < 8 * VEC_INITIAL_CAPACITY; i++) {
        twinm_assert(vec_push(vec, i));
    }
    twinm_assert_gt(vec_capacity(vec), VEC_INITIAL_CAPACITY);

    for(unsigned i = 0; i < 8 * VEC_INITIAL_CAPACITY; i++) {
        twinm_assert_eq(vec[i], i);
    }

    vec_free(vec);
}

void test_vec_pop(void) {
    int *vec = 0;
    for(unsigned i = 0; i < 10; i++) {
        twinm_assert(vec_push(vec, i));
    }
    twinm_assert_eq(vec_size(vec), 10);
    twinm_assert_eq(vec_back(vec), 9);
    vec_pop(vec);
    twinm_assert_eq(vec_size(vec), 9);
    twinm_assert_eq(vec_back(vec), 8);
    vec_free(vec);
}

void test_vec_reserve(void) {
    int *vec = 0;
    twinm_assert(vec_reserve(vec, 10));
    twinm_assert_geq(vec_capacity(vec), 10);
    twinm_assert(vec_reserve(vec, 5));
    twinm_assert_geq(vec_capacity(vec), 10);
    twinm_assert(vec_reserve(vec, 100));
    twinm_assert_geq(vec_capacity(vec), 100);

    vec_free(vec);
}

void test_vec_resize(void) {
    int *vec = 0;
    twinm_assert(vec_resize(vec, 10));
    twinm_assert_geq(vec_capacity(vec), 10);
    twinm_assert_eq(vec_size(vec), 10);
    for(unsigned i = 0; i < 10; i++) {
        twinm_assert_eq(vec[i], 0);
    }

    for(unsigned i = 10; i < 20; i++) {
        twinm_assert(vec_push(vec, i));
    }

    for(unsigned i = 0; i < 20; i++) {
        twinm_assert_eq(vec[i], i * (i >= 10));
    }

    twinm_assert_eq(vec_size(vec), 20);
    twinm_assert(vec_resize(vec, 15));
    twinm_assert_eq(vec_size(vec), 15);
    twinm_assert(vec_resize(vec, 20));
    twinm_assert_eq(vec_size(vec), 20);

    for(unsigned i = 0; i < 20; i++) {
        twinm_assert_eq(vec[i], i * (i >= 10 && i < 15));
    }

    vec_free(vec);
}

void test_vec_guard(void) {
    int *vec = 0;
    int status;

    vec_guard(vec, &status) {
        twinm_assert_eq(status, -EHEAP_ALLOC);
        twinm_assert(vec_push(vec, 1));
        twinm_assert(vec_push(vec, 2));
        twinm_assert(vec_push(vec, 3));
        twinm_assert(vec_push(vec, 4));
        twinm_assert(vec_push(vec, 5));
        vec_pop(vec);
        twinm_assert(vec_push(vec, 20));
    }

    twinm_assert_eq(status, 0);
}

void test_vec_access(void) {
    int *vec = 0;

    vec_guard(vec) {
        twinm_assert(vec_push(vec, 1));
        twinm_assert(vec_push(vec, 2));
        twinm_assert(vec_push(vec, 3));
        twinm_assert(vec_push(vec, 4));
        vec[2] = 12;
        twinm_assert_eq(vec[0], 1);
        twinm_assert_eq(vec[1], 2);
        twinm_assert_eq(vec[2], 12);
        twinm_assert_eq(vec[3], 4);
    }
}
