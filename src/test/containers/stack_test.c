#include "macro.h"
#include "stack.h"
#include "stack_test.h"
#include "twinm_test.h"

struct int_node {
    int value;
    struct stack_node node;
};

void test_stack_push_pop(void) {
    enum { SIZE = 4 };

    int value;

    stack_init(stack);

    struct int_node nodes[SIZE];

    for(int i = 0; i < SIZE; i++) {
        nodes[i].value = i;
        stack_push(&stack, &nodes[i].node);
    }

    for(int i = 0; i < SIZE; i++) {
        value = container(stack_pop(&stack), struct int_node, node)->value;
        twinm_assert_eq(nodes[SIZE - i - 1].value, value);
    }

    stack_cleanup(&stack);
}

void test_stack_dynamic_allocation_behavior(void) {
    enum { SIZE = STACK_STATIC_SIZE + 1 };

    int value;

    stack_init(stack);

    struct int_node nodes[SIZE];

    for(int i = 0; i < SIZE; i++) {
        nodes[i].value = i;
        stack_push(&stack, &nodes[i].node);
    }

    for(int i = 0; i < SIZE; i++) {
        value = container(stack_pop(&stack), struct int_node, node)->value;
        twinm_assert_eq(nodes[SIZE - i - 1].value, value);
    }

    stack_cleanup(&stack);
}

void test_stack_peek(void) {
    enum { SIZE = 4 };

    int value;

    stack_init(stack);

    struct int_node nodes[SIZE];

    for(int i = 0; i < SIZE; i++) {
        nodes[i].value = i;
        stack_push(&stack, &nodes[i].node);
        value = container(stack_peek(&stack), struct int_node, node)->value;
        twinm_assert_eq(value, i);
    }

    stack_cleanup(&stack);
}
