#ifndef QUEUE_TEST_H
#define QUEUE_TEST_H

void test_queue_enqueue_dequeue(void);
void test_queue_dynamic_allocation_behavior(void);
void test_queue_indexing(void);

#endif /* QUEUE_TEST_H */
