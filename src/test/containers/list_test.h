#ifndef LIST_TEST_H
#define LIST_TEST_H

void test_list_push_front(void);
void test_list_push_back(void);
void test_list_push_front_back(void);
void test_list_extract(void);
void test_list_empty(void);
void test_list_size(void);
void test_list_for_each_zip(void);
void test_list_find(void);

#endif /* LIST_TEST_H */
