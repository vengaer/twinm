#ifndef RBTREE_TEST_H
#define RBTREE_TEST_H

void test_rbtree_init(void);
void test_rbtree_find(void);
void test_rbtree_insert(void);
void test_rbtree_remove(void);

#endif /* RBTREE_TEST_H */
