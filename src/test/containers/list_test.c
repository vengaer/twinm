#include "list.h"
#include "list_test.h"
#include "twinm_test.h"

#include <string.h>

struct list_node {
    struct list_head head;
    int id;
};

void test_list_push_front(void) {
    list_init(list);

    int ids[4] = { 3, 2, 1, 0 };

    struct list_node n0 = { .id = 0 };
    struct list_node n1 = { .id = 1 };
    struct list_node n2 = { .id = 2 };
    struct list_node n3 = { .id = 3 };

    list_push_front(&list, &n0.head);
    list_push_front(&list, &n1.head);
    list_push_front(&list, &n2.head);
    list_push_front(&list, &n3.head);

    struct list_head *it;
    int i = 0;

    list_for_each(it, &list) {
        twinm_assert_eq(container(it, struct list_node, head)->id, ids[i++]);
    }
}

void test_list_push_back(void) {
    list_init(list);

    int ids[4] = { 0, 1, 2, 3 };

    struct list_node n0 = { .id = 0 };
    struct list_node n1 = { .id = 1 };
    struct list_node n2 = { .id = 2 };
    struct list_node n3 = { .id = 3 };

    list_push_back(&list, &n0.head);
    list_push_back(&list, &n1.head);
    list_push_back(&list, &n2.head);
    list_push_back(&list, &n3.head);

    struct list_head *it;
    int i = 0;

    list_for_each(it, &list) {
        twinm_assert_eq(container(it, struct list_node, head)->id, ids[i++]);
    }
}
void test_list_push_front_back(void) {
    list_init(list);

    int ids[4] = { 2, 0, 1, 3 };

    struct list_node n0 = { .id = 0 };
    struct list_node n1 = { .id = 1 };
    struct list_node n2 = { .id = 2 };
    struct list_node n3 = { .id = 3 };

    list_push_front(&list, &n0.head);
    list_push_back(&list, &n1.head);
    list_push_front(&list, &n2.head);
    list_push_back(&list, &n3.head);

    struct list_head *it;
    int i = 0;

    list_for_each(it, &list) {
        twinm_assert_eq(container(it, struct list_node, head)->id, ids[i++]);
    }
}

void test_list_extract(void) {
    list_init(list);

    int ids0[4] = { 1, 2, 3, 4 };
    int ids1[3] = { 1, 2, 4 };

    struct list_node n0 = { .id = 0 };
    struct list_node n1 = { .id = 1 };
    struct list_node n2 = { .id = 2 };
    struct list_node n3 = { .id = 3 };
    struct list_node n4 = { .id = 4 };

    list_push_back(&list, &n0.head);
    list_push_back(&list, &n1.head);
    list_push_back(&list, &n2.head);
    list_push_back(&list, &n3.head);
    list_push_back(&list, &n4.head);

    struct list_head *extracted = list_extract(&list);

    twinm_assert_eq(container(extracted, struct list_node, head)->id, 0);

    struct list_head *it;
    int i = 0;

    list_for_each(it, &list) {
        twinm_assert_eq(container(it, struct list_node, head)->id, ids0[i++]);
    }

    extracted = list_extract(list.tail->tail);

    twinm_assert_eq(container(extracted, struct list_node, head)->id, 3);

    i = 0;
    list_for_each(it, &list) {
        twinm_assert_eq(container(it, struct list_node, head)->id, ids1[i++]);
    }
}

void test_list_empty(void) {
    list_init(list);

    twinm_assert(list_empty(&list));

    struct list_node n0 = { .id = 0 };

    list_push_back(&list, &n0.head);

    twinm_assert(!list_empty(&list));

    (void)list_extract(&list);

    twinm_assert(list_empty(&list));
}

void test_list_size(void) {
    list_init(list);

    twinm_assert_eq(list_size(&list), 0);

    struct list_node n0 = { .id = 0 };
    struct list_node n1 = { .id = 1 };

    list_push_front(&list, &n0.head);

    twinm_assert_eq(list_size(&list), 1);

    list_push_front(&list, &n1.head);

    twinm_assert_eq(list_size(&list), 2);

    (void)list_extract(&list);

    twinm_assert_eq(list_size(&list), 1);

    (void)list_extract(&list);

    twinm_assert_eq(list_size(&list), 0);
}

void test_list_for_each_zip(void) {
    enum {
        SIZE0 = 32,
        SIZE1 = 24
    };

    list_init(list0);
    list_init(list1);

    struct list_node nodes0[SIZE0];
    struct list_node nodes1[SIZE1];

    memset(nodes0, 0, sizeof(nodes0));
    memset(nodes1, 0, sizeof(nodes1));

    for(int i = 0; i < SIZE0; i++) {
        nodes0[i].id = i;
        list_push_back(&list0, &nodes0[i].head);
    }

    for(int i = 0; i < SIZE1; i++) {
        nodes1[i].id = i;
        list_push_back(&list1, &nodes1[i].head);
    }

    unsigned index = 0;

    struct list_node *n0, *n1;
    struct list_head *iter0, *iter1;

    list_for_each_zip(iter0, iter1, &list0, &list1) {
        n0 = container(iter0, struct list_node, head);
        n1 = container(iter1, struct list_node, head);

        twinm_assert_lt(index, SIZE1);

        twinm_assert_eq(n0->id, nodes0[index].id);
        twinm_assert_eq(n1->id, nodes1[index].id);
        ++index;
    }
}

static int compare_list_nodes(struct list_head const *iter, void const *data) {
    return *((int const *)data) - container(iter, struct list_node, head, const)->id;
}

void test_list_find(void) {
    list_init(list);

    struct list_node n0 = { .id = 0 };
    struct list_node n1 = { .id = 1 };
    struct list_node n2 = { .id = 2 };

    list_push_back(&list, &n0.head);
    list_push_back(&list, &n1.head);
    list_push_back(&list, &n2.head);

    int data = 0;

    twinm_assert_eq(list_find(&list, (void *)&data, compare_list_nodes), &n0);
    ++data;
    twinm_assert_eq(list_find(&list, (void *)&data, compare_list_nodes), &n1);
    ++data;
    twinm_assert_eq(list_find(&list, (void *)&data, compare_list_nodes), &n2);
}
