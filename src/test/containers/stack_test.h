#ifndef STACK_TEST_H
#define STACK_TEST_H

void test_stack_push_pop(void);
void test_stack_dynamic_allocation_behavior(void);
void test_stack_peek(void);

#endif /* STACK_TEST_H */
