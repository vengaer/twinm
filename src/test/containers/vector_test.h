#ifndef VECTOR_TEST_H
#define VECTOR_TEST_H

void test_vec_push(void);
void test_vec_pop(void);
void test_vec_reserve(void);
void test_vec_resize(void);
void test_vec_guard(void);
void test_vec_access(void);

#endif /* VECTOR_TEST_H */
