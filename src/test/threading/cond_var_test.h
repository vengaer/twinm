#ifndef COND_VAR_TEST_H
#define COND_VAR_TEST_H

void test_cond_var_wake(void);
void test_cond_var_wait_timeout(void);
void test_cond_var_wake_all(void);

#endif /* COND_VAR_TEST_H */
