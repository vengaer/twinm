#include "mutex.h"
#include "mutex_test.h"
#include "twinm_test.h"
#include "twinm_types.h"

void test_mutex_lock_unlock(void) {
    mutex_handle mutex;

    twinm_assert_eq(mutex_init(&mutex), 0);

    twinm_assert_eq(mutex_lock(&mutex), 0);
    twinm_assert_neq(mutex_try_lock(&mutex), 0);
    twinm_assert_eq(mutex_unlock(&mutex), 0);

    twinm_assert_eq(mutex_destroy(&mutex), 0);
}

void test_mutex_lock_guard(void) {
    mutex_handle mutex;

    twinm_assert_eq(mutex_init(&mutex), 0);

    twinm_assert_eq(mutex_try_lock(&mutex), 0);
    twinm_assert_eq(mutex_unlock(&mutex), 0);
    int status = 0;
    lock_guard(&mutex, &status) {
        twinm_assert_neq(mutex_try_lock(&mutex), 0);
        twinm_assert_eq(status, -ELOCK);
    }
    twinm_assert_eq(status, 0);
    twinm_assert_eq(mutex_try_lock(&mutex), 0);
    twinm_assert_eq(mutex_unlock(&mutex), 0);

    lock_guard(&mutex, 0) {
        twinm_assert_neq(mutex_try_lock(&mutex), 0);
    }

    twinm_assert_eq(mutex_try_lock(&mutex), 0);
    twinm_assert_eq(mutex_unlock(&mutex), 0);

    lock_guard(&mutex) {
        twinm_assert_neq(mutex_try_lock(&mutex), 0);
    }

    twinm_assert_eq(mutex_try_lock(&mutex), 0);
    twinm_assert_eq(mutex_unlock(&mutex), 0);

    twinm_assert_eq(mutex_destroy(&mutex), 0);
}
