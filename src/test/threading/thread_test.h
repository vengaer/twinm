#ifndef THREAD_TEST_H
#define THREAD_TEST_H

void test_thread_fork_join(void);
void test_thread_exit(void);
void test_thread_guard(void);

#endif /* THREAD_TEST_H */
