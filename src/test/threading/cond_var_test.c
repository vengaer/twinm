#include "cond_var.h"
#include "cond_var_test.h"
#include "macro.h"
#include "mutex.h"
#include "thread.h"
#include "twinm_test.h"

static mutex_handle assertion_mutex;
static cond_var cv;
static mutex_handle mutex;
static int data;

void *work(void *args) {
    UNUSED(args);

    lock_guard(&mutex) {
        while(!data) {
            cond_wait(&cv, &mutex);
        }
        lock_guard(&assertion_mutex) {
            twinm_assert_eq(data, 1);
        }
        /* Ensure main thread starts waiting */
        sleep(2);
        data = 0;
        cond_wake(&cv);
    }

    return 0;
}

void *work_nowake(void *args) {
    UNUSED(args);

    lock_guard(&mutex) {
        while(!data) {
            cond_wait(&cv, &mutex);
        }
        lock_guard(&assertion_mutex) {
            twinm_assert_eq(data, 1);
        }
        /* Ensure main thread starts waiting */
        sleep(2);
        data = 0;
    }

    return 0;
}

void *work_wakeall(void *args) {
    UNUSED(args);

    lock_guard(&mutex) {
        while(!data) {
            cond_wait(&cv, &mutex);
        }
        lock_guard(&assertion_mutex) {
            twinm_assert_eq(data, 1);
        }
        /* Ensure other threads start waiting */
        sleep(2);
        data = 2;
        cond_wake_all(&cv);
    }

    return 0;
}

void *work_useless_wait(void *args) {
    UNUSED(args);

    lock_guard(&mutex) {
        while(data != 2) {
            cond_wait(&cv, &mutex);
        }
    }

    return 0;
}

void test_cond_var_wake(void) {
    thread_handle thread;
    data = 0;

    twinm_assert_eq(mutex_init(&mutex), 0);
    twinm_assert_eq(mutex_init(&assertion_mutex), 0);
    twinm_assert_eq(cond_init(&cv), 0);

    int forkres = thread_fork(&thread, work, 0);

    lock_guard(&assertion_mutex) {
        twinm_assert_eq(forkres, 0);
    }

    /* Ensure work thread starts waiting */
    sleep(2);

    lock_guard(&mutex) {
        lock_guard(&assertion_mutex) {
            twinm_assert_eq(data, 0);
        }
        data = 1;
        cond_wake(&cv);
    }

    lock_guard(&mutex) {
        while(data) {
            cond_wait(&cv, &mutex);
        }
        lock_guard(&assertion_mutex) {
            twinm_assert_eq(data, 0);
        }
    }

    int joinres = thread_join(thread, 0);

    twinm_assert_eq(joinres, 0);

    twinm_assert_eq(cond_destroy(&cv), 0);
    twinm_assert_eq(mutex_destroy(&assertion_mutex), 0);
    twinm_assert_eq(mutex_destroy(&mutex), 0);
}

void test_cond_var_wait_timeout(void) {
    thread_handle thread;
    data = 0;

    twinm_assert_eq(mutex_init(&mutex), 0);
    twinm_assert_eq(mutex_init(&assertion_mutex), 0);
    twinm_assert_eq(cond_init(&cv), 0);

    int forkres = thread_fork(&thread, work_nowake, 0);
    lock_guard(&assertion_mutex) {
        twinm_assert_eq(forkres, 0);
    }

    /* Ensure work thread starts waiting */
    sleep(2);

    lock_guard(&mutex) {
        lock_guard(&assertion_mutex) {
            twinm_assert_eq(data, 0);
        }
        data = 1;
        cond_wake(&cv);
    }

    lock_guard(&mutex) {
        while(data) {
            /* Never woken by cond_wake */
            cond_wait_timeout(&cv, &mutex, 1000000000);
        }
        lock_guard(&assertion_mutex) {
            twinm_assert_eq(data, 0);
        }
    }

    int joinres = thread_join(thread, 0);
    twinm_assert_eq(joinres, 0);

    twinm_assert_eq(cond_destroy(&cv), 0);
    twinm_assert_eq(mutex_destroy(&assertion_mutex), 0);
    twinm_assert_eq(mutex_destroy(&mutex), 0);
}

void test_cond_var_wake_all(void) {
    thread_handle thread0, thread1;
    data = 0;

    twinm_assert_eq(mutex_init(&mutex), 0);
    twinm_assert_eq(mutex_init(&assertion_mutex), 0);
    twinm_assert_eq(cond_init(&cv), 0);

    int forkres = thread_fork(&thread0, work_useless_wait, 0);
    lock_guard(&assertion_mutex) {
        twinm_assert_eq(forkres, 0);
    }
    forkres = thread_fork(&thread1, work_wakeall, 0);
    lock_guard(&assertion_mutex) {
        twinm_assert_eq(forkres, 0);
    }

    /* Ensure work thread starts waiting */
    sleep(2);

    lock_guard(&mutex) {
        lock_guard(&assertion_mutex) {
            twinm_assert_eq(data, 0);
        }
        data = 1;
        cond_wake_all(&cv);
    }

    lock_guard(&mutex) {
        while(data != 2) {
            cond_wait(&cv, &mutex);
        }
        lock_guard(&assertion_mutex) {
            twinm_assert_eq(data, 2);
        }
    }

    int joinres = thread_join(thread0, 0);
    lock_guard(&assertion_mutex) {
        twinm_assert_eq(joinres, 0);
    }
    joinres = thread_join(thread1, 0);
    twinm_assert_eq(joinres, 0);

    twinm_assert_eq(cond_destroy(&cv), 0);
    twinm_assert_eq(mutex_destroy(&assertion_mutex), 0);
    twinm_assert_eq(mutex_destroy(&mutex), 0);
}
