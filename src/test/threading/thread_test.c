#include "macro.h"
#include "twinm_test.h"
#include "thread.h"
#include "thread_test.h"

static int volatile value = 0;
static int retval = 0;

static void *procedure(void *args) {
    UNUSED(args);
    value = 1;
    retval = 3;

    return &retval;
}

static void *procedure_exit(void *args) {
    retval = *((int *)args);
    thread_exit(&retval);
    return 0;
}

void test_thread_fork_join(void) {
    thread_handle worker;

    value = 0;
    twinm_assert_eq(thread_fork(&worker, procedure, 0), 0);

    void *vptr;
    twinm_assert_eq(thread_join(worker, &vptr), 0);
    twinm_assert_eq(value, 1);
    twinm_assert_eq(*((int *)vptr), 3);
}

void test_thread_exit(void) {
    thread_handle worker;
    retval = 0;
    int arg = 4;
    twinm_assert_eq(thread_fork(&worker, procedure_exit, (void *)&arg), 0);
    void *vptr;
    twinm_assert_eq(thread_join(worker, &vptr), 0);
    twinm_assert_eq(*(int *)vptr, arg);
}

void test_thread_guard(void) {
    thread_handle worker;
    void *vptr;
    int status;

    retval = 0;

    thread_guard(&worker, procedure, 0, &vptr, &status) {
        twinm_assert_eq(status, -EFORK);
    }

    twinm_assert_eq(*(int *)vptr, retval);
}
