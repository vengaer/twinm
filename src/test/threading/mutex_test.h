#ifndef MUTEX_TEST_H
#define MUTEX_TEST_H

void test_mutex_lock_unlock(void);
void test_mutex_lock_guard(void);

#endif /* MUTEX_TEST_H */
