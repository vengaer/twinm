#ifndef LINALG_TEST_H
#define LINALG_TEST_H

void test_seq_mv_mul_4(void);
void test_seq_mat_4_transpose(void);

#endif /* LINALG_TEST_H */
