#include "linalg.h"
#include "linalg_test.h"
#include "macro.h"
#include "twinm_test.h"
#include "twinm_types.h"

#include <math.h>

void test_seq_mv_mul_4(void) {
    float matrix[] = {
         1.f,  2.f, -2.f,  4.f,
         0.f,  3.f, -1.f,  0.f,
         3.f,  3.f,  0.f,  8.f,
         1.f,  4.f, -9.f,  3.f,
    };

    float vec[] = {
        1.f,
        3.f,
        6.f,
        2.f
    };

    float const res[] = {
          3.f,
          3.f,
         28.f,
        -35.f
    };

    float dst[array_size(vec)];

    seq_mv_mul_4(dst, matrix, vec);

    for(unsigned i = 0; i < array_size(res); i++) {
        twinm_assert_lt(fabsf(dst[i] - res[i]), EPSILON);
    }
}

void test_seq_mat_4_transpose(void) {
    float const matrix[] = {
        1.f,  2.f,  3.f,  4.f,
        5.f,  6.f,  7.f,  8.f,
        9.f,  10.f, 11.f, 12.f,
        13.f, 14.f, 15.f, 16.f
    };
    float const ref[] = {
        1.f,  5.f,  9.f,  13.f,
        2.f,  6.f,  10.f, 14.f,
        3.f,  7.f,  11.f, 15.f,
        4.f,  8.f,  12.f, 16.f
    };
    float dst[array_size(matrix)];

    seq_mat_4_transpose(dst, matrix);

    for(unsigned i = 0; i < array_size(ref); i++) {
        twinm_assert_lt(fabsf(dst[i] - ref[i]), EPSILON);
    }
}
