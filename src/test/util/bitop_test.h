#ifndef BITOP_TEST_H
#define BITOP_TEST_H

void test_nand(void);
void test_nor(void);
void test_xnor(void);
void test_max(void);
void test_min(void);

#endif /* BITOP_TEST_H */
