#ifndef STRUTILS_TEST_H
#define STRUTILS_TEST_H

void test_strscpy_result(void);
void test_strscpy_return_value(void);
void test_strscpy_null_termination(void);

void test_strsncpy_result(void);
void test_strsncpy_return_value(void);
void test_strsncpy_null_termination(void);

void test_seq_string_tolower(void);
void test_strlower(void);

void test_seq_string_toupper(void);
void test_strupper(void);

void test_strrepl(void);

#endif /* STRUTILS_TEST_H */
