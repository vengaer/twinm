#include "macro.h"
#include "macro_test.h"
#include "twinm_test.h"

void test_array_index(void) {
    enum { SIZE = 231 };

    int arr[SIZE];

    twinm_assert_eq(array_index(arr, &arr[222]), 222);
}

void test_array_size(void) {
    enum { SIZE = 321 };

    int arr[SIZE];

    twinm_assert_eq(array_size(arr), SIZE);
}

void test_nargs(void) {
    twinm_assert_eq(nargs(a), 1);
    twinm_assert_eq(nargs(a,b), 2);
    twinm_assert_eq(nargs(a,b,c), 3);
    twinm_assert_eq(nargs(a,b,c,d), 4);
    twinm_assert_eq(nargs(a,b,c,d,e), 5);
    twinm_assert_eq(nargs(a,b,c,d,e,f), 6);
    twinm_assert_eq(nargs(a,b,c,d,e,f,g), 7);
    twinm_assert_eq(nargs(a,b,c,d,e,f,g,h), 8);
}

void test_is_immediate(void) {
    int lval;
    twinm_assert(is_immediate(0));
    twinm_assert(is_immediate(1 * 2));
    twinm_assert(is_immediate(1));
    twinm_assert(is_immediate((1 << 8) + 36));
    twinm_assert(!is_immediate(lval));
    twinm_assert(!is_immediate(&lval));
    twinm_assert(!is_immediate(1 + lval));
}
