#ifndef MACRO_TEST_H
#define MACRO_TEST_H

void test_array_index(void);
void test_array_size(void);
void test_nargs(void);
void test_is_immediate(void);

#endif /* MACRO_TEST_H */
