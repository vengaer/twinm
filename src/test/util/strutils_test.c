#include "strutils.h"
#include "strutils_test.h"
#include "twinm_test.h"
#include "twinm_types.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static inline void tolower_seq_inplace(char *str, unsigned len) {
    for(unsigned i = 0; i < len; i++) {
        str[i] = tolower(str[i]);
    }
}

static inline void toupper_seq_inplace(char *str, unsigned len) {
    for(unsigned i = 0; i < len; i++) {
        str[i] = toupper(str[i]);
    }
}

void test_strscpy_result(void) {
    char const *longstr = "String too long to fit in dst";
    char const *shortstr = "String";

    char dst[8];

    (void) strscpy(dst, shortstr, sizeof dst);

    twinm_assert_eq(strcmp(dst, shortstr), 0);

    (void) strscpy(dst, longstr, sizeof dst);

    twinm_assert_eq(strcmp(dst, "String "), 0);
}

void test_strscpy_return_value(void) {
    char const *longstr = "String too long to fit in dst";
    char const *shortstr = "String";

    char pfit[8];
    memset(pfit, 'a', sizeof pfit - 1);
    pfit[7] = '\0';

    char dst[8];

    twinm_assert_eq(strscpy(dst, pfit, sizeof dst), (ssize_t) strlen(pfit));
    twinm_assert_eq(strscpy(dst, shortstr, sizeof dst), (ssize_t) strlen(shortstr));
    twinm_assert_eq(strscpy(dst, longstr, sizeof dst), -E2BIG);
}

void test_strscpy_null_termination(void) {
    char const *longstr = "String too long to fit in dst";
    char const *shortstr = "String";

    char dst[8];

    dst[7] = 1;

    (void) strscpy(dst, shortstr, sizeof dst);

    /* Ensure excessive null bytes aren't written */
    twinm_assert_eq(dst[sizeof dst - 1], 1);

    twinm_assert_eq(dst[strlen(shortstr)], '\0');

    (void) strscpy(dst, longstr, sizeof dst);

    twinm_assert_eq(dst[sizeof dst - 1], '\0');
}

void test_strsncpy_result(void) {
    char const *longstr = "String too long to fit in dst";
    char const *shortstr = "String";

    char dst[8];

    (void) strsncpy(dst, shortstr, sizeof dst, 3);
    twinm_assert_eq(strcmp(dst, "Str"), 0);

    (void) strsncpy(dst, shortstr, sizeof dst, 32);
    twinm_assert_eq(strcmp(dst, shortstr), 0);

    (void) strsncpy(dst, longstr, sizeof dst, 32);
    twinm_assert_eq(strcmp(dst, "String "), 0);
}

void test_strsncpy_return_value(void) {
    char const *longstr = "String too long to fit in dst";
    char const *shortstr = "String";

    char pfit[8];
    memset(pfit, 'a', sizeof pfit - 1);
    pfit[7] = '\0';

    char dst[8];

    twinm_assert_eq(strsncpy(dst, pfit, sizeof dst, sizeof dst - 1), (ssize_t) strlen(pfit));
    twinm_assert_eq(strsncpy(dst, shortstr, sizeof dst, strlen(shortstr)), (ssize_t) strlen(shortstr));
    twinm_assert_eq(strsncpy(dst, longstr, sizeof dst, sizeof dst), -E2BIG);
    twinm_assert_eq(strsncpy(dst, pfit, sizeof dst, 3), 3);
    twinm_assert_eq(strsncpy(dst, pfit, sizeof dst, sizeof pfit), -E2BIG);
    twinm_assert_eq(strsncpy(dst, longstr, sizeof dst, sizeof dst + 1), -E2BIG);
}

void test_strsncpy_null_termination(void) {
    char const *longstr = "String too long to fit in dst";
    char const *shortstr = "String";

    char dst[8];

    dst[7] = 1;

    (void) strsncpy(dst, shortstr, sizeof dst, strlen(shortstr));

    /* Ensure excessive null bytes aren't written */
    twinm_assert_eq(dst[sizeof dst - 1], 1);
    twinm_assert_eq(dst[strlen(shortstr)], '\0');

    (void) strsncpy(dst, longstr, sizeof dst, 4);
    twinm_assert_eq(dst[4], '\0');

    (void) strsncpy(dst, longstr, sizeof dst, strlen(longstr));
    twinm_assert_eq(dst[sizeof dst - 1], '\0');

    (void) strsncpy(dst, longstr, sizeof dst, sizeof dst);
    twinm_assert_eq(dst[sizeof dst - 1], '\0');
}

void test_seq_string_tolower(void) {
    enum { SIZE = 32 };

    char buffer[SIZE];
    char input[SIZE];

    snprintf(input, sizeof input, "TEST StrInG");
    twinm_assert_eq(seq_string_tolower(buffer, input, sizeof buffer), 0);
    tolower_seq_inplace(input, strlen(input));
    twinm_assert_eq(strcmp(buffer, input), 0);

    snprintf(input, sizeof input, "Some test string");
    twinm_assert_lt(seq_string_tolower(buffer, input, strlen(input)), 0);
}

void test_strlower(void) {
    enum { SIZE = 32 };

    char buffer[SIZE];
    char input[SIZE];

    snprintf(input, sizeof input, "TEST StrInG");
    twinm_assert_eq(strlower(buffer, input, sizeof buffer), 0);
    tolower_seq_inplace(input, strlen(input));
    twinm_assert_eq(strcmp(buffer, input), 0);

    snprintf(input, sizeof input, "Some test string");
    twinm_assert_lt(strlower(buffer, input, strlen(input)), 0);
}

void test_seq_string_toupper(void) {
    enum { SIZE = 32 };

    char buffer[SIZE];
    char input[SIZE];

    snprintf(input, sizeof input, "TEST StrInG");
    twinm_assert_eq(seq_string_toupper(buffer, input, sizeof buffer), 0);
    toupper_seq_inplace(input, strlen(input));
    twinm_assert_eq(strcmp(buffer, input), 0);

    snprintf(input, sizeof input, "Some test string");
    twinm_assert_lt(seq_string_toupper(buffer, input, strlen(input)), 0);
}

void test_strupper(void) {
    enum { SIZE = 32 };

    char buffer[SIZE];
    char input[SIZE];

    snprintf(input, sizeof input, "TEST StrInG");
    twinm_assert_eq(strupper(buffer, input, sizeof buffer), 0);
    toupper_seq_inplace(input, strlen(input));
    twinm_assert_eq(strcmp(buffer, input), 0);

    snprintf(input, sizeof input, "Some test string");
    twinm_assert_lt(strupper(buffer, input, strlen(input)), 0);
}

void test_strrepl(void) {
    enum { SIZE = 64 };
    char buffer[SIZE];

    snprintf(buffer, sizeof buffer, "A\nstring\nwith\nnewlines");

    strrepl(buffer, '\n', ' ');

    twinm_assert_eq(strcmp(buffer, "A string with newlines"), 0);

    snprintf(buffer, sizeof buffer, "Another string but with cApital As");

    strrepl(buffer, 'A', 'a');

    twinm_assert_eq(strcmp(buffer, "another string but with capital as"), 0);
}
