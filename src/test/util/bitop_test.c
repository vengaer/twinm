#include "bitop.h"
#include "bitop_test.h"
#include "twinm_test.h"

#include <limits.h>


void test_nand(void) {
    twinm_assert(nand(0,0));
    twinm_assert(nand(1,0));
    twinm_assert(nand(0,1));
    twinm_assert(!nand(1,1));
}

void test_nor(void) {
    twinm_assert(nor(0,0));
    twinm_assert(!nor(1,0));
    twinm_assert(!nor(0,1));
    twinm_assert(!nor(1,1));
}

void test_xnor(void) {
    twinm_assert(xnor(0,0));
    twinm_assert(!xnor(1,0));
    twinm_assert(!xnor(0,1));
    twinm_assert(xnor(1,1));
}

void test_max(void) {
    unsigned v0 = 30;
    unsigned v1 = 26000;

    twinm_assert_eq(max(v0, v1), v1);

    v1 = UINT_MAX;

    twinm_assert_eq(max(v0, v1), v1);

    v0 = 0;

    twinm_assert_eq(max(v0, v1), v1);
}

void test_min(void) {
    unsigned v0 = 30;
    unsigned v1 = 26000;

    twinm_assert_eq(min(v0, v1), v0);

    v1 = UINT_MAX;

    twinm_assert_eq(min(v0, v1), v0);

    v0 = 0;

    twinm_assert_eq(min(v0, v1), v0);
}
