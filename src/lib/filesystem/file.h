#ifndef FILE_H
#define FILE_H

#include "macro.h"
#include "twinm_types.h"

#include <stdbool.h>
#include <stdio.h>

#if defined TWINM_HOST_LINUX

#include <unistd.h>
#include <sys/stat.h>

#elif defined TWINM_HOST_WINDOWS

#include <fileapi.h>
#include <shlwapi.h>

#endif /* defined TWINM_HOST_LINUX */

#if defined TWINM_HOST_LINUX

static inline int file_unlink(char const *path) {
    return unlink(path);
}

static inline bool file_exists(char const *path) {
    struct stat s;
    return !stat(path, &s);
}

#elif defined TWINM_HOST_WINDOWS

static inline int file_unlink(char const *path) {
    /* Return 0 on success */
    return !DeleteFileA(path);
}

static inline bool file_exists(char const *path) {
    return PathFileExistsA(path);
}

#endif /* defined TWINM_HOST_LINUX */

static inline int internal_open_file(FILE **fp, char const *restrict path, char const *restrict mode) {
    *fp = fopen(path, mode);
    return !*fp;
}

/* Open file for duration of scope, i.e.
 *
 * FILE *fp;
 * int status;
 *
 * file_guard(fp, "path/to/file", "w", &status) {
 *
 *     // File open
 *     // ...
 *
 * } // File closed
 *
 * If the file could not be opened, status is set to
 * -EOPEN and the scope is never executed.
 *
 * @fp:     file pointer
 * @path:   path of the file
 * @mode:   mode to open the file in
 * @status: pointer to int, set to 0 on success and
 *          -EOPEN if the file could not be opened
 */
#define file_guard(fp, path, mode, status) \
    guard(internal_open_file(&fp, path, mode), fclose(fp), status, -EOPEN)

#endif /* FILE_H */
