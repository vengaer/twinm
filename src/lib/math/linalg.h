#ifndef LINALG_H
#define LINALG_H

#if defined TWINM_SIMD_AVX || defined TWINM_SIMD_SSE

#include "simd.h"

#endif /* defined TWINM_SIMD_AVX || defined TWINM_SIMD_SSE */

#if defined TWINM_TEST_BUILD || (!defined TWINM_SIMD_AVX && !defined TWINM_SIMD_SSE)

void seq_mv_mul_4(float *dst, float const *matrix, float const *vec);
void seq_mat_4_transpose(float *dst, float const *matrix);

#endif /* defined TWINM_TEST_BUILD || (!defined TWINM_SIMD_AVX && !defined TWINM_SIMD_SSE) */

#if defined TWINM_SIMD_AVX

static inline void mvmul4(float *dst, float const *matrix, float const *vec) {
    avx_mv_mul_4(dst, matrix, vec);
}

static inline void mat4transpose(float *dst, float const *matrix) {
    avx_mat_4_transpose(dst, matrix);
}

#elif defined TWINM_SIMD_SSE

static inline void mvmul4(float *dst, float const *matrix, float const *vec) {
    sse_mv_mul_4(dst, matrix, vec);
}

static inline void mat4transpose(float *dst, float const *matrix) {
    sse_mat_4_transpose(dst, matrix);
}

#else

static inline void mvmul4(float *dst, float const *matrix, float const *vec) {
    seq_mv_mul_4(dst, matrix, vec);
}

static inline void mat4transpose(float *dst, float const *matrix) {
    seq_mat_4_transpose(dst, matrix);
}

#endif

#endif /* LINALG_H */
