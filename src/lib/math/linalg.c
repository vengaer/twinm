#include "linalg.h"

#if defined TWINM_TEST_BUILD || (!defined TWINM_SIMD_AVX && !defined TWINM_SIMD_SSE)

void seq_mv_mul_4(float *dst, float const *matrix, float const *vec) {
    for(unsigned i = 0; i < 4; i++) {
        dst[i] = 0.f;
        for(unsigned j = 0; j < 4; j++) {
            dst[i] += matrix[4 * i + j] * vec[j];
        }
    }
}

void seq_mat_4_transpose(float *dst, float const *matrix) {
    for(unsigned i = 0; i < 16; i++) {
        dst[i] = matrix[(i * 4) % 16 + i / 4];
    }
}

#endif /* defined TWINM_TEST_BUILD || (!defined TWINM_SIMD_AVX && !defined TWINM_SIMD_SSE) */
