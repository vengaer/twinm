#ifndef MUTEX_H
#define MUTEX_H

#include "macro.h"
#include "twinm_types.h"

#if defined TWINM_HOST_LINUX

#include <pthread.h>

#elif defined TWINM_HOST_WINDOWS

#include <synchapi.h>

#endif /* defined TWINM_HOST_LINUX */

#if defined TWINM_HOST_LINUX

typedef pthread_mutex_t mutex_handle;

static inline int mutex_init(mutex_handle *mtx) {
    return pthread_mutex_init(mtx, 0);
}

static inline int mutex_destroy(mutex_handle *mtx) {
    return pthread_mutex_destroy(mtx);
}

static inline int mutex_lock(mutex_handle *mtx) {
    return pthread_mutex_lock(mtx);
}

static inline int mutex_try_lock(mutex_handle *mtx) {
    return pthread_mutex_trylock(mtx);
}

static inline int mutex_unlock(mutex_handle *mtx) {
    return pthread_mutex_unlock(mtx);
}

#elif defined TWINM_HOST_WINDOWS

typedef CRITICAL_SECTION mutex_handle;

static inline int mutex_init(mutex_handle *mtx) {
    return !InitializeCriticalSectionAndSpinCount(mtx, 0x200);
}

static inline int mutex_destroy(mutex_handle *mtx) {
    DeleteCriticalSection(mtx);
    /* No status indication from winapi */
    return 0;
}

static inline int mutex_lock(mutex_handle *mtx) {
    EnterCriticalSection(mtx);
    /* No status indication from winapi */
    return 0;
}

static inline int mutex_try_lock(mutex_handle *mtx) {
    /* Return 0 on successful mutex acquisition */
    return !TryEnterCriticalSection(mtx);
}

static inline int mutex_unlock(mutex_handle *mtx) {
    LeaveCriticalSection(mtx);
    /* No status indication from winapi */
    return 0;
}

#endif /* defined TWINM_HOST_LINUX */

/* Lock guard internal implementation
 *
 * @lock:   pointer to the mutex to lock
 * @status: pointer to int. If failing to aqcuire the lock
 *          and status is not NULL, a non-zero status will
 *          be writted into the location pointed to by status.
 *          Assumed to be thread-private
 */
#define lock_guard_overload_2(lock, status)    \
    guard(mutex_lock(lock), mutex_unlock(lock), status, -ELOCK)


#define lock_guard_overload_1(lock) \
    lock_guard_overload_2(lock, 0)

/* Lock guard interface, takes a lock and, optionally,
 * a pointer to int for reporting status
 *
 * @lock:   pointer to the mutex to lock
 * @status: (optional) pointer to int. If a non-NULL value is given,
 *          the status will be set to non-zero if the lock was not acquired
 */
#define lock_guard(...)\
    overload(lock_guard_overload_, __VA_ARGS__)

#endif /* MUTEX_H */
