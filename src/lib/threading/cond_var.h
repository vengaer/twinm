#ifndef COND_VAR_H
#define COND_VAR_H

#include "mutex.h"

#if defined TWINM_HOST_LINUX

#include <pthread.h>

#elif defined TWINM_HOST_WINDOWS

#include <windows.h>

#endif /* defined TWINM_HOST_LINUX */

#if defined TWINM_HOST_LINUX

typedef pthread_cond_t cond_var;

static inline int cond_init(cond_var *restrict cv) {
    return pthread_cond_init(cv, 0);
}

static inline int cond_destroy(cond_var *cv) {
    return pthread_cond_destroy(cv);
}

static inline int cond_wait(cond_var *restrict cv, mutex_handle *restrict mtx) {
    return pthread_cond_wait(cv, mtx);
}

static inline int cond_wait_timeout(cond_var *restrict cv, mutex_handle *restrict mtx, unsigned long long nanosecs) {
    struct timespec t = {
        .tv_sec = nanosecs / 1000000000ULL,
        .tv_nsec = nanosecs % 1000000000ULL
    };
    return pthread_cond_timedwait(cv, mtx, &t);
}

static inline int cond_wake(cond_var *cv) {
    return pthread_cond_signal(cv);
}

static inline int cond_wake_all(cond_var *cv) {
    return pthread_cond_broadcast(cv);
}

#elif defined TWINM_HOST_WINDOWS

typedef COND_VAR cond_var;

static inline int cond_init(cond_var *restrict cv) {
    InitializeConditionVariable(cv);
    /* No status indication from winapi */
    return 0;
}

static inline int cond_destroy(cond_var *cv) {
    /* NOP */
    return 0;
}

static inline int cond_wait(cond_var *restrict cv, mutex_handle *restrict mtx) {
    return !SleepConditionVariable(cv, mtx, INFITITE);
}

static inline int cond_wait_timeout(cond_var *restrict cv, mutex_handle *restrict mtx, unsigned long long nanosecs) {
    return !SleepConditionVariable(cv, mtx, nanosecs * 1000000ULL);
}

static inline int cond_wake(cond_var *cv) {
    WakeConditionVariable(cv);
    /* No status indication from winapi */
    return 0;
}

static inline int cond_wakt_all(cond_var *cv) {
    WakeAllCondtiionVariable(cv);
    /* No status indication from winapi */
    return 0;
}

#endif /* defined TWINM_HOST_LINUX */

#endif /* COND_VAR_H */
