#ifndef THREAD_H
#define THREAD_H

#include "macro.h"
#include "twinm_types.h"

#if defined TWINM_HOST_LINUX

#include <pthread.h>
#include <unistd.h>

#elif defined TWINM_HOST_WINDOWS

#include <windows.h>

#endif /* defined TWINM_HOST_LINUX */

#if defined TWINM_HOST_LINUX

typedef pthread_t thread_handle;

static inline int thread_fork(thread_handle *thread, void *(*routine)(void *), void *args) {
    return pthread_create(thread, 0, routine, args);
}

static inline int thread_join(thread_handle thread, void **retval) {
    return pthread_join(thread, retval);
}

static inline int thread_detach(thread_handle thread) {
    return pthread_detach(thread);
}

static inline void thread_exit(void *retval) {
    pthread_exit(retval);
}

static inline thread_handle thread_self(void) {
    return pthread_self();
}

#elif defined TWINM_HOST_WINDOWS

static inline void sleep(unsigned secs) {
    Sleep(1000 * secs);
}

#endif /* defined TWINM_HOST LINUX */

/* Fork and join thread at start and end of scope, respectively
 *
 * @thread:  pointer to thread_handle
 * @routine: entry point for thread, signature void *(*)(void *)
 * @args:    arguments for thread entry point
 * @retaddr: pointer to pointer (e.g. void **) that will be set to
 *           point to the return value of the thread entry point
 * @status:  pointer to int. Set to 0 on success and -EFORK on
 *           failure
 */
#define thread_guard(thread, routine, args, retaddr, status) \
    guard(thread_fork(thread, routine, args), thread_join(*thread, retaddr), status, -EFORK)

#endif /* THREAD_H */
