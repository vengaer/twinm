#include "strutils.h"
#include "twinm_log.h"

#include <stdarg.h>
#include <time.h>

enum { TIMESTAMP_SIZE = 32 };
enum { LOGGER_LVL_STR_SIZE = 16 };

/* Logger file pointer */
static FILE *logfilep;

/* Log level */
static enum log_level logger_current_lvl = TWINM_LOG_DEFAULT_LVL;

static inline char const *log_lvl_str(enum log_level lvl) {
    switch(lvl) {
        case ll_err:
            return "Error";
        case ll_warn:
            return "Warning";
        case ll_dbg1:
        case ll_dbg2:
        case ll_dbg3:
            return "Debug";
        default:
            /* Nop */
            break;
    }
    return "Unknown";
}

static void logger_add_stamp(FILE *fp, enum log_level lvl) {
    char timestamp[TIMESTAMP_SIZE];

    time_t ctime = time(0);

    strscpy(timestamp, asctime(localtime(&ctime)), sizeof(timestamp));
    strrepl(timestamp, '\n', '\0');

    fprintf(fp, "[%s]: <%s> ", timestamp, log_lvl_str(lvl));
}

bool logger_init(void) {
    logfilep = fopen(LOG_FILE, "a");
    return logfilep;
}

void logger_close(void) {
    fclose(logfilep);
}

void logger_set_level(enum log_level lvl) {
    logger_current_lvl = lvl;
}

enum log_level logger_level(void) {
    return logger_current_lvl;
}

void twinm_log(enum log_level lvl, char const *restrict fmt, ...) {
    if(logger_current_lvl > lvl) {
        /* Not severe enough */
        return;
    }
    logger_add_stamp(logfilep, lvl);
    va_list args;
    va_start(args, fmt);
    /* Write to log file */
    vfprintf(logfilep, fmt, args);

    /* Write warnings and errors to stderr */
    if(lvl >= ll_warn) {
        logger_add_stamp(stderr, lvl);
        vfprintf(stderr, fmt, args);
    }

    va_end(args);
}

void twinm_logf(FILE *fp, enum log_level lvl, char const *restrict fmt, ...) {
    if(logger_current_lvl > lvl) {
        /* Not severe enough */
        return;
    }
    logger_add_stamp(fp, lvl);
    va_list args;
    va_start(args, fmt);
    /* Write only to designated file */
    vfprintf(fp, fmt, args);
    va_end(args);
}
