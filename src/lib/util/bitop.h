#ifndef BITOP_H
#define BITOP_H

#define nand(op0, op1) \
    !((op0) & (op1))

#define nor(op0, op1) \
    !((op0) | (op1))

#define xnor(op0, op1) \
    !((op0) ^ (op1))

#define max(x, y) \
    ((x) ^ (((x) ^ (y)) & -((x) < (y))))

#define min(x, y) \
    ((y) ^ (((x) ^ (y)) & -((x) < (y))))

#endif /* BITOP_H */
