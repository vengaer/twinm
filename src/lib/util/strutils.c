#include "strutils.h"

#include <string.h>

#if defined TWINM_TEST_BUILD || (!defined TWINM_SIMD_AVX && !defined TWINM_SIMD_SSE)

#include <ctype.h>

#endif /* defined TWINM_TEST_BUILD || (!defined TWINM_SIMD_AVX && !defined TWINM_SIMD_SSE) */

ssize_t strscpy(char *restrict dst, char const *restrict src, size_t dstlen) {
    size_t const srclen = strlen(src);
    size_t i = dstlen;
    char *d = dst;

    while(i-- && (*d++ = *src++));

    if(srclen >= dstlen) {
        dst[dstlen - 1] = '\0';
        return -E2BIG;
    }

    return srclen;
}

ssize_t strsncpy(char *restrict dst, char const *restrict src, size_t dstlen, size_t n) {
    size_t const srclen = strlen(src);
    size_t i = dstlen;
    size_t nn = n;
    char *d = dst;

    while(i-- && nn-- && (*d++ = *src++));

    if(n >= dstlen) {
        dst[dstlen - 1] = '\0';
        return -E2BIG;
    }
    if(srclen < n) {
        return -E2BIG;
    }

    dst[n] = '\0';

    return n;
}

void strrepl(char *restrict str, char from, char to) {
    for(; *str; ++str) {
        if(*str == from) {
            *str = to;
        }
    }
}

#if defined TWINM_TEST_BUILD || (!defined TWINM_SIMD_AVX && !defined TWINM_SIMD_SSE)

int seq_string_tolower(char *dst, char const *src, unsigned dst_size) {
    size_t src_len = strlen(src);

    if(src_len >= dst_size) {
        return -E2BIG;
    }

    for(; *src; ++src, ++dst) {
        *dst = tolower(*src);
    }

    *dst = '\0';

    return 0;
}

int seq_string_toupper(char *dst, char const *src, unsigned dst_size) {
    size_t src_len = strlen(src);

    if(src_len >= dst_size) {
        return -E2BIG;
    }

    for(; *src; ++src, ++dst) {
        *dst = toupper(*src);
    }

    *dst = '\0';

    return 0;
}

#endif /* defined TWINM_TEST_BUILD || (!defined TWINM_SIMD_AVX && !defined TWINM_SIMD_SSE) */
