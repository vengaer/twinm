#ifndef TWINM_TYPES_H
#define TWINM_TYPES_H

#if defined TWINM_HOST_LINUX

#include <stdint.h>

#include <sys/types.h>

#elif defined TWINM_HOST_WINDOWS

#include <BaseTsd.h>

#endif /* defined TWINM_HOST_LINUX */

#define EPSILON 1e-5

typedef unsigned char regex_char;

enum {
    E2BIG               = 7,
    EFORMAT             = 125,
    EHEAP_ALLOC         = 126,
    EMALFORMED_ESCAPE   = 127,
    ELOCK               = 128,
    EEMPTY              = 129,
    EOPEN               = 130,
    EFORK               = 131
};

#if defined TWINM_HOST_LINUX

typedef uint8_t byte;
typedef uint16_t word;
typedef uint32_t dword;
typedef uint64_t qword;

#elif defined TWINM_HOST_WINDOWS

typedef long long ssize_t;

typedef BYTE byte;
typedef WORD word;
typedef DWORD dword;
typedef QWORD qword;

#endif /* defined TWINM_HOST_LINUX */

#endif /* TWINM_TYPES_H */
