#ifndef TWINM_DEBUG_H
#define TWINM_DEBUG_H

#include "macro.h"

#include <stddef.h>
#include <stdio.h>

#define debug_format(x)             \
    _Generic((x),                   \
        char: "%c ",                \
        signed char: "%hhd ",       \
        unsigned char: "%hhu ",     \
        short: "%hd ",              \
        unsigned short: "%hu ",     \
        int: "%d ",                 \
        unsigned: "%u ",            \
        long: "%ld ",               \
        unsigned long: "%lu ",      \
        long long: "%lld ",         \
        unsigned long long: "%llu ",\
        char *: "%s ",              \
        char const *: "%s ",        \
        void *: "%p ",              \
        float: "%f ",               \
        double: "%f ",              \
        long double: "%Ld "         \
    )

#define print(...)                  \
    overload(print_overload_, __VA_ARGS__)

#define put_value(x)                \
    printf(debug_format(x), x)

#define put_value_nl(x)             \
    printf(debug_format(x), x);     \
    puts("")

#define print_overload_1(x)         \
    put_value_nl(x)

#define print_overload_2(x, ...)    \
    put_value(x);                   \
    print_overload_1(__VA_ARGS__)

#define print_overload_3(x, ...)    \
    put_value(x);                   \
    print_overload_2(__VA_ARGS__)

#define print_overload_4(x, ...)    \
    put_value(x);                   \
    print_overload_3(__VA_ARGS__)

#define print_overload_5(x, ...)    \
    put_value(x);                   \
    print_overload_4(__VA_ARGS__)

#define print_overload_6(x, ...)    \
    put_value(x);                   \
    print_overload_5(__VA_ARGS__)

#define print_overload_7(x, ...)    \
    put_value(x);                   \
    print_overload_6(__VA_ARGS__)

#define print_overload_8(x, ...)    \
    put_value(x);                   \
    print_overload_7(__VA_ARGS__)

#define print_overload_9(x, ...)    \
    put_value(x);                   \
    print_overload_8(__VA_ARGS__)

#define print_overload_10(x, ...)   \
    put_value(x);                   \
    print_overload_9(__VA_ARGS__)

#define print_overload_11(x, ...)   \
    put_value(x);                   \
    print_overload_10(__VA_ARGS__)

#define print_overload_12(x, ...)   \
    put_value(x);                   \
    print_overload_11(__VA_ARGS__)

#define print_overload_13(x, ...)   \
    put_value(x);                   \
    print_overload_12(__VA_ARGS__)

#define print_overload_14(x, ...)   \
    put_value(x);                   \
    print_overload_13(__VA_ARGS__)

#define print_overload_15(x, ...)   \
    put_value(x);                   \
    print_overload_14(__VA_ARGS__)

#define print_overload_16(x, ...)   \
    put_value(x);                   \
    print_overload_15(__VA_ARGS__)

#endif /* TWINM_DEBUG_H */
