#ifndef MACRO_H
#define MACRO_H

#include <stddef.h>

/* Get byte offset of member in type
 *
 * @type:   name of data type
 * @member: name of the member
 */
#define member_offset(type, member) \
    ((size_t) &((type *)0)->member)

/* Get pointer to struct instance containing
 * *addr.
 *
 * @addr:   pointer to member
 * @type:   name of data type
 * @member: name of the member
 * @qual:   cv qualifier
 */
#define container_overload_4(addr, type, member, qual) \
    ((type qual *) ((char qual *)addr - member_offset(type, member)))

/* Container overload accepting 3 parameters.
 * See container(...) for args
 */
#define container_overload_3(addr, type, member) \
    container_overload_4(addr, type, member,)

/* Get type instance containing the member type at address addr.
 *
 * Takes 3 or 4 parameters:
 *
 * @addr:   pointer to member
 * @type:   name of data type
 * @member: name of the member
 * @qual:   (optional) cv qualifier
 */
#define container(...) \
    overload(container_overload_, __VA_ARGS__)

/* Compute number of bytes between ptr0 and ptr1
 *
 * @ptr0: the lower address
 * @ptr1: the higher address
 */
#define bytediff(ptr0, ptr1) \
    ((char const *)(ptr1) - (char const *)(ptr0))

/* Compute the index of ptr in array
 *
 * @array: pointer to first element of array
 * @ptr:   pointer to the element whose index is to be computed
 */
#define array_index(array, ptr) \
    (size_t)(bytediff(&(array)[0], (ptr)) / sizeof(*ptr))

#define array_size(arr) \
    (sizeof(arr) / sizeof((arr)[0]))

/* Type-safe comparison
 *
 * @type: the type to interpret x and y as
 * @x:    the first value to be compared
 * @op:   a binary comparison operator
 * @y:    the second value to be compared
 */
#define SAFECMP(type, x, op, y) \
    (((type)(x)) op ((type)(y)))

#define NARGS_REV_SEQUENCE 16, 15, 14, 13, 12, 11, 10, 9, \
                           8,  7,  6,  5,  4,  3,  2,  1, 0

#define nargs_pick(a0,  a1,  a2,  a3,  a4,  a5,  a6,  a7,  a8,  a9, \
                   a10, a11, a12, a13, a14, a15, a16,  ...) a16

#define nargs_expand(...) nargs_pick(__VA_ARGS__)

/* Compute number of arguments passed to the macro */
#define nargs(...)  nargs_expand(__VA_ARGS__, NARGS_REV_SEQUENCE)

#define overload(name, ...) \
    CAT_EXPAND(name,nargs(__VA_ARGS__))(__VA_ARGS__)

#define CAT(a,b) a##b
#define CAT_EXPAND(a,b) CAT(a,b)

#define UNUSED_A1(a) (void)a
#define UNUSED_A2(a,...) UNUSED_A1(a); UNUSED_A1(__VA_ARGS__)
#define UNUSED_A3(a,...) UNUSED_A1(a); UNUSED_A2(__VA_ARGS__)
#define UNUSED_A4(a,...) UNUSED_A1(a); UNUSED_A3(__VA_ARGS__)
#define UNUSED_A5(a,...) UNUSED_A1(a); UNUSED_A4(__VA_ARGS__)
#define UNUSED_A6(a,...) UNUSED_A1(a); UNUSED_A5(__VA_ARGS__)
#define UNUSED_A7(a,...) UNUSED_A1(a); UNUSED_A6(__VA_ARGS__)
#define UNUSED_A8(a,...) UNUSED_A1(a); UNUSED_A7(__VA_ARGS__)

/* Prevent compiler from issuing warnings about arguments being unused */
#define UNUSED(...) overload(UNUSED_A,__VA_ARGS__)

#define is_immediate(x)                                     \
    _Generic((*(1 ? (void *)((long)(x) * 0l) : (int *)1)),  \
        int: 1,                                             \
        default: 0                                          \
    )

/* Generic guard macro. Used for executing code at start
 * and end of scope.
 *
 * @init_expr:    the initialization expression, must
 *                evaluate to an integer. If initialization
 *                is not required, an immediate may be
 *                passed
 * @cleanup_expr: the cleanup expression, must evaluate to
 *                an integer
 * @status:       pointer to integer for reporting status.
 *                If the init expression evaluates to
 *                non-zero, *status is set to error_code.
 *                If the init expression does evaluate to
 *                zero, status is set to 0 at scope exit.
 *                If no status is required, pass 0.
 * @error_code:   error code to set *status to on init
 *                failure
 */
#define guard(init_expr, cleanup_expr, status, error_code)  \
    for(int CAT_EXPAND(guard_var_,__LINE__) =               \
        ((status == 0 ? 0 : (*(int *)status = error_code)), \
        init_expr);                                         \
        !CAT_EXPAND(guard_var_, __LINE__);                  \
        CAT_EXPAND(guard_var_, __LINE__) =                  \
        ((status == 0 ? 0 : (*(int *)status = 0)),          \
        cleanup_expr,1))

#endif /* MACRO_H */
