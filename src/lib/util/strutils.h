#ifndef STRUTILS_H
#define STRUTILS_H

#include "twinm_types.h"

#include <stddef.h>
#include <string.h>

#if defined TWINM_SIMD_AVX || defined TWINM_SIMD_SSE

#include "simd.h"

#endif /* defined TWINM_SIMD_AVX || defined TWINM_SIMD_SSE */

/* "Safe" strcpy ensuring null-termination.  Return length of string written
 * to dst, or -E2BIG if dst is not large enough
 *
 * @dst:    destination string
 * @src:    source string
 * @dstlen: size of destination string
 */
ssize_t strscpy(char *restrict dst, char const *restrict src, size_t dstlen);

/* "Safe" strncpy ensuring null-termination.  Return length of string written
 * to dst, or -E2BIG if dst is not large enough
 *
 * @dst:    destination string
 * @src:    source string
 * @dstlen: size of destination string
 * @n:      number of chars to copy
 */
ssize_t strsncpy(char *restrict dst, char const *restrict src, size_t dstlen, size_t n);

/* Replace any occurent of 'from' in 'str' with 'to' */
void strrepl(char *restrict str, char from, char to);

/* Convert an entire string to lower case, SIMD accelerated when applicable
 *
 * Errors indicated by negative return value. Does not null-terminate on
 * error (but always on success)
 *
 * @dst:      pointer to buffer for storing the result
 * @src:      the source string
 * @dst_size: size of dst buffer
 */
#if defined TWINM_TEST_BUILD || (!defined TWINM_SIMD_AVX && !defined TWINM_SIMD_SSE)

int seq_string_tolower(char *dst, char const *src, unsigned dst_size);

#endif /* defined TWINM_TEST_BUILD || (!defined TWINM_SIMD_AVX && !defined TWINM_SIMD_SSE) */

#if defined TWINM_SIMD_AVX

static inline int strlower(char *dst, char const *src, unsigned dst_size) {
    return avx_string_tolower(dst, src, dst_size, strlen(src));
}

#elif defined TWINM_SIMD_SSE

static inline int strlower(char *dst, char const *src, unsigned dst_size) {
    return sse_string_tolower(dst, src, dst_size, strlen(src));
}

#else

static inline int strlower(char *dst, char const *src, unsigned dst_size) {
    return seq_string_tolower(dst, src, dst_size);
}

#endif /* defined TWINM_SIMD_AVX */

/* Convert an entire string to upper case, SIMD accelerated when applicable
 *
 * Errors indicated by negative return value. Does not null-terminate on
 * error (but always on success)
 *
 * @dst:      pointer to buffer for storing the result
 * @src:      the source string
 * @dst_size: size of dst buffer
 */
#if defined TWINM_TEST_BUILD || (!defined TWINM_SIMD_AVX && !defined TWINM_SIMD_SSE)

int seq_string_toupper(char *dst, char const *src, unsigned dst_size);

#endif /* defined TWINM_TEST_BUILD || (!defined TWINM_SIMD_AVX && !defined TWINM_SIMD_SSE) */

#if defined TWINM_SIMD_AVX

static inline int strupper(char *dst, char const *src, unsigned dst_size) {
    return avx_string_toupper(dst, src, dst_size, strlen(src));
}

#elif defined TWINM_SIMD_SSE

static inline int strupper(char *dst, char const *src, unsigned dst_size) {
    return sse_string_toupper(dst, src, dst_size, strlen(src));
}

#else

static inline int strupper(char *dst, char const *src, unsigned dst_size) {
    return seq_string_toupper(dst, src, dst_size);
}

#endif /* defined TWINM_SIMD_AVX */

/* Zero nbytes bytes starting at addr */
#if defined TWINM_SIMD_AVX

static inline void *memzero(void *addr, unsigned nbytes) {
    return avx_memzero(addr, nbytes);
}

#elif defined TWINM_SIMD_SSE

static inline void *memzero(void *addr, unsigned nbytes) {
    return sse_memzero(addr, nbytes);
}

#else

static inline void *memzero(void *addr, unsigned nbytes) {
    return addr ? memset(addr, 0, nbytes) : addr;
}

#endif /* defined TWIM_SIMD_AVX */

#endif /* STRUTILS_H */
