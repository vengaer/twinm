#ifndef TWINM_LOG_H
#define TWINM_LOG_H

#include <stdbool.h>
#include <stdio.h>

#define TWINM_LOG_DEFAULT_LVL ll_warn
#define LOG_FILE "twinm.log"

enum log_level {
    ll_dbg3,
    ll_dbg2,
    ll_dbg1,
    ll_warn,
    ll_err
};

void twinm_log(enum log_level lvl, char const *restrict fmt, ...);
void twinm_logf(FILE *fp, enum log_level lvl, char const *restrict fmt, ...);

bool logger_init(void);
void logger_close(void);

void logger_set_level(enum log_level lvl);
enum log_level logger_level(void);

#endif /* TWINM_LOG_H */
