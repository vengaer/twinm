#include "msgbus.h"
#include "strutils.h"
#include "twinm_types.h"

#include <stdbool.h>
#include <stdlib.h>

enum { MSGBUS_BUCKET_SIZE = 16 };
enum { MSGBUS_LOG_LINE_SIZE = 128 };

struct busmsg {
    char content[MSGBUS_LOG_LINE_SIZE];
};

struct msgbus_bucket {
    struct busmsg messages[MSGBUS_BUCKET_SIZE];
    struct msgbus_bucket *next;
    atomic_uint nsubscribers;
};

/* Allocate and initialize a new bucket */
static inline bool msgbus_init_bucket(struct msgbus_bucket **bucket) {
    *bucket = calloc(1, sizeof(**bucket));
    return *bucket;
}

/* Destroy bucket */
static inline void msgbus_destroy_bucket(struct msgbus_bucket *bucket) {
    free(bucket);
}

/* Append bucket to bus */
static inline bool msgbus_append_bucket(struct msgbus *bus) {
    if(!msgbus_init_bucket(&bus->current->next)) {
        return false;
    }
    bus->current = bus->current->next;
    return true;
}

/* Compute next message index */
static inline void msgbus_advance_index(unsigned *index) {
    *index = (*index + 1) % MSGBUS_BUCKET_SIZE;
}

/* Compute next message index using atomic cmpxchg */
static inline void msgbus_atomic_advance_index(atomic_uint *index) {
    unsigned expected;
    unsigned newval = *index;

    msgbus_advance_index(&newval);

    do {
        expected = *index;
    } while(!atomic_compare_exchange_weak(index, &expected, newval));
}

/* Discard leading buckets without subscribers */
static int msgbus_discard(struct msgbus *bus) {
    struct msgbus_bucket *bucket;

    int lock_failed = 0;

    lock_guard(&bus->mutex, &lock_failed) {
        while(bus->head != bus->current && !bus->head->nsubscribers) {
            bucket = bus->head;
            bus->head = bus->head->next;
            msgbus_destroy_bucket(bucket);
        }
    }
    return lock_failed;
}

/* Initialize message bus */
int msgbus_init(struct msgbus *bus) {
    if(!msgbus_init_bucket(&bus->current)) {
        return -EHEAP_ALLOC;
    }

    bus->head = bus->current;
    bus->msg_index = 0;
    bus->nsubscribers = 0;
    return mutex_init(&bus->mutex);
}

/* Deallocate message bus */
int msgbus_destroy(struct msgbus *bus) {
    while(bus->head) {
        bus->current = bus->head->next;
        msgbus_destroy_bucket(bus->head);
        bus->head = bus->current;
    }
    return -mutex_destroy(&bus->mutex);
}

/* Append message to bus */
int msgbus_append(struct msgbus *bus, char const *msg) {
    if(!bus->nsubscribers) {
        /* No use keeping messages */
        return 0;
    }

    int status = 0;
    int lock_failed = 0;
    struct msgbus_bucket *bucket = bus->current;

    lock_guard(&bus->mutex, &lock_failed) {
        if(bus->msg_index == MSGBUS_BUCKET_SIZE - 1) {
        /* Pre-allocate next bucket (since readers might
         * pass the current index) */
            if(!msgbus_append_bucket(bus)) {
                status = -EHEAP_ALLOC;
            }
        }
        if(!status) {
            /* Store message */
            status = strscpy(bucket->messages[bus->msg_index].content, msg,
                        sizeof(bucket->messages[bus->msg_index].content));
            msgbus_atomic_advance_index(&bus->msg_index);
        }
    }

    return (status < 0) | lock_failed;
}

/* Subscribe to bus */
int msgbus_subscribe(struct msgbus *bus, struct msgbus_subscription *sub) {
    int lock_failed = 0;

    atomic_fetch_add(&bus->nsubscribers, 1);
    lock_guard(&bus->mutex, &lock_failed) {
        atomic_fetch_add(&bus->current->nsubscribers, 1);
        sub->bucket = bus->current;
        sub->msg_index = bus->msg_index;
    }
    sub->bus = bus;

    return lock_failed;
}

int msgbus_unsubscribe(struct msgbus_subscription *sub) {
    int prev_sub_count = atomic_fetch_sub(&sub->bucket->nsubscribers, 1);

    if(prev_sub_count == 1) {
        /* No subscribers to bucket, discard */
        return msgbus_discard(sub->bus);
    }
    return 0;
}

/* Read next message from bus */
int msgbus_try_get(char *dst, struct msgbus_subscription *sub, size_t dstbuf_size) {
    int status = 0;
    int lock_failed = 0;

    if(sub->msg_index == sub->bus->msg_index) {
        /* sub->bus->current may change while trying to acquire lock
         * in which case there is no need to return */
        lock_guard(&sub->bus->mutex, &lock_failed) {
            status = (sub->bucket == sub->bus->current) * -EEMPTY;
        }

        if(lock_failed) {
            return lock_failed;
        }
        if(status) {
            return status;
        }
    }

    /* Bucket will not be destroyed while it has subscribers, no need
     * to lock */
    status = strscpy(dst, sub->bucket->messages[sub->msg_index].content, dstbuf_size);

    if(status < 0) {
        return status;
    }

    status = 0;

    msgbus_advance_index(&sub->msg_index);

    int prev_sub_count;
    /* End of bucket */
    if(!sub->msg_index) {
        /* Decrement number of subscribers to current bucket */
        prev_sub_count = atomic_fetch_sub(&sub->bucket->nsubscribers, 1);
        /* Next bucket */
        sub->bucket = sub->bucket->next;
        /* Add subscriber to next bucket */
        atomic_fetch_add(&sub->bucket->nsubscribers, 1);

        if(prev_sub_count == 1) {
            /* No subscribers to bucket, discard */
            status = msgbus_discard(sub->bus);
        }
    }


    return status;
}
