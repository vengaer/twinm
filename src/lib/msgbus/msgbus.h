#ifndef MSGBUS_H
#define MSGBUS_H

#include "mutex.h"

#include <stdatomic.h>
#include <stddef.h>

struct msgbus_bucket;

struct msgbus {
    struct msgbus_bucket *head;
    struct msgbus_bucket *current;
    atomic_uint msg_index;
    atomic_uint nsubscribers;
    mutex_handle mutex;
};

struct msgbus_subscription {
    struct msgbus *bus;
    struct msgbus_bucket *bucket;
    unsigned msg_index;
};

/* Initialize the message bus */
int msgbus_init(struct msgbus *bus);
/* Destroy the message bus, not thread safe */
int msgbus_destroy(struct msgbus *bus);

/* Append message to bus */
int msgbus_append(struct msgbus *bus, char const *msg);

/* Subscribe to bus */
int msgbus_subscribe(struct msgbus *bus, struct msgbus_subscription *sub);

/* Unsubscribe from bus */
int msgbus_unsubscribe(struct msgbus_subscription *sub);

/* Get message from bus
 *
 * @dst:         buffer to write message to
 * @sub:         the bus subscription
 * @dstbus_size: size of destination buffer
 */
int msgbus_try_get(char *dst, struct msgbus_subscription *sub, size_t dstbuf_size);

#endif /* MSGBUS_H */
