    section .text

    global avx_mat_4_transpose

; Transpose 4x4 matrix
; Params:
;     rdi: pointer to destination array (16 floats)
;     rsi: pointer to source array (16 floats)
; Return:
;     -
avx_mat_4_transpose:
    vmovups ymm0, [rsi]                     ; First 2 rows to ymm0
    vmovups ymm1, [rsi + 32]                ; Last 2 rows to ymm1

    vunpcklps       ymm2, ymm0, ymm1        ; Unpack and interleave low lanes

    vmovaps ymm4, ymm2                      ; Intermediate to ymm4

    vextractf128    xmm3, ymm2, 1           ; High 128 bits of ymm2 to xmm3
    vmovaps         xmm5, xmm3              ; Intermediate to xmm5
    vunpcklps       xmm2, xmm3              ; Unpack and interleave low lanes

    vunpckhps       xmm4, xmm5              ; Unpack and interleave high lanes
    vinsertf128     ymm2, ymm2, xmm4, 1     ; Values from xmm4 to 128 high bits of ymm2

    vmovups [rdi], ymm2                     ; First 2 rows to memory

    vunpckhps       ymm2, ymm0, ymm1        ; Unpack and interleave high lanes

    vmovaps ymm4, ymm2                      ; Intermediate to xmm4

    vextractf128    xmm3, ymm2, 1           ; High 128 bits of ymm2 to xmm3
    vmovaps         xmm5, xmm3              ; Intermediate to xmm3
    vunpcklps       xmm2, xmm3              ; Unpack and interleave low lanes

    vunpckhps       xmm4, xmm5              ; Unpack and interleave high lanes
    vinsertf128     ymm2, ymm2, xmm4, 1     ; Values from xmm4 to 128 high bits of ymm2

    vmovups [rdi + 32], ymm2                ; Last 2 rows to memory

    vzeroupper                              ; Zero upper 128 bits of ymm registers
    ret
