    section .text

    global sse_mat_4_transpose

; Transpose 4x4 matrix
; Params:
;     rdi: pointer to destination array (16 floats)
;     rsi: pointer to source array (16 floats)
; Return:
;     -
sse_mat_4_transpose:
    movups  xmm0, [rsi]                     ; First row to xmm0
    movups  xmm1, [rsi + 16]                ; Second row to xmm1
    movups  xmm2, [rsi + 32]                ; Third row to xmm2
    movups  xmm3, [rsi + 48]                ; Fourth row to xmm3

    movaps  xmm4, xmm0                      ; Copy first row to xmm4
    unpcklps    xmm4, xmm1                  ; Unpack and interleave low lanes

    movaps  xmm5, xmm2                      ; Copy third row to xmm5
    unpcklps    xmm5, xmm3                  ; Unpack and interleave low lanes

    movaps  xmm6, xmm4                      ; Copy xmm4 to xmm6
    shufps  xmm6, xmm5, 0x44                ; Low lanes of xmm5 to high lanes of xmm6

    movups  [rdi], xmm6                     ; First row to memory

    shufps  xmm4, xmm5, 0xee                ; High lanes of xmm4 to low lanes of xmm4
                                            ; High lanes of xmm5 to high lanes of xmm4

    movups  [rdi + 16], xmm4                ; Second row to memory

    unpckhps    xmm0, xmm1                  ; Unpack and interleave high lanes
    unpckhps    xmm2, xmm3                  ; Unpack and interleave high lanes

    movaps  xmm1, xmm0                      ; Copy xmm0 to xmm1
    shufps  xmm1, xmm2, 0x44                ; Low lanes of xmm2 to high lanes of xmm1

    movups  [rdi + 32], xmm1                ; Third row to memory

    shufps  xmm0, xmm2, 0xee                ; High lanes of xmm0 to low lanes of xmm0
                                            ; High lanes of xmm2 to high lanes of xmm0

    movups  [rdi + 48], xmm0                ; Fourth row to memory

    ret
