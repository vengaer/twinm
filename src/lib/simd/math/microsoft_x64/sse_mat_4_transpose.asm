    section .text

    global sse_mat_4_transpose

; Transpose 4x4 matrix
; Params:
;     rcx: pointer to destination array (16 floats)
;     rdx: pointer to source array (16 floats)
; Return:
;     -
sse_mat_4_transpose:
    movups  xmm0, [rdx]                     ; First row to xmm0
    movups  xmm1, [rdx + 16]                ; Second row to xmm1
    movups  xmm2, [rdx + 32]                ; Third row to xmm2
    movups  xmm3, [rdx + 48]                ; Fourth row to xmm3

    movaps  xmm4, xmm0                      ; Copy first row to xmm4
    unpcklps    xmm4, xmm1                  ; Unpack and interleave low lanes

    movaps  xmm5, xmm2                      ; Copy third row to xmm5
    unpcklps    xmm5, xmm3                  ; Unpack and interleave low lanes

    unpckhps    xmm0, xmm1                  ; Unpack and interleave high lanes
    unpckhps    xmm2, xmm3                  ; Unpack and interleave high lanes

    movaps  xmm1, xmm4                      ; Copy xmm4 to xmm1
    shufps  xmm1, xmm5, 0x44                ; Low lanes of xmm5 to high lanes of xmm1

    movups  [rcx], xmm1                     ; First row to memory

    shufps  xmm4, xmm5, 0xee                ; High lanes of xmm4 to low lanes of xmm4
                                            ; High lanes of xmm5 to high lanes of xmm5

    movups  [rcx + 16], xmm4                ; Second row to memory

    movaps  xmm1, xmm0                      ; Copy xmm0 to xmm1
    shufps  xmm1, xmm2, 0x44                ; Low lanes of xmm2 to high lanes of xmm1

    movups  [rcx + 32], xmm1                ; Third row to memory

    shufps  xmm0, xmm2, 0xee                ; High lanes of xmm0 to low lanes of xmm0
                                            ; High lanes of xmm2 to high lanes of xmm0

    movups  [rcx + 48], xmm0                ; Fourth row to memory

    ret
