    section .text

    global sse_mv_mul_4

; Multiply 4x4 matrix by 4x1 vector
; Params:
;     rcx: pointer to destination array (4 floats)
;     rdx: pointer to array of 16 floats
;     r8:  pointer to array of 4 floats
; Return:
;     -
sse_mv_mul_4:
    movups  xmm0, [r8]                      ; Vector to xmm0
    movups  xmm1, [rdx]                     ; First row to xmm1

    movaps  xmm2, xmm0                      ; Vector to xmm2
    dpps    xmm2, xmm1, 0xf1                ; Dot product of xmm1 and xmm2 to lane 0 of xmm2

    movups  xmm1, [rdx + 16]                ; Second row to xmm1

    movaps  xmm3, xmm0                      ; Vector to xmm3
    dpps    xmm3, xmm1, 0xf2                ; Dot product of xmm1 and xmm3 to lane 1 of xmm3
    orps    xmm2, xmm3                      ; Dot product to lane 2 of xmm2

    movups  xmm1, [rdx + 32]                ; Third row to xmm1

    movaps  xmm3, xmm0                      ; Vector to xmm3
    dpps    xmm3, xmm1, 0xf4                ; Dot product of xmm1 and xmm3 to lane 2 of xmm3
    orps    xmm2, xmm3                      ; Dot product to lane 3 of xmm2

    movups  xmm1, [rdx + 48]                ; Fourth row to xmm1

    dpps    xmm0, xmm1, 0xf8                ; Dot product of xmm0 and xmm1 to lane 3 of xmm3
    orps    xmm2, xmm0                      ; Dot product to lane 4 of xmm2

    movups  [rcx], xmm2                     ; Write to memory

    ret
