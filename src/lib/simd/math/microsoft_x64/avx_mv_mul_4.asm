    section .text

    global avx_mv_mul_4

; Multiply 4x4 matrix by 4x1 vector
; Params:
;     rcx: pointer to destination array (4 floats)
;     rdx: pointer to array of 16 floats
;     r8:  pointer to array of 4 floats
; Return:
;     -
avx_mv_mul_4:
    vmovups xmm0, [r8]                      ; Vector to xmm0
    vmovups ymm1, [rdx]                     ; First 2 rows to ymm1
    vmovups ymm2, [rdx + 32]                ; Last 2 rows to ymm2

    vextractf128    xmm3, ymm1, 0x1         ; High 128 bits of ymm1 to xmm3

    vdpps   xmm4, xmm0, xmm1, 0xf1          ; Dot product of xmm0 and xmm1 to lane 0 of xmm4

    vdpps   xmm5, xmm0, xmm3, 0xf2          ; Dot product of xmm0 and xmm3 to lane 1 of xmm5
    vorps   xmm4, xmm4, xmm5                ; Computed values to 2 low lanes of xmm4

    vextractf128    xmm3, ymm2, 0x1         ; High 128 bits of ymm2 to xmm3

    vdpps   xmm5, xmm0, xmm2, 0xf4          ; Dot product of xmm0 and xmm2 to lane 2 of xmm5
    vorps   xmm4, xmm4, xmm5                ; Dot product to lane 3 of xmm4

    vdpps   xmm5, xmm0, xmm3, 0xf8          ; Dot product of xmm0 and xmm3 to lane 3 of xmm5
    vorps   xmm4, xmm4, xmm5                ; Dot product to lane 4 of xmm4

    vmovups  [rcx], xmm4                    ; Write to memory

    vzeroupper                              ; Zero upper 128 bits of ymm registers
    ret
