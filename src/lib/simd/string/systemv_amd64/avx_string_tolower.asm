    section .rodata

    align 32
    diffs:  db 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32
    Alim:   db 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64
    Zlim:   db 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91

    section .text

    global avx_string_tolower

; AVX-accelerated tolower for byte string
; Params:
;     rdi: pointer to dst buffer
;     rsi: pointer to src string
;     edx: size of dst buffer
;     ecx: length of src string
; Return:
;     eax: 0 on success, negative value on failure
avx_string_tolower:
    mov     eax, -7                         ; Presume failure for now

    cmp     edx, ecx                        ; Src string too large for dst buffer?
    jbe     .epi

    xor     eax, eax                        ; Return code 0

    vmovdqa ymm5, [rel diffs]               ; Fill ymm5 with 32
    vmovdqa ymm3, [rel Alim]                ; Fill ymm3 with 64
    vmovdqa ymm4, [rel Zlim]                ; Fill ymm4 with 91

    mov     edx, ecx
    shr     ecx, 5                          ; Unsigned division by 32

    jz      .preproc16

    and     edx, 0x1F                       ; Bytes remaining after avx loop

.proc32:
    vmovdqu ymm0, [rsi]                     ; Next 32 bytes

    vmovdqa ymm1, ymm0
    vpcmpgtb    ymm1, ymm3                  ; All bits 1 if byte is greater than 64

    vmovdqa ymm2, ymm4
    vpcmpgtb    ymm2, ymm0                  ; All bits 1 if 91 is greater than byte

    vpand   ymm1, ymm2                      ; All 1s if byte greater than 64 and less than 91
    vpand   ymm1, ymm5                      ; Bytes in ymm1 are 32 iff corresponding byte in ymm0 is upper case, 0 otherwise
    vpaddb  ymm0, ymm1                      ; Convert upper to lower case

    vmovdqu [rdi], ymm0                     ; Store

    add     rdi, 32                         ; Advance dst pointer
    add     rsi, 32                         ; Advance src pointer
    dec     ecx
    jnz     .proc32

.preproc16:
    mov     ecx, edx                        ; Remaining bytes to ecx
    shr     ecx, 4                          ; Unsigned division by 16

    jz      .preproc1

    and     edx, 0xF                        ; Remaining bytes

.proc16:
    vmovdqu xmm0, [rsi]                     ; Next 16 bytes

    vmovdqa xmm1, xmm0
    vpcmpgtb    xmm1, xmm3                  ; All bits 1 if byte is greater than 64

    movdqa  xmm2, xmm4
    vpcmpgtb    xmm2, xmm0                  ; All bits 1 if 91 is greater than byte

    vpand   xmm1, xmm2                      ; All 1s if byte greater than 64 and less than 91
    vpand   xmm1, xmm5                      ; Bytes in xmm1 are 32 iff corresponding byte in xmm0 is upper case, 0 otherwise
    vpaddb  xmm0, xmm1                      ; Convert upper to lower case

    vmovdqu [rdi], xmm0                     ; Store

    add     rdi, 16                         ; Advance dst pointer
    add     rsi, 16                         ; Advance src pointer
    dec     ecx
    jnz     .proc16

.preproc1:
    cmp     edx, 0                          ; No residual iterations?
    je      .done

.proc1:
    mov     cl, byte [rsi]                  ; Next byte to cl

    cmp     cl, 65                          ; Is cl less than 'A'?
    jl      .proc1.skip

    cmp     cl, 90                          ; Is cl greater than 'Z'?
    jg      .proc1.skip

    add     cl, 32                          ; Convert to lower case

.proc1.skip:
    mov     byte [rdi], cl                  ; Write byte to dst string
    inc     rdi                             ; Advance dst pointer
    inc     rsi                             ; Advance src pointer

    dec     edx
    jnz     .proc1

.done:
    mov     byte [rdi], 0                   ; Null-terminate

.epi:
    vzeroupper                              ; Zero upper 128 bits of ymm registers
    ret
