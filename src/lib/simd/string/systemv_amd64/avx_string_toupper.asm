    section .rodata

    align 32
    diffs:  db 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32
    alim:   db 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96
    zlim:   db 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123,

    section .text

    global avx_string_toupper

; AVX-accelerated toupper for byte string
; Params:
;     rdi: pointer to dst buffer
;     rsi: pointer to src string
;     edx: size of dst buffer
;     ecx: length of src string
; Return:
;     eax: 0 on success, negative value on failure
avx_string_toupper:
    mov     eax, -7                         ; Presume failure for now

    cmp     edx, ecx                        ; Src string too large for dst buffer?
    jbe     .epi

    xor     eax, eax                        ; Return code 0

    vmovdqa ymm5, [rel diffs]               ; Fill ymm5 with 32
    vmovdqa ymm3, [rel alim]                ; Fill ymm3 with 96
    vmovdqa ymm4, [rel zlim]                ; Fill ymm4 with 123

    mov     edx, ecx
    shr     ecx, 5                          ; Unsigned division by 32

    jz      .preproc16

    and     rdx, 0x1F                       ; Bytes remaining after avx loop

.proc32:
    vmovdqu ymm0, [rsi]                     ; Next 32 bytes

    vmovdqa ymm1, ymm0
    vpcmpgtb    ymm1, ymm3                  ; All bits 1 if byte is greater than 96

    vmovdqa ymm2, ymm4
    vpcmpgtb    ymm2, ymm0                  ; All bits 1 if 123 is greater than byte

    vpand   ymm1, ymm2                      ; All 1s if byte greater than 96 and less than 123
    vpand   ymm1, ymm5                      ; Bytes in ymm1 are 32 iff corresponding byte in ymm0 is lower case, 0 otherwise
    vpsubb  ymm0, ymm1                      ; Convert lower to upper

    vmovdqu [rdi], ymm0                     ; Store

    add     rdi, 32                         ; Advance dst pointer
    add     rsi, 32                         ; Advance src pointer
    dec     ecx
    jnz     .proc32

.preproc16:

    mov     ecx, edx                        ; Remaining bytes to ecx
    shr     ecx, 4                          ; Unsigned division by 16

    jz      .preproc1

    and     edx, 0xF                        ; Bytes remaining after sse loop

.proc16:
    vmovdqu xmm0, [rsi]                     ; Next 16 bytes

    vmovdqa xmm1, xmm0
    vpcmpgtb    xmm1, xmm3                  ; All bits 1 if byte is greater than 96

    vmovdqa xmm2, xmm4
    vpcmpgtb    xmm2, xmm0                  ; All bits 1 if 123 is greater than byte

    vpand   xmm1, xmm2                      ; All 1s if byte greater than 96 and less than 123
    vpand   xmm1, xmm5                      ; Bytes in xmm1 are 32 iff corresponding byte in xmm0 is lower case, 0 otherwise
    vpsubb  xmm0, xmm1                      ; Convert lower to upper

    vmovdqu [rdi], xmm0                     ; Store

    add     rdi, 16                         ; Advance dst pointer
    add     rsi, 16                         ; Advance src pointer
    dec     ecx
    jnz     .proc16

.preproc1:
    cmp     edx, 0                          ; No residual iterations?
    je      .done

.proc1:
    mov     cl, byte [rsi]                  ; Next byte to cl

    cmp     cl, 97                          ; Is cl less than 'a'?
    jl      .proc1.skip

    cmp     cl, 122                         ; Is cl greater than 'z'?
    jg      .proc1.skip

    sub     cl, 32                          ; Convert to lower case

.proc1.skip:
    mov     byte [rdi], cl                  ; Write byte to dst string
    inc     rdi                             ; Advance dst pointer
    inc     rsi                             ; Advance src pointer

    dec     edx
    jnz     .proc1

.done:
    mov     byte [rdi], 0                   ; Null-terminate

.epi:
    vzeroupper                              ; Zero upper 128 bits of ymm registers
    ret
