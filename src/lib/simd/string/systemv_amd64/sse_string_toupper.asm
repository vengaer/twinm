    section .rodata

    align 16
    diffs:  db 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32
    alim:   db 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96, 96
    zlim:   db 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123

    section .text

    global sse_string_toupper

ret
; SSE-accelerated toupper for byte string
; Params:
;     rdi: pointer to dst buffer
;     rsi: pointer to src string
;     edx: size of dst buffer
;     ecx: length of src string
; Return:
;     eax: 0 on success, negative value on failure
sse_string_toupper:
    mov     eax, -7                         ; presume failure for now

    cmp     edx, ecx                        ; src string too large for dst buffer?
    jbe     .epi

    xor     eax, eax                        ; return code 0

    mov     edx, ecx
    shr     ecx, 4                          ; unsigned division by 16

    jz      .preproc1

    and     edx, 0xF                        ; bytes remaining after sse loop

    movdqa  xmm3, [rel alim]                ; fill xmm3 with 96
    movdqa  xmm4, [rel zlim]                ; fill xmm4 with 123
    movdqa  xmm5, [rel diffs]               ; fill xmm5 with 32

.proc16:
    movdqu  xmm0, [rsi]                     ; next 16 bytes

    movdqa  xmm1, xmm0
    pcmpgtb xmm1, xmm3                      ; all bits 1 if byte is greater than 96

    movdqa  xmm2, xmm4
    pcmpgtb xmm2, xmm0                      ; all bits 1 if 123 is greater than byte

    pand    xmm1, xmm2                      ; all bits 1 if byte is greater than 96 and less than 91
    pand    xmm1, xmm5                      ; bytes in xmm1 are 32 iff corresponding byte in xmm0 is lower case
    psubb   xmm0, xmm1                      ; convert to upper case

    movdqu  [rdi], xmm0                     ; store

    add     rdi, 16                         ; advance dst pointer
    add     rsi, 16                         ; advance src pointer
    dec     ecx
    jnz     .proc16

.preproc1:
    cmp     edx, 0                          ; no residual iterations?
    je      .done

.proc1:
    mov     cl, byte [rsi]                  ; next byte to cl

    cmp     cl, 97                          ; cl less than 'a'?
    jl      .proc1.skip

    cmp     cl, 122                         ; cl greater than 'z'
    jg      .proc1.skip

    sub     cl, 32                          ; convert to lower case

.proc1.skip:
    mov     byte [rdi], cl                  ; store
    inc     rdi                             ; advance dst pointer
    inc     rsi                             ; advance src pointer

    dec     edx
    jnz     .proc1

.done:
    mov     byte [rdi], 0                   ; null-terminate
    xor     eax, eax                        ; return 0

.epi:
    ret
