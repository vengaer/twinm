    section .text

    global sse_memzero

; Zero esi bytes of memory at address [rdi]
; Params:
;     rdi: address to be zeroed
;     esi: number of bytes to zero
; Return:
;     rax: address of the memory that was zeroed
;          (i.e. the first parameter)
sse_memzero:

    section .data

.align_table:
    dq  .align2b
    dq  .align4b
    dq  .align8b
    dq  .align16b

.residual_table:
    dq  .res1b
    dq  .res2b
    dq  .res4b
    dq  .res8b

    section .text

    mov     rax, rdi                        ; Return destination pointer

    cmp     esi, 0
    je      .epi

    pxor    xmm0, xmm0                      ; Use xmm0 for zeroing 128 bit blocks

    cmp     di, 0                           ; Check for alignment
    je      .align_end

    lea     rdx, [rel .align_table]         ; Load jump table

    bsf     r8w, di                         ; Index of least significant set bit in di to r8w

    cmp     r8b, 0x3                        ; Check for 16-byte alignment
    ja      .align_end

    shl     r8b, 0x3                        ; Multiply r8b by 8 for offset
    movzx   r8d, r8b                        ; Zero extend
    jmp     [rdx + r8]                      ; Jump to branch

.align2b:
    mov     byte [rdi], 0                   ; Zero byte

    inc     rdi                             ; Advance destination pointer
    dec     esi                             ; Decrement number of remaining bytes

    bsf     r8w, di                         ; Index of least significant set bit in di to r8w

    cmp     r8b, 0x3                        ; Check for 16-byte alignment
    ja      .align_end

    shl     r8b, 0x3                        ; Multiply by 8
    movzx   r8d, r8b                        ; Zero extend
    jmp     [rdx + r8]                      ; Jump to branch
.align4b:
    cmp     sil, 0x2                        ; Fewer than 2 bytes remaining?
    jb      .res_begin

    mov     word [rdi], 0                   ; Zero word

    add     rdi, 0x2                        ; Advance pointer
    sub     esi, 0x2                        ; Decrement number of remaining bytes

    bsf     r8w, di                         ; Index of least significant set bit in di to r8w

    cmp     r8b, 0x3                        ; Check for 16-byte alignment
    ja      .align_end

    shl     r8b, 0x3                        ; Multiply by 8
    movzx   r8d, r8b                        ; Zero extend
    jmp     [rdx + r8]                      ; Jump to branch
.align8b:
    cmp     sil, 0x4                        ; Fewer than 4 bytes remaining?
    jb      .res_begin

    mov     dword [rdi], 0                  ; Zero dword

    add     rdi, 0x4                        ; Advance pointer
    sub     esi, 0x4                        ; Decrement number of remaining bytes

    test    dil, 0x3                        ; Test for 16-byte alignment
    jnz     .align16b

    jmp     .align_end
.align16b:
    cmp     sil, 0x8                        ; Fewer than 8 bytes remaining?
    jb      .res_begin

    mov     qword [rdi], 0                  ; Zero qword

    add     rdi, 0x8                        ; Advance pointer
    sub     esi, 0x8                        ; Decrement number of remaining bytes
.align_end:
    cmp     esi, 0x10                       ; At least 16 bytes remaining?
    jb      .res_begin

    movdqu  [rdi], xmm0                     ; Zero 128 bits

    add     rdi, 0x10                       ; Advance pointer
    sub     esi, 0x10                       ; Decrement number of remaining bytes

    jmp     .align_end
.res_begin:
    cmp     esi, 0
    je      .epi

    lea     rdx, [rel .residual_table]      ; Load jump table

    bsr     r8w, si                         ; Most significant set bit of si to r8w

    shl     r8b, 0x3                        ; Multiply by 8
    movzx   r8d, r8b                        ; Zero extend
    jmp     [rdx + r8]                      ; Jump to branch
.res8b:
    mov     qword [rdi], 0                  ; Zero qword

    add     rdi, 0x8                        ; Advance pointer
    sub     esi, 0x8                        ; Decrement number of remaining bytes

    jz      .epi

    bsr     r8w, si                         ; Most significant set bit of si to r8w

    shl     r8b, 0x3                        ; Multiply by 8
    movzx   r8d, r8b                        ; Zero extend
    jmp     [rdx + r8]                      ; Jump to branch
.res4b:
    mov     dword [rdi], 0                  ; Zero dword

    add     rdi, 0x4                        ; Advance pointer
    sub     esi, 0x4                        ; Decrement number of remaining bytes

    jz      .epi

    bsr     r8w, si                         ; Most significant set bit of si to r8w

    shl     r8b, 0x3                        ; Multiply by 8
    movzx   r8d, r8b                        ; Zero extend
    jmp     [rdx + r8]                      ; Jump to branch
.res2b:
    mov     word [rdi], 0                   ; Zero word

    add     rdi, 0x2                        ; Advance pointer
    sub     esi, 0x2                        ; Decrement number of remaining bytes

    jz      .epi
.res1b:
    mov     byte [rdi], 0                   ; Zero byte

.epi:
    ret
