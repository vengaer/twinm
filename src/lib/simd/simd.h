#ifndef SIMD_H
#define SIMD_H

/* String functions */

#if defined TWINM_SIMD_AVX

extern int avx_string_tolower(char *dst, char const *src, unsigned dst_len, unsigned src_len);
extern int avx_string_toupper(char *dst, char const *src, unsigned dst_len, unsigned src_len);

extern void *avx_memzero(void *addr, unsigned nbytes);

#endif /* defined TWINM_SIMD_AVX */

#if defined TWINM_SIMD_SSE || (defined TWINM_SIMD_AVX && defined TWINM_TEST_BUILD)

extern int sse_string_tolower(char *dst, char const *src, unsigned dst_len, unsigned src_len);
extern int sse_string_toupper(char *dst, char const *src, unsigned dst_len, unsigned src_len);

extern void *sse_memzero(void *addr, unsigned nbytes);

#endif /* defined TWINM_SIMD_SSE || ... */

/* Math functions */

#if defined TWINM_SIMD_AVX

extern void avx_mv_mul_4(float *dst, float const *matrix, float const *vec);
extern void avx_mat_4_transpose(float *dst, float const *matrix);

#endif /* defined TWINM_SIMD_AVX */

#if defined TWINM_SIMD_SSE || (defined TWINM_SIMD_AVX && defined TWINM_TEST_BUILD)

extern void sse_mv_mul_4(float *dst, float const *matrix, float const *vec);
extern void sse_mat_4_transpose(float *dst, float const *matrix);

#endif /* defined TWINM_SIMD_SSE || ... */

#endif /* SIMD_H */
