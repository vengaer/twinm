#ifndef NFA_H
#define NFA_H

#include "capture_info.h"
#include "charcodes.h"
#include "list.h"
#include "queue.h"
#include "regex_type.h"
#include "twinm_types.h"

#include <stdbool.h>
#include <stddef.h>

/* Number of outgoing connections from an nfa state */
enum { NFA_STATE_NUM_EDGES = 2 };
/* Max number of states in an nfa */
enum { NFA_STATE_POOL_SIZE = REGEX_MAX_SIZE };

/* Max size of an epsilon closure */
enum { NFA_CLOSURE_POOL_SIZE = regex_charcode_accept - 32 + 1 };

/* Struct representing a single nfa state
 *
 * @charcode: the charcode of the state (ascii character or regex charcode)
 * @edges:    list nodes used to link states, the lists are constructed s.t.
 *            following the 0th edge never results in a loop
 * @flags:    flags and potential index of capture groups for the state.
 *            The bits are used as follows:
 *              bit 0x1:  unused
 *              bit 0x2:  if set, indicates the start of a capture
 *              bit 0x4:  if set, indicates the end of a capture
 *              bit 0x8:  if set, indicates that the capture should be opened
 *                        in the last state possible
 *              bit 0x10: if set, indicates whether the matching of the node
 *                        should be negated
 *              bit 0x20: indicates that the next match should be performed at
 *                        the current position of the input as opposed to
 *                        the next char
 * @capture:  index of the capture group
 */
struct nfa_state {
    struct list_head edges[NFA_STATE_NUM_EDGES];
    regex_char charcode;
    regex_flag_type flags;
    struct regex_capture_info capture;
};

/* Struct used for placing nfa in a queue */
struct nfa_state_node {
    struct nfa_state *state;
    struct queue_node node;
};

/* Struct representing a pool of nfa states */
struct nfa_state_pool {
    struct queue *queue;
    struct nfa_state_node *queue_node_pool;
};

/* Struct representing an nfa segment (i.e. one or more linked
 * nfa states)
 *
 * @entry:         the first state in the segment
 * @tail:          the last state in the segment
 * @capture_group: index of the capture group
 */
struct nfa_segment {
    struct nfa_state *entry;
    struct nfa_state *tail;
    regex_capture_type capture_group;
};

/* Type used to store states a list while
 * translating epsilon closures
 */
struct nfa_closure_list_node {
    struct nfa_state *state;
    struct list_head list_head;
};

/* A finite, nondeterministic automaton
 *
 * @state_pool:  pool of nfa states used in the nfa. The pool should be allocated by
 *               calling the nfa_init function
 * @start_index: index of the start state of the nfa
 * @ncaptures:   number of capture groups encountered during compilation
 * @flags:       auxiliary bitflags
 */
struct nfa {
    struct nfa_state *state_pool;
    unsigned short start_index;
    regex_capture_type ncaptures;
    regex_flag_type flags;
};

/* Initialize the nfa state pool */
bool nfa_init(struct nfa *nfa);

/* Free the nfa state pool */
void nfa_free(struct nfa *nfa);

/* Initialize empty nfa segment
 *
 * @state_pool:    pool of nfa states
 * @segment:       pointer to the segment to be initialized
 * @capture_index: index of capture group
 */
int nfa_init_segment(struct nfa_state_pool *state_pool, struct nfa_segment *segment, regex_capture_type capture_index);

/* Construct an atomic (single-node) nfa segment
 *
 * @state_pool: pool of nfa states
 * @segment:    the segment used to refer to the newly constructed state
 * @c:          the charcode to be stored in the state
 * @flags:      flags for the atomic segment
 */
int nfa_construct_atomic_segment(struct nfa_state_pool *state_pool, struct nfa_segment *segment, regex_char c, regex_flag_type flags);

/* Construct an non-atomic nfa segment. Consists of entry state whose charcode
 * is set to c and a tail whose charcode is unspecified
 *
 * @state_pool: pool of nfa states
 * @segment:    the segment used to refer to the newly constructed state
 * @c:          the charcode to be stored in the state
 * @flags:      flags for the atomic segment
 */
int nfa_construct_segment(struct nfa_state_pool *state_pool, struct nfa_segment *segment, regex_char c, regex_flag_type flags);

/* Repeat nfa segment zero or one times (?)
 *
 * @state_pool: pool of nfa states
 * @segment:    nfa_segment to be repeated
 */
int nfa_repeat_zero_one(struct nfa_state_pool *state_pool, struct nfa_segment *segment);

/* Repeat nfa segment one or more times (+)
 *
 * @state_pool: pool of nfa states
 * @segment:    nfa_segment to be repeated (state B)
 */
int nfa_repeat_one_any(struct nfa_state_pool *state_pool, struct nfa_segment *segment);

/* Repeat nfa segment zero or more times (*)
 *
 * @state_pool: pool of nfa states
 * @segment:    nfa_segment to be repeated
 */
int nfa_repeat_zero_any(struct nfa_state_pool *state_pool, struct nfa_segment *segment);

/* Repeat nfa segment n times ({n})
 *
 * @state_pool: pool of nfa states
 * @segment:    nfa segment to be repeated
 * @n:          number of times to repeat segment (inclusive)
 */
int nfa_repeat_n(struct nfa_state_pool *state_pool, struct nfa_segment *segment, unsigned n);

/* Repeat nfa segment n to m times ({n,m})
 *
 * @state_pool: pool of nfa states
 * @segment:    nfa segment to be repeated
 * @n:          lower bound of repetition (inclusive)
 * @m:          upper bound of repetition (inclusive)
 */
int nfa_repeat_n_to_m(struct nfa_state_pool *state_pool, struct nfa_segment *segment, unsigned n, unsigned m);

/* Repeat nfa segment at least n times ({n,})
 *
 * @state_pool: pool of nfa states
 * @segment:    segment to be repeated
 * @n:          lower bound of repetition (inclusive)
 */
int nfa_repeat_n_to_any(struct nfa_state_pool *state_pool, struct nfa_segment *segment, unsigned n);

/* Pass through one of two possible nfa states (|)
 *
 * @segment0: the segment used as the first path. The nfa_segment is updated s.t. it,
 *            when the function returns, refers to the entire alternation
 * @segment1: the segment used as the second path
 */
int nfa_alternate(struct nfa_segment *segment0, struct nfa_segment const *segment1);

/* Append nfa state
 *
 * @state_pool: pool of nfa states
 * @tail:       the tail of the nfa_segment being constructed (state A)
 * @segment:    nfa_segment to be inserted (in place of state B)
 */
int nfa_concatenate(struct nfa_state_pool *state_pool, struct nfa_state **state, struct nfa_segment *segment);

/* Make the tail state an accepting state */
void nfa_accept_state(struct nfa_state *tail);

int nfa_epsilon_closure(struct list_head *closure_list, struct nfa_state *entry,
                        struct nfa_closure_list_node *list_node_pool, unsigned list_pool_size,
                        regex_flag_type *flags, struct regex_capture_info *capture);

bool nfa_closure_contains_accept_state(struct list_head const *closure_list);

/* Get the nfa_state referred to by edge */
static inline struct nfa_state *nfa_follow_edge(struct list_head const *edge) {
    return container(edge->tail, struct nfa_state, edges);
}

/* Check if given nfa segment is atomic (i.e. consists of a single nfa_state) */
static inline int nfa_segment_is_atomic(struct nfa_segment const *segment) {
    return  segment->entry == segment->tail;
}

#ifdef TWINM_TEST_BUILD
/* Convert nfa to regex
 *
 * @dst: the destination string
 * @nfa: the nfa to convert
 * @n:   size of the destination string
 */
ssize_t nfa_to_regex(char *dst, struct nfa *nfa, size_t n);
#endif /* ifdef TWINM_TEST_BUILD */

#endif /* NFA_H */
