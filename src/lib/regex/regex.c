#include "dfa.h"
#include "nfa.h"
#include "parser.h"
#include "regex.h"


int regex_compile(regex_handle *restrict handle, char const *regex, regex_flag_type flags) {
    int status = 0;

    struct nfa nfa;

    if(!nfa_init(&nfa)) {
        return -EHEAP_ALLOC;
    }

    status = regex_to_nfa(&nfa, regex, flags);

    if(status < 0) {
        nfa_free(&nfa);
        return status;
    }

    if(!dfa_init(handle)) {
        nfa_free(&nfa);
        return -EHEAP_ALLOC;
    }

    status = nfa_to_dfa(handle, &nfa);

    nfa_free(&nfa);

    if(status < 0) {
        dfa_free(handle);
    }

    return status;
}

int regex_match(regex_handle const *restrict handle, char const *input, regex_capture *captures, unsigned ncaptures) {
    return dfa_simulate(handle, input, captures, ncaptures);
}

void regex_free(regex_handle *handle) {
    dfa_free(handle);
}
