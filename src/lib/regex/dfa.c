#include "bitop.h"
#include "dfa.h"
#include "list.h"
#include "macro.h"
#include "nfa.h"
#include "queue.h"
#include "rbtree.h"
#include "strutils.h"

#include <assert.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

enum { DFA_CAPTURE_POOL_SIZE = DFA_EDGE_POOL_SIZE / 4 };

/* Entry used when translating an nfa to a dfa
 *
 * Maps an nfa epsilon closure to a single dfa state
 *
 * @rbnode:            used to place the entry in an rbtree
 * @nfa_state_indices: pool indices of the nfa states
 * @dfa_state_index:   pool index of the dfa state
 * @nnfa_states:       number of states in the nfa epsilon closure
 */
struct transition_table_entry {
    struct rbnode rbnode;
    unsigned short *nfa_state_indices;
    int dfa_state_index;
    unsigned short nnfa_states;
};

/* State used when simulating the dfa
 *
 * @state:       current dfa state
 * @string:      pointer to (sub)string for which the
 *               simulation is run
 * @queue_node:  used to place the state in a queue
 */
struct dfa_simulation_state {
    struct dfa_state *state;
    char const *string;
    struct queue_node queue_node;
};

/* Index used for placing dfa_simulation_states in a queue
 *
 * @index:      index of the state in the queue node pool
 * @queue_node: used to place the index itself in a separate queue
 */
struct dfa_simulation_index {
    unsigned short index;
    struct queue_node queue_node;
};

struct dfa_capture_stats {
    unsigned short nclose;
    regex_flag_type flags;
};

/* Get simulation index from queue node */
static inline unsigned short dfa_dequeue_simulation_index(struct queue *queue) {
    return container(queue_dequeue(queue), struct dfa_simulation_index, queue_node)->index;
}

/* Get the simulation state at the head of the queue */
static inline struct dfa_simulation_state *dfa_dequeue_simulation_state(struct queue *queue) {
    return container(queue_dequeue(queue), struct dfa_simulation_state, queue_node);
}

/* Compare arrays lexicographically */
static inline int dfa_lexicographical_comparison(unsigned short const *a0, unsigned short const *a1, size_t size) {
    int result = 0;

    for(unsigned i = 0; !result && i < size; i++) {
        result = a1[i] - a0[i];
    }

    return result;
}

/* Compare transition table entries */
static int dfa_compare_transition_table_entries(struct rbnode const *n0, struct rbnode const *n1) {
    struct transition_table_entry const *e0 = container(n0, struct transition_table_entry, rbnode, const);
    struct transition_table_entry const *e1 = container(n1, struct transition_table_entry, rbnode, const);

    int result = e1->nnfa_states - e0->nnfa_states;

    return result || dfa_lexicographical_comparison(e0->nfa_state_indices, e1->nfa_state_indices, e0->nnfa_states);
}

/* Fill an entry of the transition table
 *
 * @entry:           the entry to be filled out
 * @closure_list:    list of the nodes in the current epsilon closure
 * @closure_size:    size of the epsilon closure
 * @nfa:             the nfa being translated from
 * @dfa_state_index: index of the dfa state corresponding to the epsilon
 *                   closure
 */
static bool dfa_fill_transition_table_entry(struct transition_table_entry *entry,
                                            struct list_head const *closure_list,
                                            unsigned short closure_size,
                                            struct nfa const *nfa,
                                            int dfa_state_index)
{
    entry->nfa_state_indices = realloc(entry->nfa_state_indices, closure_size * sizeof(*entry->nfa_state_indices));

    /* Allocation failure */
    if(!entry->nfa_state_indices) {
        return false;
    }

    struct nfa_state *closure_state;

    struct list_head *iter;
    unsigned index = 0;

    /* Add nfa states */
    list_for_each(iter, closure_list) {
        closure_state = container(iter, struct nfa_closure_list_node, list_head)->state;
        entry->nfa_state_indices[index++] = array_index(nfa->state_pool, closure_state);
    }

    entry->nnfa_states = closure_size;
    entry->dfa_state_index = dfa_state_index;

    return true;
}

/* Free the entries of the transition table */
static inline void dfa_free_transition_table_entries(struct transition_table_entry *entries, unsigned size) {
    for(unsigned i = 0; i < size && entries[i].nnfa_states > 0; i++) {
        free(entries[i].nfa_state_indices);
    }
}

/* Check whether there are dfa states remaining */
static inline bool dfa_state_pool_exceeded(struct dfa const *dfa) {
    return dfa->nstates >= DFA_STATE_POOL_SIZE;
}

/* Check whether there are dfa edges remaining */
static inline bool dfa_edge_pool_exceeded(struct dfa const *dfa) {
    return dfa->nedges >= DFA_EDGE_POOL_SIZE;
}

/* Get next dfa state from the state pool */
static inline struct dfa_state *dfa_pool_next_state(struct dfa *dfa) {
    list_clear(&dfa->state_pool[dfa->nstates].edges);
    return &dfa->state_pool[dfa->nstates++];
}

/* Get next dfa edge from the edge pool */
static inline struct dfa_edge *dfa_pool_next_edge(struct dfa *dfa) {
    list_clear(&dfa->edge_pool[dfa->nedges].list_head);
    dfa->edge_pool[dfa->nedges].flags = 0;
    return &dfa->edge_pool[dfa->nedges++];
}

/* Match any character (.) */
static inline bool dfa_match_any(char c) {
    return c != '\n';
}

/* Match any digit */
static inline bool dfa_match_digit(char c) {
    return c >= '0' && c <= '9';
}

/* Match any lower case char */
static inline bool dfa_match_lower_case_char(char c) {
    return c >= 'a' && c <= 'z';
}

/* Match any upper case char */
static inline bool dfa_match_upper_case_char(char c) {
    return c >= 'A' && c <= 'Z';
}

/* Match [A-Za-z0-9_] */
static inline bool dfa_match_alnum(char c) {
    return dfa_match_digit(c) ||
           dfa_match_lower_case_char(c) ||
           dfa_match_upper_case_char(c) ||
           c == '_';
}

/* Match any whitespace char */
static inline bool dfa_match_whitespace(char c) {
    return c == ' '  ||
           c == '\n' ||
           c == '\r' ||
           c == '\t' ||
           c == '\f';
}

/* Check whether a dfa edges matches the char c */
static inline bool dfa_edge_match(struct dfa_edge const *edge, char c) {
    bool match;

    switch(edge->charcode) {
        case regex_charcode_match_any:
            match = dfa_match_any(c);
            break;
        case regex_charcode_digit:
            match = dfa_match_digit(c);
            break;
        case regex_charcode_alnum:
            match = dfa_match_alnum(c);
            break;
        case regex_charcode_whitespace:
            match = dfa_match_whitespace(c);
            break;
        case regex_charcode_lower_case:
            match = dfa_match_lower_case_char(c);
            break;
        case regex_charcode_upper_case:
            match = dfa_match_upper_case_char(c);
            break;
        default:
            match = (edge->charcode == (regex_char)c);
            break;
    }

    return regex_flag_test(edge->flags, REGEX_FLAG_NEGATE) ^ match;
}

/* Initialize the dfa
 *
 * - allocate state pool
 * - allocate edge pool
 * - set indices
 * - clear flags
 */
bool dfa_init(struct dfa *dfa) {
    dfa->state_pool = calloc(DFA_STATE_POOL_SIZE, sizeof(*dfa->state_pool));
    dfa->edge_pool = calloc(DFA_EDGE_POOL_SIZE, sizeof(*dfa->edge_pool));

    dfa->nstates = 0;
    dfa->nedges = 0;

    dfa->flags = 0;

    return dfa->state_pool;
}

/* Free state and edge pools */
void dfa_free(struct dfa *dfa) {
    free(dfa->state_pool);
    free(dfa->edge_pool);
}

/* Check whether given state is an accept state
 *
 * @state:  current dfa state
 * @flags:  dfa flags
 * @string: position in the input string
 */
static inline bool dfa_state_accept(struct dfa_state const *restrict state, regex_flag_type flags, char const *restrict string) {
    return regex_flag_test(state->flags, REGEX_FLAG_ACCEPT) & nand(regex_flag_test(flags, REGEX_FLAG_ANCHOR_END), !!*string);
}

static inline bool dfa_start_capture(struct dfa_state const *restrict state) {
    return regex_flag_test(state->flags, REGEX_FLAG_CAPTURE_START);
}

static inline bool dfa_end_capture(struct dfa_state const *state) {
    return regex_flag_test(state->flags, REGEX_FLAG_CAPTURE_END);
}

static bool dfa_capture_start_overridable(regex_flag_type flags, struct dfa_capture const *capture,  regex_flag_type *restrict capture_flags) {
    *capture_flags |= REGEX_FLAG_LATE_CAPTURE_START * regex_flag_test(flags, REGEX_FLAG_LATE_CAPTURE_START);
    return (!capture->start) | regex_flag_test(*capture_flags, REGEX_FLAG_LATE_CAPTURE_START);
}

static inline bool dfa_capture_end_overridable(bool capture_greedy, unsigned short *restrict nclose, struct dfa_capture const *capture) {
    /* Override if capture is greedy, if the capture has not yet been closed or both */
    return nand(!capture_greedy, !((*nclose)++ < 1 + (capture->start == capture->end)));
}

/* Open captures as indicated by the bits in capture. If the capture is already open, skip it
 *
 * @captures:  pointer to array for storing the captures
 * @ncaptures: size of capture array
 * @cstate:          the current simulation state
 */
static void dfa_open_captures(struct dfa_capture *captures, unsigned ncaptures, struct dfa_simulation_state *cstate, struct dfa_capture_stats *capture_stats) {
    regex_capture_type mask = 0x1;
    regex_capture_type groups = cstate->state->capture.groups;
    unsigned bits = sizeof(groups) * CHAR_BIT;

    ncaptures = min(ncaptures, bits);

    for(unsigned i = 1; i < ncaptures; i++) {
        if((groups & mask) && dfa_capture_start_overridable(cstate->state->flags, &captures[i], &capture_stats[i].flags)) {
            captures[i].start = cstate->string;
        }
        groups >>= 1;
    }
}

/* Close captures as indicated by the bits in capture
 *
 * @captures:        pointer to array for storing the captures
 * @ncaptures:       size of capture array
 * @cstate:          the current simulation state
 * @capture_stats:    pointer to array of dfa_capture_statss used for
 *                   determining whether captures are modifiable
 */
static void dfa_close_captures(struct dfa_capture *captures, unsigned ncaptures, struct dfa_simulation_state *cstate, struct dfa_capture_stats *capture_stats) {
    regex_capture_type mask = 0x1;
    regex_capture_type capture = cstate->state->capture.groups;
    regex_capture_type greedy = cstate->state->capture.greedy;

    unsigned bits = sizeof(capture) * CHAR_BIT;
    ncaptures = min(ncaptures, bits);

    for(unsigned i = 1; i < ncaptures; i++) {
        if((capture & mask) && dfa_capture_end_overridable(greedy & mask, &capture_stats[i].nclose, &captures[i])) {
            captures[i].end = cstate->string;
        }

        capture >>= 1;
        greedy >>= 1;
    }
}

static char const *dfa_attempt_advance(regex_flag_type flags, char const *pos) {
    return pos + !regex_flag_test(flags, REGEX_FLAG_HALT_ADVANCE);
}

/* Simulate the DFA
 *
 * Nondeterministic simulation required despite the DFA being deterministic by definition
 * in order to support the . metacharacter
 *
 * @dfa:       the DFA
 * @input:     the DFA input
 * @captures:  array in which to store captures
 * @ncaptures: size of capture array
 */
int dfa_simulate(struct dfa const *restrict dfa, char const *input, struct dfa_capture *captures, unsigned ncaptures) {
    static_assert(SAFECMP(unsigned, DFA_ALTERNATIVE_SIMULATION_PATHS, <=, QUEUE_STATIC_SIZE), "Too many simulation paths for queue");

    char lcbuffer[REGEX_MAX_SIZE];
    char const *siminput = input;

    if(regex_flag_test(dfa->flags, REGEX_FLAG_IGNORE_CASE)) {
        int status = strlower(lcbuffer, input, sizeof(lcbuffer));

        if(status < 0) {
            return -E2BIG;
        }

        siminput = lcbuffer;
    }

    /* Auxiliary info for the ncaptures captures */
    struct dfa_capture_stats capture_stats[REGEX_MAX_CAPTURE_GROUPS];
    memzero(capture_stats, sizeof(capture_stats[0]) * ncaptures);

    int action = DFA_SIMULATION_ACTION_REJECT;

    /* queue of indices for simulation state pool */
    queue_init(index_queue);

    struct dfa_simulation_index simulation_indices[DFA_ALTERNATIVE_SIMULATION_PATHS];

    for(unsigned i = 0; i < array_size(simulation_indices); i++) {
        simulation_indices[i].index = i;
        queue_enqueue(&index_queue, &simulation_indices[i].queue_node);
    }

    /* queue storing alternative simulation paths, required for . special char */
    queue_init(path_queue);

    struct dfa_simulation_state simulation_states[DFA_ALTERNATIVE_SIMULATION_PATHS];
    unsigned short simulation_index = dfa_dequeue_simulation_index(&index_queue);

    struct dfa_edge *edge;
    struct list_head *iter;

    struct dfa_simulation_state *cstate;
    struct dfa_simulation_state *nstate;

    /* Clear potential captures */
    memzero(captures, ncaptures * sizeof(*captures));

    char const *end_pos;
    {
        regex_flag_type anchor_start = regex_flag_test(dfa->flags, REGEX_FLAG_ANCHOR_START);
        end_pos = siminput + anchor_start + !anchor_start * strlen(siminput);
    }

    /* Start simulation in each char of input unless the REGEX_FLAG_ANCHOR_START bit is set */
    for(char const *pos = siminput; pos < end_pos; ++pos) {

        /* Simulation start state */
        cstate = &simulation_states[simulation_index];
        cstate->state = &dfa->state_pool[0];
        cstate->string = pos;

        queue_enqueue(&path_queue, &cstate->queue_node);

        while(queue_size(&path_queue)) {
            /* Current simulation state */
            cstate = dfa_dequeue_simulation_state(&path_queue);

            if(dfa_state_accept(cstate->state, dfa->flags, cstate->string)) {
                action = DFA_SIMULATION_ACTION_ACCEPT;

                if(ncaptures) {
                    /* Check for end of last capture group */
                    if(dfa_end_capture(cstate->state)) {
                        dfa_close_captures(captures, ncaptures, cstate, capture_stats);
                    }
                    /* Capture group 0 */
                    captures[0].start = siminput;
                    captures[0].end = siminput + strlen(siminput);
                }
            }

            /* No match for path */
            if(!*cstate->string) {
                continue;
            }

            /* Attempt to traverse each edge starting in cstate->state */
            list_for_each(iter, &cstate->state->edges) {
                edge = container(iter, struct dfa_edge, list_head);

                if(dfa_edge_match(edge, *cstate->string)) {
                    if(dfa_start_capture(cstate->state)) {
                        dfa_open_captures(captures, ncaptures, cstate, capture_stats);
                    }

                    if(dfa_end_capture(cstate->state)) {
                        dfa_close_captures(captures, ncaptures, cstate, capture_stats);
                    }

                    /* Potential next state */
                    simulation_index = dfa_dequeue_simulation_index(&index_queue);
                    nstate = &simulation_states[simulation_index];
                    nstate->state = &dfa->state_pool[edge->end_state_index];
                    nstate->string = dfa_attempt_advance(edge->flags, cstate->string);

                    /* Add next state to queue */
                    queue_enqueue(&path_queue, &nstate->queue_node);
                }
            }

            /* Place index back into index pool */
            queue_enqueue(&index_queue, &simulation_indices[array_index(simulation_states, cstate)].queue_node);
        }

        /* Match, no need to keep checking */
        if(action == DFA_SIMULATION_ACTION_ACCEPT) {
            break;
        }
    }

    return regex_flag_test(dfa->flags, REGEX_FLAG_INVERT_MATCH) ^ action;
}

/* Convert nfa to dfa */
int nfa_to_dfa(struct dfa *dfa, struct nfa const *nfa) {
    static_assert(SAFECMP(unsigned, DFA_STATE_POOL_SIZE, <=, QUEUE_STATIC_SIZE), "DFA state pool too large for queue");

    int status = 0;

    struct transition_queue_node {
        struct nfa_state *nfa_state;
        struct dfa_state *dfa_prior;
        struct queue_node queue_node;
        regex_char charcode;
        regex_flag_type flags;
    };

    struct transition_queue_node queue_pool[NFA_STATE_POOL_SIZE];
    unsigned queue_index = 0;

    struct transition_table_entry transition_pool[DFA_STATE_POOL_SIZE];
    memzero(&transition_pool[0], sizeof(transition_pool));
    unsigned transition_index = 0;

    struct nfa_closure_list_node closure_pool[NFA_CLOSURE_POOL_SIZE];

    dfa->flags = nfa->flags;

    /* Transition table */
    rbtree_init(ttable);

    queue_init(queue);

    int closure_size;
    list_init(closure_list);

    /* NFA start state */
    struct nfa_state *nfa_state = &nfa->state_pool[nfa->start_index];

    regex_flag_type state_flags;
    struct regex_capture_info state_capture;

    closure_size = nfa_epsilon_closure(&closure_list, nfa_state, closure_pool, array_size(closure_pool), &state_flags, &state_capture);

    if(closure_size <= 0) {
        return -E2BIG;
    }

    struct dfa_state *dfa_start = dfa_pool_next_state(dfa);

    struct transition_table_entry *ttentry = &transition_pool[transition_index++];

    if(!dfa_fill_transition_table_entry(ttentry, &closure_list, closure_size, nfa, array_index(dfa->state_pool, dfa_start))) {
        return -EHEAP_ALLOC;
    }

    dfa_start->flags = state_flags;
    dfa_start->capture = state_capture;
    regex_flag_set_accept(&dfa_start->flags, nfa_closure_contains_accept_state(&closure_list));

    rbtree_insert(&ttable, &ttentry->rbnode, dfa_compare_transition_table_entries);

    struct list_head *iter;
    struct transition_queue_node *tqnode;

    list_for_each(iter, &closure_list) {
        nfa_state = container(iter, struct nfa_closure_list_node, list_head)->state;

        tqnode = &queue_pool[queue_index++];
        /* Successor of nfa_state */
        tqnode->nfa_state = nfa_follow_edge(&nfa_state->edges[0]);
        /* Current dfa state, charcode and flags for the edge */
        tqnode->dfa_prior = dfa_start;
        tqnode->charcode = nfa_state->charcode;
        tqnode->flags = nfa_state->flags;
        queue_enqueue(&queue, &tqnode->queue_node);
    }

    struct dfa_state *dfa_end;
    struct dfa_edge *edge;
    struct rbnode *rbiter;

    while(queue_size(&queue)) {
        if(dfa_edge_pool_exceeded(dfa)) {
            status = -E2BIG;
            goto cleanup;
        }

        tqnode = container(queue_dequeue(&queue), struct transition_queue_node, queue_node);
        dfa_start = tqnode->dfa_prior;
        nfa_state = tqnode->nfa_state;

        /* Edge to add */
        edge = dfa_pool_next_edge(dfa);
        edge->charcode = tqnode->charcode;
        edge->flags = (tqnode->flags & DFA_EDGE_MASK);

        closure_size = nfa_epsilon_closure(&closure_list, nfa_state, closure_pool, array_size(closure_pool), &state_flags, &state_capture);

        if(closure_size <= 0) {
            status = -E2BIG;
            goto cleanup;
        }

        ttentry = &transition_pool[transition_index++];

        if(!dfa_fill_transition_table_entry(ttentry, &closure_list, closure_size, nfa, -1)) {
            /* Failed to malloc for this entry, no need to free it */
            --transition_index;
            status = -EHEAP_ALLOC;
            goto cleanup;
        }

        rbiter = rbtree_find(&ttable, &ttentry->rbnode, dfa_compare_transition_table_entries);

        if(rbiter) {
            /* Entry exists in transition table, no need to keep this one */
            --transition_index;
            ttentry = container(rbiter, struct transition_table_entry, rbnode);

            /* Use end state from transition table */
            dfa_end = &dfa->state_pool[ttentry->dfa_state_index];
        }
        else {
            /* New entry in transition table */
            if(dfa_state_pool_exceeded(dfa)) {
                status = -E2BIG;
                goto cleanup;
            }

            /* New end state */
            dfa_end = dfa_pool_next_state(dfa);
            dfa_end->capture = state_capture;
            dfa_end->flags = state_flags;
            regex_flag_set_accept(&dfa_end->flags, nfa_closure_contains_accept_state(&closure_list));

            ttentry->dfa_state_index = array_index(dfa->state_pool, dfa_end);

            rbtree_insert(&ttable, &ttentry->rbnode, dfa_compare_transition_table_entries);

            /* Add successors of node in the epsilon closure */
            list_for_each(iter, &closure_list) {
                nfa_state = container(iter, struct nfa_closure_list_node, list_head)->state;

                if(nfa_state->charcode == regex_charcode_accept) {
                    /* Outdegree 0 for NFA accept states */
                    continue;
                }

                tqnode = &queue_pool[queue_index++];
                tqnode->nfa_state = nfa_follow_edge(&nfa_state->edges[0]);
                tqnode->dfa_prior = dfa_end;
                tqnode->charcode = nfa_state->charcode;
                tqnode->flags = nfa_state->flags;
                queue_enqueue(&queue, &tqnode->queue_node);
            }
        }

        /* Link states */
        edge->end_state_index = array_index(dfa->state_pool, dfa_end);
        list_push_back(&dfa_start->edges, &edge->list_head);
    }

cleanup:
    dfa_free_transition_table_entries(transition_pool, array_size(transition_pool));

    return status;
}
