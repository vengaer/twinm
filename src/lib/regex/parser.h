#ifndef PARSER_H
#define PARSER_H

#include "nfa.h"
#include "regex_type.h"

int regex_to_nfa(struct nfa *nfa, char const *regex, regex_flag_type flags);

#endif /* PARSER_H */
