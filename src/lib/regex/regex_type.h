#ifndef REGEX_TYPE_H
#define REGEX_TYPE_H

#define REGEX_FLAG_NONE 0u
#define REGEX_FLAG_IGNORE_CASE 0x4u
#define REGEX_FLAG_INVERT_MATCH 0x8u
#define REGEX_COMPILATION_MASK 0xCu /* 0x4 | 0x8 */

/* Maximum allowed size of a regex */
enum { REGEX_MAX_SIZE = 128 };

typedef unsigned char regex_flag_type;
typedef unsigned short regex_capture_type;

/* Defined in dfa.h */
struct dfa_state;
struct dfa_edge;

/* A finite, deterministic (in essence) automaton
 *
 * @state_pool: pool of dfa states
 * @edge_pool:  pool of dfa edges
 * @nstates:    number of states currently
 *              in the dfa
 * @nedges:     number of edges currently
 *              in the dfa
 * @flags:      bitflags used as follows:
 *                bit 0x1: if set, indicates that the match should be
 *                         anchored to the start of the string (^⁾
 *                bit 0x2: if set, indicates that the match should be
 *                         anchored to the end of the string ($)
 *                bit 0x4: if set, indicates that the match should be
 *                         case insensitive
 *                bit 0x8: if set, indicates that the match should be
 *                         inverted
 */
struct dfa {
    struct dfa_state *state_pool;
    struct dfa_edge *edge_pool;
    unsigned short nstates;
    unsigned short nedges;
    regex_flag_type flags;
};

/* Structure representing a capture group
 *
 * @start: pointer to the first char in the capture
 * @end:   pointer to the char directly AFTER the
 *         last char in the capture
 */
struct dfa_capture {
    char const *start;
    char const *end;
};

#endif /* REGEX_TYPE_H */
