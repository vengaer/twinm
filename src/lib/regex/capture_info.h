#ifndef CAPTURE_INFO_H
#define CAPTURE_INFO_H

#include "regex_type.h"

#include <stdbool.h>

struct regex_capture_info {
    regex_capture_type groups;
    regex_capture_type greedy;
};

static inline void regex_capture_set_group(struct regex_capture_info *info, regex_capture_type group) {
    regex_capture_type bit = (1 << group);
    info->groups |= bit;
    info->greedy |= bit;
}

#endif /* CAPTURE_INFO_H */
