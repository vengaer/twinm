#ifndef REGEX_H
#define REGEX_H

#include "macro.h"
#include "regex_type.h"
#include "twinm_types.h"

typedef struct dfa regex_handle;
typedef struct dfa_capture regex_capture;

int regex_compile(regex_handle *restrict handle, char const *regex, regex_flag_type flags);
int regex_match(regex_handle const *restrict handle, char const *input, regex_capture *captures, unsigned ncaptures);
void regex_free(regex_handle *handle);

#define regex_guard_overload_4(handle, regex, flags, status)    \
    guard(regex_compile(handle, regex, flags), regex_free(handle), status, -E2BIG)

#define regex_guard_overload_3(handle, regex, flags)            \
    regex_guard_overload_4(handle, regex, flags, 0)

/* Automatically compile and free regex at start and end of scope, respectively.
 *
 * regex_handle handle;
 * int status;
 * extern char const *input;
 *
 * vec_guard(&handle, "abc{3}d", 0, &status) {
 *
 *     if(regex_match(&handle, input, 0, 0) == 0) {
 *         // ...
 *     }
 *
 * } // handle freed
 *
 * @handle: pointer to regex handle
 * @regex:  the regular expression
 * @flags:  compilation flags for the regex
 * @status: (optional) pointer to int, set to non-zero if
 *          regex compilation fails
 */
#define regex_guard(...)    \
    overload(regex_guard_overload_, __VA_ARGS__)


#endif /* REGEX_H */
