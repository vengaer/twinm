#ifndef DFA_H
#define DFA_H

#include "capture_info.h"
#include "list.h"
#include "regex_type.h"
#include "twinm_types.h"

#include <stdbool.h>

struct nfa;

/* Max number of states in a dfa */
enum { DFA_STATE_POOL_SIZE = REGEX_MAX_SIZE };
/* Max number of edges in a dfa */
enum { DFA_EDGE_POOL_SIZE = REGEX_MAX_SIZE };

/* Max number of alternative paths to
 * simulate at one time
 * */
enum { DFA_ALTERNATIVE_SIMULATION_PATHS = 32 };

enum {
    DFA_SIMULATION_ACTION_ACCEPT = 0,
    DFA_SIMULATION_ACTION_REJECT = 1,
};

/* A dfa state
 *
 * @edges:   outgoing edges
 * @flags:   bitflags used as follows:
 *             bit 0x1: if set, indicates whether the state is an accept state
 *             bit 0x2: if set, indicates start of a capture
 *             bit 0x4: if set, indicates end of a capture
 *             bit 0x8: if set, indicates that the capture should be opened
 *                      in the last state possible
 * @capture: the capture index, indicated by each bit. In other words, a value
 *             1 << n indicates capture group n + 1
 */
struct dfa_state {
    struct list_head edges;
    struct regex_capture_info capture;
    regex_flag_type flags;
};

/* A dfa edge
 *
 * @list_head:       list head used to place the edge in the edge
 *                   list of a state
 * @end_state_index: pool index of the state at the end of
 *                   the edge
 * @charcode:        charcode of the edge
 * @flags:           bitflags used as follows:
                       bits 0x1, 0x2, 0x4, 0x8, 0x10 and 0x20 unused
 *                     bit 0x10: if set, indicates whether the matching of the node should be negated
 *                     bit 0x20: if set, indicates that the next match should be performed at the current
 *                               position of the input as opposed to the next char
 */
struct dfa_edge {
    struct list_head list_head;
    unsigned short end_state_index;
    regex_char charcode;
    regex_flag_type flags;
};

/* Initialize the dfa */
bool dfa_init(struct dfa *dfa);
/* Free the dfa state and edge pools */
void dfa_free(struct dfa *dfa);

/* Simulate the dfa
 *
 * @dfa:       the dfa
 * @input:     the input of the dfa
 * @captures:  pointer to array of dfa captures
 * @ncaptures: number of captures
 */
int dfa_simulate(struct dfa const *restrict dfa, char const *input, struct dfa_capture *captures, unsigned ncaptures);

/* Convert nfa to dfa
 *
 * @dfa: the output dfa
 * @nfa: the input nfa
 */
int nfa_to_dfa(struct dfa *dfa, struct nfa const *nfa);

#endif /* DFA_H */
