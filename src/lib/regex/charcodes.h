#ifndef CHARCODES_H
#define CHARCODES_H

#include "macro.h"
#include "regex_type.h"

#include <limits.h>
#include <stdbool.h>

/* Flags used for the regex_handle */
#define REGEX_FLAG_ANCHOR_START 0x1u
#define REGEX_FLAG_ANCHOR_END 0x2u

#define REGEX_FLAG_ACCEPT 0x1u
/* Flags used for nfa and dfa states */
#define REGEX_FLAG_CAPTURE_START 0x2u
#define REGEX_FLAG_CAPTURE_END 0x4u
#define REGEX_FLAG_LATE_CAPTURE_START 0x8u

/* Flag used for dfa edges and nfa states */
#define REGEX_FLAG_NEGATE 0x10u
#define REGEX_FLAG_HALT_ADVANCE 0x20u

/* Used for picking flags to keep during concatenation of nfa segment */
#define NFA_TAIL_MASK 0x1Cu /* bits 0x4, 0x8 and 0x10 */

/* Used for picking flags to be stored in dfa states */
#define DFA_STATE_MASK 0xFu /* Lower nibble, 0x10 and 0x20 */

/* Used for picking flags to be stored in dfa edges */
#define DFA_EDGE_MASK 0x30u /* 0x40 and 0x80 */

/* Max number of supported capture groups */
#define REGEX_MAX_CAPTURE_GROUPS CHAR_BIT * sizeof(regex_capture_type)

#define REGEX_CAPTURE_NONE 0u

enum regex_charcode {
    regex_charcode_base = 127,
    regex_charcode_alt,
    regex_charcode_repeat_zero_one,
    regex_charcode_repeat_one_any,
    regex_charcode_repeat_zero_any,
    regex_charcode_match_any,
    regex_charcode_split,
    regex_charcode_digit,
    regex_charcode_alnum,
    regex_charcode_whitespace,
    regex_charcode_lower_case,
    regex_charcode_upper_case,
    regex_charcode_accept
};

enum regex_bit_status {
    regex_bit_status_unset,
    regex_bit_status_set
};


static inline bool regex_is_special_char(char c) {
    switch(c) {
        case '*':
        case '+':
        case '?':
        case '|':
        case '.':
        case '(':
        case ')':
        case '\\':
        case '^':
        case '$':
        case '{':
        case '}':
        case '[':
        case ']':
            return true;
        default:
            /* NOP */
            break;
    }
    return false;
}

static inline bool regex_is_escape_metacharacter(char c) {
    switch(c) {
        case 'd':
        case 'w':
        case 's':
        case 'D':
        case 'W':
        case 'S':
            return true;
        default:
            /* NOP */
            break;
    }
    return false;
}

static inline bool regex_is_escapable_in_char_class(char c) {
    switch(c) {
        case ']':
        case '^':
        case '-':
        case '\\':
            return true;
        default:
            /* NOP */
            break;
    }
    return false;
}

static inline void regex_flag_set_anchor_start(regex_flag_type *flags) {
    *flags |= REGEX_FLAG_ANCHOR_START;
}

static inline void regex_flag_set_anchor_end(regex_flag_type *flags) {
    *flags |= REGEX_FLAG_ANCHOR_END;
}

static inline void regex_flag_set_negate(regex_flag_type *flags) {
    *flags |= REGEX_FLAG_NEGATE;
}

static inline void regex_flag_set_capture_start(regex_flag_type *flags) {
    *flags |= REGEX_FLAG_CAPTURE_START;
}

static inline void regex_flag_set_capture_end(regex_flag_type *flags) {
    *flags |= REGEX_FLAG_CAPTURE_END;
}

static inline void regex_flag_set_late_capture_start(regex_flag_type *flags) {
    *flags |= REGEX_FLAG_LATE_CAPTURE_START;
}

static inline void regex_flag_set_accept(regex_flag_type *flags, bool accept) {
    *flags |= (accept * REGEX_FLAG_ACCEPT);
}

static inline enum regex_bit_status regex_flag_test(regex_flag_type flags, regex_flag_type bits) {
    return (enum regex_bit_status) ((flags & bits) == bits);
}

#endif /* CHARCODES_H */
