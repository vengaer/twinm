#include "charcodes.h"
#include "macro.h"
#include "parser.h"
#include "queue.h"
#include "stack.h"
#include "strutils.h"
#include "twinm_types.h"

#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

/* Max number of nfa segments allowed */
enum { PARSER_STACK_SIZE = NFA_STATE_POOL_SIZE };
/* Max number of nested parentheses allowed in a regex */
enum { PARSER_MAX_NESTED_PARENS = 32 };
/* Max number of digits in a quantifier ({a,b})*/
enum { PARSER_QUANTIFIER_MAX_LENGTH = 16 };
/* Max number of states generated from a char class */
enum { PARSER_MAX_CHAR_CLASS_SIZE = regex_charcode_accept - 32 + 1 };
/* Number of unprintable chars at the start of the ascii table */
enum { PARSER_CHAR_CLASS_MEMBER_SHIFT = 32 };
/* Size of the ascii alphabet, either lower or upper case */
enum { PARSER_ASCII_ALPHABET_SIZE = 'Z' - 'A' + 1 };
/* Number of ascii digits */
enum { PARSER_ASCII_DIGITS = '9' - '0' + 1 };

enum parser_stack_nest_type {
    parser_stack_nest_type_none,
    parser_stack_nest_type_parenthesis,
    parser_stack_nest_type_alternation
};

/* Struct representing a char class range (e.g. a-z)
 *
 * @start: start char (i.e. a in a-z)
 * @end:   end char (i.e. (z in a-z)
 * @flags: flags to set for each state
 */
struct char_class_range {
    char start;
    char end;
    regex_flag_type flags;
};

/* Struct used to place nfa segments on the segment stack.
 *
 * The top of the segment stack holds the nfa_segment currently
 * being constructed. The bottommost slot of the segment stack represents the
 * entire nfa. New subsegments are pushed whenever an opening parenthesis
 * is encountered. When encountering a closing parenthesis, the level of the
 * parser stack is poped. This is followed by poping the top of the segment stack,
 * placing the poped segment on top of the parser stack. This means that the next
 * segment to be added to the segment that is now on top of the segment stack is
 * the one that was just poped.
 */
struct nfa_substate {
    struct nfa_segment nfa;
    struct stack_node node;
};

/* Struct used to bundle the actual segment stack with a pointer to
 * its node pool.
 *
 * @stackp:    pointer to the underlying generic stack
 * @node_pool: the pool of nfa substates
 */

struct segment_stack {
    struct stack *stackp;
    struct nfa_substate *node_pool;
};


/* Struct used to represent up to 16 stacks of nfa segments
 * using a single array. All operations performed on the topmost
 * stack. Essentially, a stack of up to 16 stacks
 *
 * Used to hold nfa_segments to be added to the segment on top of the
 * segment stack. Opening and closing parentheses encountered when parsing
 * correspond to pushing and poping the level of the parser stack, respectively.
 *
 * Note that neither pushing nor poping deals with elements of the stack,
 * they simply adjust the top index. Accessing the elements can be done
 * only with the top function.
 *
 * @segments:   the pool of nfa segments
 * @bottoms:    index of the bottom slot of the 16 stacks
 * @nest_level: index of the topmost stack, used to identify its bottom
 * @index:      index of the stack top
 */
struct parser_stack {
    struct nfa_segment segments[PARSER_STACK_SIZE];
    unsigned bottoms[PARSER_MAX_NESTED_PARENS];
    unsigned index;
    unsigned nest_level;
    enum parser_stack_nest_type nest_type[PARSER_MAX_NESTED_PARENS];
};

/* Create empty parser stack
 *
 * @name: the name of the instance
 */
#define parser_stack_init(name) \
    struct parser_stack name = { .nest_level = 0, .index = 0 }; \
    name.bottoms[0] = 0; \
    name.nest_type[0] = parser_stack_nest_type_none;

/* Push the top of the current level of the parser stack */
static inline void parser_stack_push(struct parser_stack *stack) {
    ++stack->index;
}

/* Pop the top of the current level of the parser stack */
static inline void parser_stack_pop(struct parser_stack *stack) {
    --stack->index;
}

/* Return pointer to the current top of the parser stack */
static inline struct nfa_segment *parser_stack_top(struct parser_stack *stack) {
    return &stack->segments[stack->index - 1];
}

/* Get size of the current level in the parser stack */
static inline unsigned parser_stack_level_size(struct parser_stack const *stack) {
    return stack->index - stack->bottoms[stack->nest_level];
}

/* Push the level of the parser stack */
static inline void parser_stack_push_nest_level(struct parser_stack *stack, enum parser_stack_nest_type type) {
    stack->bottoms[++stack->nest_level] = stack->index;
    stack->nest_type[stack->nest_level] = type;
}

/* Pop the level of the parser stack */
static inline void parser_stack_pop_nest_level(struct parser_stack *stack) {
    --stack->nest_level;
}

static inline enum parser_stack_nest_type parser_stack_top_nest_type(struct parser_stack const *stack) {
    return stack->nest_type[stack->nest_level];
}

static inline bool parser_stack_top_nest_is_alternation(struct parser_stack const *stack) {
    return parser_stack_top_nest_type(stack) == parser_stack_nest_type_alternation;
}

/* Get the nfa_substate indentified by node */
static inline struct nfa_substate *parser_get_nfa_substate(struct stack_node *node) {
    return container(node, struct nfa_substate, node);
}

/* Get the size of the segment stack */
static inline unsigned segment_stack_size(struct segment_stack const *segment_stack) {
    return stack_size(segment_stack->stackp);
}

/* Push a new empty segment to the segment stack
 *
 * @state_pool:    pool of nfa states
 * @segment_stack: the segment stack
 * @node:          pointer to the node used to identify the nfa_substate
 */
static inline int segment_stack_push(struct nfa_state_pool *state_pool, struct segment_stack *segment_stack, regex_capture_type capture_group) {
    struct stack_node *node = &segment_stack->node_pool[segment_stack_size(segment_stack)].node;
    stack_push(segment_stack->stackp, node);
    return nfa_init_segment(state_pool, &parser_get_nfa_substate(node)->nfa, capture_group);
}

/* Pop the topmost nfa_substate off of the segment stack */
static inline struct nfa_substate *segment_stack_pop(struct segment_stack *segment_stack) {
    return parser_get_nfa_substate(stack_pop(segment_stack->stackp));
}

/* Peek at the topmost nfa_substate of the segment stack */
static inline struct nfa_substate *segment_stack_peek(struct segment_stack *segment_stack) {
    return parser_get_nfa_substate(stack_peek(segment_stack->stackp));
}

/* Get the nfa segment at the top of the segment stack */
static inline struct nfa_segment *segment_stack_top(struct segment_stack *segment_stack) {
    return &segment_stack_peek(segment_stack)->nfa;
}

static inline regex_capture_type segment_stack_top_capture_group(struct segment_stack *segment_stack) {
    return segment_stack_peek(segment_stack)->nfa.capture_group;
}

static void parser_init_state_pool(struct nfa *nfa, struct nfa_state_pool *state_pool) {
    for(unsigned i = 0; i < NFA_STATE_POOL_SIZE; i++) {
        state_pool->queue_node_pool[i].state = &nfa->state_pool[i];
        queue_enqueue(state_pool->queue, &state_pool->queue_node_pool[i].node);
    }
}

/* Transfer the nfa segment at the top (level n) of the segment stack to the new top (level n-1) of the parser stack */
static inline void segment_stack_top_to_parser_stack(struct segment_stack *segment_stack, struct parser_stack *parser_stack) {
    struct nfa_segment *segment = &segment_stack_pop(segment_stack)->nfa;

    /* Place top of segment stack on top of parser stack */
    parser_stack_push(parser_stack);
    memcpy(parser_stack_top(parser_stack), segment, sizeof(*segment));
}

/* Flush the current level of the parser stack, adding any states to the segment on top of the segment stack */
static inline int parser_stack_flush(struct nfa_state_pool *state_pool, struct segment_stack *segment_stack, struct parser_stack *parser_stack) {
    int status = 0;

    struct nfa_segment *nfa = segment_stack_top(segment_stack);

    while(parser_stack_level_size(parser_stack)) {
        status = nfa_concatenate(state_pool, &nfa->tail, parser_stack_top(parser_stack));
        if(status < 0) {
            return status;
        }
        parser_stack_pop(parser_stack);
    }

    struct nfa_segment *segment;

    while(parser_stack_top_nest_is_alternation(parser_stack)) {
        parser_stack_pop_nest_level(parser_stack);

        segment = parser_stack_top(parser_stack);

        parser_stack_pop(parser_stack);

        status = nfa_alternate(segment_stack_top(segment_stack), segment);

        if(status < 0) {
            break;
        }
    }

    return status;
}

/* Add top state of the parser stack to the segment on top of the segment stack */
static int regex_concatenate_queued_segment(struct nfa_state_pool *state_pool, struct segment_stack *segment_stack, struct parser_stack *parser_stack) {
    int status = 0;
    struct nfa_segment *nfa = segment_stack_top(segment_stack);

    status = nfa_concatenate(state_pool, &nfa->tail, parser_stack_top(parser_stack));

    if(status < 0) {
        return status;
    }

    parser_stack_pop(parser_stack);

    return 0;
}

/* Append a zero-one repetition (?) of the segment at the top of the parser stack to the tail of the segment at the top of the segment stack */
static inline int regex_parse_repeat_zero_one(struct nfa_state_pool *state_pool, struct parser_stack *parser_stack) {
    if(!parser_stack_level_size(parser_stack)) {
        /* No segment to apply ? to */
        return -EFORMAT;
    }

    return nfa_repeat_zero_one(state_pool, parser_stack_top(parser_stack));
}

/* Append a one-any repetition (+) of the segment at the top of the parser stack to the tail of the segment at
 * the top of the segment stack
 * */
static int regex_parse_repeat_one_any(struct nfa_state_pool *state_pool, struct parser_stack *parser_stack) {
    int status = 0;

    if(!parser_stack_level_size(parser_stack)) {
        /* No segment to appy + to */
        return -EFORMAT;
    }
    regex_flag_type flags = 0;

    struct nfa_segment *segment = parser_stack_top(parser_stack);

    status = nfa_repeat_one_any(state_pool, segment);

    if(status < 0) {
        return status;
    }

    segment->tail->flags = flags;

    return 0;
}

/* Append a zero-any repetition (*) of the segment at the top of the parser stack to the tail of the
 * segment at the top of the segment stack
 * */
static inline int regex_parse_repeat_zero_any(struct nfa_state_pool *state_pool, struct parser_stack *parser_stack) {
    int status = 0;

    if(!parser_stack_level_size(parser_stack)) {
        /* No segment to apply * to */
        return -EFORMAT;
    }

    regex_flag_type flags = 0;

    struct nfa_segment *segment = parser_stack_top(parser_stack);
    status = nfa_repeat_zero_any(state_pool, segment);

    if(status < 0) {
        return status;
    }

    segment->tail->flags = flags;

    return 0;
}

/* Parse an alternation. Pushes the current top segment of the segment stack to the top of the parser stack,
 * starting the construction of a new segment to alternate with the former
 */
static int regex_parse_alternation(struct nfa_state_pool *state_pool, struct segment_stack *segment_stack, struct parser_stack *parser_stack) {
    if(!parser_stack_level_size(parser_stack)) {
        /* No segment to apply | to */
        return -EFORMAT;
    }

    int status = 0;

    if(parser_stack_level_size(parser_stack)) {
        status = regex_concatenate_queued_segment(state_pool, segment_stack, parser_stack);

        if(status < 0) {
            return status;
        }
    }

    if(parser_stack->nest_level >= PARSER_MAX_NESTED_PARENS || segment_stack_size(segment_stack) >= PARSER_MAX_NESTED_PARENS) {
        return -E2BIG;
    }

    regex_capture_type capture_group = segment_stack_top_capture_group(segment_stack);

    segment_stack_top_to_parser_stack(segment_stack, parser_stack);
    parser_stack_push_nest_level(parser_stack, parser_stack_nest_type_alternation);

    return segment_stack_push(state_pool, segment_stack, capture_group);
}

/* Parse opening parenthesis in regex.
 *
 * Performs the following:
 *  - Pushes the nesting level of the parser stack
 *  - Pushes a new nfa segment to the segment stack. This segment
 *    is the one that subsequent segments are appended to, until a matching
 *    clonsing parenthesis is encountered
 */
static inline int regex_parse_open_paren(struct nfa_state_pool *state_pool, struct segment_stack *segment_stack, struct parser_stack *parser_stack, regex_capture_type capture_group) {
    if(capture_group >= REGEX_MAX_CAPTURE_GROUPS) {
        return -E2BIG;
    }

    if(parser_stack_level_size(parser_stack)) {
        regex_concatenate_queued_segment(state_pool, segment_stack, parser_stack);
    }

    if(parser_stack->nest_level >= PARSER_MAX_NESTED_PARENS || segment_stack_size(segment_stack) >= PARSER_MAX_NESTED_PARENS) {
        return -E2BIG;
    }
    /* Capture 0 reserved for entire match */
    regex_capture_type capture = capture_group;

    parser_stack_push_nest_level(parser_stack, parser_stack_nest_type_parenthesis);
    return segment_stack_push(state_pool, segment_stack, capture);
}

/* Parse closing parenthesis in regex.
 *
 * Performs the following:
 *  - Ensures a matching opening parenthesis has been encountered
 *  - Ensures the parenthesis are not empty
 *  - Pops the nesting level of the parser stack from level n to n-1
 *  - Pops an nfa segment off of top of the segment stack and places
 *    this at the top of level n-1 of the parser stack
 */
static int regex_parse_close_paren(struct nfa_state_pool *state_pool, struct segment_stack *segment_stack, struct parser_stack *parser_stack) {
    int status;

    if(segment_stack_size(segment_stack) < 2 || parser_stack_level_size(parser_stack) == 0) {
        /* Unbalanced or empty parens */
        return -EFORMAT;
    }

    status = parser_stack_flush(state_pool, segment_stack, parser_stack);

    if(status < 0) {
        return status;
    }

    struct nfa_segment *segment = &segment_stack_peek(segment_stack)->nfa;

    regex_flag_set_capture_start(&segment->entry->flags);
    regex_flag_set_capture_end(&segment->tail->flags);
    regex_capture_set_group(&segment->entry->capture, segment->capture_group);
    regex_capture_set_group(&segment->tail->capture, segment->capture_group);

    parser_stack_pop_nest_level(parser_stack);

    segment_stack_top_to_parser_stack(segment_stack, parser_stack);

    return 0;
}


/* Parse single regex_char c */
static int regex_parse_char(struct nfa_state_pool *state_pool, struct segment_stack *segment_stack, struct parser_stack *parser_stack, regex_char c, regex_flag_type flags) {
    if(parser_stack_level_size(parser_stack)) {
        regex_concatenate_queued_segment(state_pool, segment_stack, parser_stack);
    }

    parser_stack_push(parser_stack);
    return nfa_construct_atomic_segment(state_pool, parser_stack_top(parser_stack), c, flags);
}

static inline int regex_parse_metacharacter(struct nfa_state_pool *state_pool, struct segment_stack *segment_stack, struct parser_stack *parser_stack, char c) {
    regex_char metachar;
    regex_flag_type flags = (c >= 'A' && c <= 'Z') * REGEX_FLAG_NEGATE;

    switch(c) {
        case 'd':
        case 'D':
            metachar = regex_charcode_digit;
            break;
        case 'w':
        case 'W':
            metachar = regex_charcode_alnum;
            break;
        case 's':
        case 'S':
            metachar = regex_charcode_whitespace;
            break;
        default:
            return -EFORMAT;
            break;
    }

    return regex_parse_char(state_pool, segment_stack, parser_stack, metachar, flags);
}

/* Parse single escaped char c */
static inline int regex_parse_escaped_char(struct nfa_state_pool *state_pool, struct segment_stack *segment_stack, struct parser_stack *parser_stack, char const *regex) {

    if(!*regex) {
        return -EFORMAT;
    }

    if(regex_is_escape_metacharacter(*regex)) {
        return regex_parse_metacharacter(state_pool, segment_stack, parser_stack, *regex);
    }

    if(!regex_is_special_char(*regex)) {
        return -EMALFORMED_ESCAPE;
    }


    return regex_parse_char(state_pool, segment_stack, parser_stack, (regex_char)*regex, REGEX_FLAG_NONE);
}
static inline int regex_parse_anchor_start(struct nfa *restrict nfa, char const *pos, char const *regex) {
    if(pos != regex) {
        return -EFORMAT;
    }

    regex_flag_set_anchor_start(&nfa->flags);
    return 0;
}

static inline int regex_parse_anchor_end(struct nfa *restrict nfa, char const *pos, char const *regex) {
    if(pos != regex + strlen(regex) - 1) {
        return -EFORMAT;
    }

    regex_flag_set_anchor_end(&nfa->flags);
    return 0;
}

static int regex_parse_quantifier(struct nfa_state_pool *state_pool, struct parser_stack *parser_stack, char const **regex) {

    if(!parser_stack_level_size(parser_stack)) {
        /* No segment to repeat */
        return -EFORMAT;
    }

    int quantifiers[2];

    quantifiers[0] = -1;
    quantifiers[1] = -1;

    unsigned quantifier_index = 0;

    char buffer[PARSER_QUANTIFIER_MAX_LENGTH];
    unsigned buffer_index = 0;

    for(++(*regex); **regex; ++(*regex)) {
        switch(**regex) {
            case ',':
                if(quantifier_index) {
                    return -EFORMAT;
                }

                /* This interprets {,m} as {0,m} by design */
                buffer[buffer_index] = '\0';
                quantifiers[quantifier_index++] = atoi(buffer);
                buffer_index = 0;

                break;
            case '}':
                if(buffer_index) {
                    buffer[buffer_index] = '\0';
                    quantifiers[quantifier_index] = atoi(buffer);
                }
                goto parse_success;

            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                buffer[buffer_index++] = **regex;

                if(buffer_index >= array_size(buffer)) {
                    return -E2BIG;
                }
                break;

            default:
                return -EFORMAT;
        }
    }

    if(!**regex) {
        return -EFORMAT;
    }

parse_success:

    /* Catch {} */
    if(quantifiers[0] == -1) {
        return -EFORMAT;
    }

    /* Either {n,} or {n,m} */
    if(quantifier_index) {
        /* {n,} */
        if(quantifiers[1] == -1) {
            return nfa_repeat_n_to_any(state_pool, parser_stack_top(parser_stack), quantifiers[0]);
        }
        /* {n,m} where n <= m, not supported */
        else if(quantifiers[1] <= quantifiers[0]) {
            return -EFORMAT;
        }
        /* {n,m} */
        else {
            return nfa_repeat_n_to_m(state_pool, parser_stack_top(parser_stack), quantifiers[0], quantifiers[1]);
        }
    }

    /* {n} */

    /* Catch .{0} */
    if(!quantifiers[0]) {
        return -EFORMAT;
    }

    return nfa_repeat_n(state_pool, parser_stack_top(parser_stack), quantifiers[0]);
}

static inline regex_char cidx(regex_char idx) {
    return idx - PARSER_CHAR_CLASS_MEMBER_SHIFT;
}

/* Parse a char class range (e.g. a-z)
 *
 * state_pool:  pool of nfa states
 * char_record: pointer to array of booleans used to keep track of chars already parsed
 * segments:    pointer to array of nfa segments, used for storing generated segments
 * nsegments:   number of segments in the array
 * range:       the range being parsed
 */
static int regex_parse_char_class_range(struct nfa_state_pool *state_pool, bool *char_record, struct nfa_segment *segments, unsigned *nsegments, struct char_class_range *range) {
    int status = 0;

    if(range->end <= range->start) {
        return -EFORMAT;
    }

    if(!((range->start >= 'a' && range->end <= 'z') ||
         (range->start >= 'A' && range->end <= 'Z') ||
         (range->start >= '0' && range->end <= '9')))
    {
        return -EFORMAT;
    }

    /* If range is a-z, A-Z or 0-9, use more efficient encoding */
    if(range->start == 'a' && range->end == 'z') {
        if(!char_record[cidx(regex_charcode_lower_case)]) {
            char_record[cidx(regex_charcode_lower_case)] = true;
            memset(&char_record[cidx('a')], 1, PARSER_ASCII_ALPHABET_SIZE);
            status = nfa_construct_segment(state_pool, &segments[(*nsegments)++], regex_charcode_lower_case, range->flags);
        }
    }
    else if(range->start == 'A' && range->end == 'Z') {
        if(!char_record[cidx(regex_charcode_upper_case)]) {
            char_record[cidx(regex_charcode_upper_case)] = true;
            memset(&char_record[cidx('A')], 1, PARSER_ASCII_ALPHABET_SIZE);
            status = nfa_construct_segment(state_pool, &segments[(*nsegments)++], regex_charcode_upper_case, range->flags);
        }
    }
    else if(range->start == '0' && range->end == '0') {
        if(!char_record[cidx(regex_charcode_digit)]) {
            char_record[cidx(regex_charcode_digit)] = true;
            memset(&char_record[cidx('0')], 1, PARSER_ASCII_DIGITS);
            status = nfa_construct_segment(state_pool, &segments[(*nsegments)++], regex_charcode_digit, range->flags);
        }
    }
    else {
        for(regex_char c = range->start; c <= range->end; ++c) {
            if(!char_record[cidx(c)]) {
                char_record[cidx(c)] = true;
                status = nfa_construct_segment(state_pool, &segments[(*nsegments)++], c, range->flags);

                if(status < 0) {
                    return status;
                }
            }
        }
    }

    /* Reset range */
    range->start = 0;
    range->end = 0;

    return status;
}

/* Generate nfa segment representing a negated char class
 *
 * state_pool: pool of nfa states
 * segments:   pointer to array of nfa segments to be combined
 * nsegments:  number of segments in the array
 */
static int regex_generate_negated_char_class_segment(struct nfa_state_pool *state_pool, struct nfa_segment *segments, unsigned nsegments) {
    int status = 0;

    /* Allow simulation to advance input for last segment */
    segments[nsegments - 1].entry->flags &= ~REGEX_FLAG_HALT_ADVANCE;

    for(unsigned i = 1; i < nsegments; i++) {
        status = nfa_concatenate(state_pool, &segments[0].tail, &segments[i]);

        if(status < 0) {
            return status;
        }
    }

    return 0;
}

/* Generate nfa segment representing a non-negated char class
 *
 * segments:  pointer to array of nfa segments to be combined
 * nsegments: number of segments in the array
 */
static int regex_generate_non_negated_char_class_segment(struct nfa_segment *segments, unsigned nsegments) {
    int status = 0;

    /* Reduce segments. Each iteration alternates segment i with segment j, where
     *   0 <= i < pass / 2,
     *   pass / 2 <= j == i + pass / 2 < pass.
     * The resulting segment is stored in segment i.
     *
     * This causes the worst-case depth of the entire char class segment to be
     * logarithmic instead of linear.
     */
    for(unsigned pass = nsegments; pass > 1; pass >>= 1) {
        for(unsigned i = 0, j = pass >> 1; i < pass >> 1; i++, j++) {
            status = nfa_alternate(&segments[i], &segments[j]);
            if(status < 0) {
                return status;
            }
        }
        /* Handle potential residual */
        if(pass % 2) {
            status = nfa_alternate(&segments[0], &segments[pass - 1]);
            if(status < 0) {
                return status;
            }
        }
    }

    return 0;
}

/* Parse character class ([...]) */
static int regex_parse_char_class(struct nfa_state_pool *state_pool, struct parser_stack *parser_stack, struct segment_stack *segment_stack, char const **regex) {
    int status = 0;

    if(parser_stack_level_size(parser_stack)) {
        regex_concatenate_queued_segment(state_pool, segment_stack, parser_stack);
    }
    /* Used to avoid duplicate states in case of bad input */
    bool char_record[PARSER_MAX_CHAR_CLASS_SIZE] = { 0 };

    struct nfa_segment segments[PARSER_MAX_CHAR_CLASS_SIZE];
    unsigned nsegments = 0;

    struct char_class_range range = {
        .start = 0,
        .end = 0,
    };

    switch(*++(*regex)) {
        case '^':
            ++(*regex);
            range.flags = (REGEX_FLAG_NEGATE | REGEX_FLAG_HALT_ADVANCE);
            break;
        default:
            range.flags = REGEX_FLAG_NONE;
            break;
    }

    if(**regex == ']') {
        /* Empty char class */
        return -EFORMAT;
    }

    for(; **regex; ++(*regex)) {
        switch(**regex) {
            case '-':
                if(!range.start) {
                    return -EFORMAT;
                }
                range.end = *++(*regex);
                status = regex_parse_char_class_range(state_pool, char_record, segments, &nsegments, &range);
                break;
            case ']':
                goto parse_success;

            case '\\':
                ++(*regex);
                if(!regex_is_escapable_in_char_class(**regex)) {
                    return -EFORMAT;
                }
                /* fall through */
            default:
                if(range.start && !char_record[cidx(range.start)]) {
                    char_record[cidx(range.start)] = true;
                    status = nfa_construct_segment(state_pool, &segments[nsegments++], (regex_char) range.start, range.flags);
                }
                range.start = **regex;
                break;
        }

        if(status < 0) {
            return status;
        }
    }

    if(!**regex) {
        /* No closing bracket */
        return -EFORMAT;
    }

parse_success:

    /* Construct segment for remaining queued up char */
    if(range.start && !char_record[cidx(range.start)]) {
        status = nfa_construct_segment(state_pool, &segments[nsegments++], (regex_char) range.start, range.flags);

        if(status < 0) {
            return status;
        }
    }

    /* Generate the full segment */
    if(range.flags) {
        status = regex_generate_negated_char_class_segment(state_pool, segments, nsegments);
    }
    else {
        status = regex_generate_non_negated_char_class_segment(segments, nsegments);
    }

    if(status < 0) {
        return status;
    }

    /* Place segment at the top of the parser stack */
    parser_stack_push(parser_stack);
    memcpy(parser_stack_top(parser_stack), &segments[0], sizeof(segments[0]));

    return 0;
}

static inline char const *regex_get_escaped_char(char const **pos, char const *startpos, char const *regex) {
    return regex + (size_t)((*pos)++ - startpos + 1);
}

/* Convert regex to nfa */
int regex_to_nfa(struct nfa *nfa, char const *regex, regex_flag_type flags) {
    static_assert(SAFECMP(unsigned, NFA_STATE_POOL_SIZE, <=, QUEUE_STATIC_SIZE), "NFA state pool too large for queue");
    static_assert(SAFECMP(unsigned, PARSER_MAX_NESTED_PARENS, <=, STACK_STATIC_SIZE), "Number of nested parentheses too large for stack");

    int status = 0;

    flags &= REGEX_COMPILATION_MASK;
    nfa->flags = flags;

    /* Used for storing regex converted to lower case */
    char lcbuffer[REGEX_MAX_SIZE];
    /* Pointer to input, either the regex param or lcbuffer */
    char const *reginput = regex;

    if(regex_flag_test(flags, REGEX_FLAG_IGNORE_CASE)) {
        status = strlower(lcbuffer, regex, sizeof(lcbuffer));

        if(status < 0) {
            return status;
        }

        reginput = lcbuffer;
    }

    struct nfa_substate nfa_pool[PARSER_MAX_NESTED_PARENS];
    struct nfa_state_node queue_node_pool[NFA_STATE_POOL_SIZE];

    queue_init(queue);

    struct nfa_state_pool state_pool = {
        .queue = &queue,
        .queue_node_pool = queue_node_pool
    };

    parser_init_state_pool(nfa, &state_pool);

    stack_init(segstack);

    struct segment_stack segment_stack = {
        .stackp = &segstack,
        .node_pool = &nfa_pool[0]
    };

    parser_stack_init(parser_stack);

    status = segment_stack_push(&state_pool, &segment_stack, REGEX_CAPTURE_NONE);

    if(status < 0) {
        return status;
    }

    for(char const *str = reginput; *str; ++str) {
        switch(*str) {
            case '?':
                status = regex_parse_repeat_zero_one(&state_pool, &parser_stack);
                break;
            case '+':
                status = regex_parse_repeat_one_any(&state_pool, &parser_stack);
                break;
            case '*':
                status = regex_parse_repeat_zero_any(&state_pool, &parser_stack);
                break;
            case '|':
                status = regex_parse_alternation(&state_pool, &segment_stack, &parser_stack);
                break;
            case '(':
                status = regex_parse_open_paren(&state_pool, &segment_stack, &parser_stack, nfa->ncaptures++);
                break;
            case ')':
                status = regex_parse_close_paren(&state_pool, &segment_stack, &parser_stack);
                break;
            case '.':
                status = regex_parse_char(&state_pool, &segment_stack, &parser_stack, regex_charcode_match_any, REGEX_FLAG_NONE);
                break;
            case '\\':
                status = regex_parse_escaped_char(&state_pool, &segment_stack, &parser_stack, regex_get_escaped_char(&str, reginput, regex));
                break;
            case '^':
                status = regex_parse_anchor_start(nfa, str, regex);
                break;
            case '$':
                status = regex_parse_anchor_end(nfa, str, regex);
                break;
            case '{':
                status = regex_parse_quantifier(&state_pool, &parser_stack, &str);
                break;
            case '[':
                status = regex_parse_char_class(&state_pool, &parser_stack, &segment_stack, &str);
                break;
            default:
                status = regex_parse_char(&state_pool, &segment_stack, &parser_stack, (regex_char)*str, REGEX_FLAG_NONE);
                break;
        }

        if(status) {
            return status;
        }
    }

    if(segment_stack_size(&segment_stack) != 1) {
        /* Paren imbalance */
        return -EFORMAT;
    }

    status = parser_stack_flush(&state_pool, &segment_stack, &parser_stack);

    if(status < 0) {
        return status;
    }

    struct nfa_segment *segment = segment_stack_top(&segment_stack);

    /* Make the tail of the nfa an accept state */
    nfa_accept_state(segment->tail);
    nfa->start_index = (segment->entry - nfa->state_pool) / sizeof(*segment->entry);

    return status;
}
