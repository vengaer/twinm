#include "macro.h"
#include "nfa.h"
#include "queue.h"
#include "rbtree.h"
#include "strutils.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

/* Max size of an nfa segment when converting
 * from nfa to regex */
enum { REGEX_SEGMENT_SIZE = NFA_STATE_POOL_SIZE };

/* Struct used for storing nfa edges in an rbtree
 *
 * @addr:   the address of the 0th edge of an nfa_state
 * @rbnode: the tree node
 */
struct nfa_addr_node {
    struct list_head *addr;
    struct rbnode rbnode;
};

/* Compare nfa_state addresses, used with rbtree */
static int nfa_compare_edge_addrs(struct rbnode const *n0, struct rbnode const *n1) {
    return container(n1, struct nfa_addr_node, rbnode, const)->addr -
           container(n0, struct nfa_addr_node, rbnode, const)->addr;
}

struct nfa_duplication_table_entry {
    struct nfa_state *orig_state;
    struct nfa_state *new_state;
    struct rbnode rbnode;
};

static int nfa_compare_duplication_table_entries(struct rbnode const *n0, struct rbnode const *n1) {
    return container(n1, struct nfa_duplication_table_entry, rbnode, const)->orig_state -
           container(n0, struct nfa_duplication_table_entry, rbnode, const)->orig_state;
}

/* Get next nfa_state from the state pool */
static inline struct nfa_state *nfa_state_pool_dequeue(struct nfa_state_pool *state_pool) {
    struct nfa_state *state = container(queue_dequeue(state_pool->queue), struct nfa_state_node, node)->state;
    state->flags = 0;
    memzero(&state->capture, sizeof(state->capture));
    return state;
}

/* Add atomaton state back to the pool */
static inline void nfa_state_pool_enqueue(struct nfa_state const *state, struct nfa_state_pool *state_pool)
{
    /* States enqueued in order */
    size_t index = array_index(state_pool->queue_node_pool[0].state, state);

    queue_enqueue(state_pool->queue, &state_pool->queue_node_pool[index].node);
}

/* Check whether there are still unused states available in the state pool
 *
 * @state_pool:      pool of nfa states
 * @required_states: the number of states requested
 */
static inline bool nfa_pool_size_exceeded(struct nfa_state_pool const *state_pool, unsigned required_states) {
    return queue_size(state_pool->queue) < required_states;
}

/* Check whether given state results in multiple paths to a later node */
static inline bool nfa_state_is_junction(struct nfa_state const *state) {
    return state->charcode == regex_charcode_alt || state->charcode == regex_charcode_repeat_zero_one;
}

static inline struct list_head *nfa_junction_successor(struct nfa_state *state) {
    struct list_head *node = &state->edges[1];

    switch(state->charcode) {
        case regex_charcode_repeat_zero_one:
            /* NOP */
            break;
        case regex_charcode_alt:
            node = &nfa_follow_edge(node)->edges[0];
            break;
        default:
            assert(0);
            break;
    }


    return node;
}

/* Get list of predecessors of target, starting in start. Return size of list
 *
 * This might not be entirely straight forward in some situations, e.g.
 *
 *                        -
 *       *-------------->|H|-------------*
 *       |      -         -      -       |
 *       |  *->|B|--*        *->|E|--*   |
 *       - /    -    \    - /    -    \  |   -
 *   -->|A|           *->|D|           *-*->|G|
 *       - \    -    /    - \    -    /      -
 *       ^  *->|C|--*        *->|F|--*       ^
 *       |      -                -           |
 *     start                               target
 *
 * The issue boils down to finding the direct predecessors of the target node in the digraph
 * represented by the nfa. The algorithm performs what is essentially an iterative
 * depth-first search, storing traversed edges in a tree.
 *
 * @list:   pointer to the head of the list to which the predecessors are to be appended
 * @start:  the start in which to start the search
 * @target: the node whose predecessors it to be found
 * */
static int nfa_find_predecessors(struct list_head *list, struct nfa_state *start, struct nfa_state const *target) {
    static_assert(SAFECMP(unsigned, NFA_STATE_POOL_SIZE, <=, QUEUE_STATIC_SIZE), "NFA state pool too large for queue");

    /* Struct to store list nodes in queue */
    struct queue_node_pool {
        struct list_head *addr;
        struct queue_node queue_node;
    };

    struct nfa_addr_node rbnode_pool[NFA_STATE_POOL_SIZE];
    struct queue_node_pool queue_node_pool[NFA_STATE_POOL_SIZE];

    unsigned queue_pool_index = 0;
    unsigned rbnode_pool_index = 0;

    /* Number of predecessors found */
    int npredecessors = 0;

    list_clear(list);

    /* Tree used to keep track of paths already traversed */
    rbtree_init(tree);
    /* Queue used to store junctions */
    queue_init(queue);

    struct queue_node_pool *qnode = &queue_node_pool[queue_pool_index++];

    qnode->addr = &start->edges[0];
    queue_enqueue(&queue, &qnode->queue_node);

    /* Separate head of the list to search, required to check the immediate successor of start */
    list_init(state_list);

    struct list_head *iter;
    struct list_head *junction;

    struct nfa_state *state;

    bool path_trodden;

    while(queue_size(&queue)) {
        path_trodden = false;

        /* Next junction */
        qnode = container(queue_dequeue(&queue), struct queue_node_pool, queue_node);
        junction = qnode->addr;

        list_clear(&state_list);
        list_push_back(&state_list, junction);

        list_for_each(iter, &state_list) {
            rbnode_pool[rbnode_pool_index].addr = iter;

            /* No need to keep node passed to find, don't increment index */
            if(rbtree_find(&tree, &rbnode_pool[rbnode_pool_index].rbnode, nfa_compare_edge_addrs)) {
                /* Path has already been traversed, move on to the next */
                path_trodden = true;
                break;
            }

            state = container(iter, struct nfa_state, edges);

            if(nfa_state_is_junction(state)) {
                /* iter refers to a junction, add it to the queue */
                qnode = &queue_node_pool[queue_pool_index++];
                qnode->addr = nfa_junction_successor(state);
                queue_enqueue(&queue, &qnode->queue_node);
            }
            else {
                /* iter does not refer to a junction, add it to the tree of already traversed non-junction states.
                 * iter assigned to the node earlier in the loop */
                if(!rbtree_insert(&tree, &rbnode_pool[rbnode_pool_index++].rbnode, nfa_compare_edge_addrs)) {
                    /* Node already in tree, no need to keep this one */
                    --rbnode_pool_index;
                }
            }

            if(iter->tail && iter->tail == &target->edges[0]) {
                /* Predecessor found */
                break;
            }
        }

        if(!path_trodden) {

            if(!iter) {
                /* Didn't find target */
                return -EFORMAT;
            }

            ++npredecessors;

            /* Zero the 0th edge of the state (i.e. iter), lest the list would contain other nodes from the nfa.
            * This has to be done when linking the node in the alternation later anyway. */
            list_clear(iter);
            list_push_back(list, iter);
        }
    }

    return npredecessors;
}

/* Duplicate nfa segment
 *
 * @state_pool:   pool of nfa states
 * @new_segment:  atomic nfa segment
 * @orig_segment: segment to duplicate
 */
static int nfa_duplicate_segment(struct nfa_state_pool *state_pool, struct nfa_segment *new_segment, struct nfa_segment const* orig_segment) {
    struct nfa_state faux_head;
    memzero(&faux_head.edges[0], sizeof(faux_head.edges));

    struct duplication_queue_node {
        struct nfa_state *orig_state;
        struct nfa_state *new_prior;
        unsigned short edge_index;
        struct queue_node queue_node;
    };

    struct nfa_addr_node addr_pool[NFA_STATE_POOL_SIZE];
    unsigned addr_index = 0;

    rbtree_init(end_states);

    struct nfa_addr_node *addr_node = &addr_pool[addr_index++];
    addr_node->addr = &orig_segment->tail->edges[0];
    rbtree_insert(&end_states, &addr_node->rbnode, nfa_compare_edge_addrs);

    struct duplication_queue_node queue_pool[NFA_STATE_POOL_SIZE];
    unsigned queue_index = 0;

    struct nfa_duplication_table_entry table_pool[NFA_STATE_POOL_SIZE];
    unsigned table_index = 0;

    rbtree_init(table);

    queue_init(queue);

    struct duplication_queue_node *dqnode = &queue_pool[queue_index++];

    dqnode->orig_state = orig_segment->entry;
    dqnode->new_prior = &faux_head;
    dqnode->edge_index = 0;
    queue_enqueue(&queue, &dqnode->queue_node);

    struct nfa_duplication_table_entry *table_entry = &table_pool[table_index++];

    table_entry->orig_state = orig_segment->entry;
    table_entry->new_state = new_segment->entry;
    rbtree_insert(&table, &table_entry->rbnode, nfa_compare_duplication_table_entries);

    struct rbnode *rbiter;

    struct nfa_state *orig_state;
    struct nfa_state *new_state;
    struct nfa_state *new_prior;
    unsigned short edge_index;

    while(queue_size(&queue)) {

        dqnode = container(queue_dequeue(&queue), struct duplication_queue_node, queue_node);
        orig_state = dqnode->orig_state;
        new_prior = dqnode->new_prior;
        edge_index = dqnode->edge_index;

        table_entry = &table_pool[table_index++];
        table_entry->orig_state = orig_state;

        rbiter = rbtree_find(&table, &table_entry->rbnode, nfa_compare_duplication_table_entries);

        if(rbiter) {
            --table_index;
            table_entry = container(rbiter, struct nfa_duplication_table_entry, rbnode);
            new_state = container(rbiter, struct nfa_duplication_table_entry, rbnode)->new_state;
        }
        else {
            if(nfa_pool_size_exceeded(state_pool, 1)) {
                return -E2BIG;
            }

            new_state = nfa_state_pool_dequeue(state_pool);
            table_entry->new_state = new_state;

            rbtree_insert(&table, &table_entry->rbnode, nfa_compare_duplication_table_entries);

            /* Delay capture to start at duplicated segment */
            new_state->capture = orig_state->capture;
            regex_flag_set_late_capture_start(&orig_state->flags);
        }

        list_push_back(&new_prior->edges[edge_index], &new_state->edges[0]);

        addr_node = &addr_pool[addr_index++];
        addr_node->addr = &orig_state->edges[0];

        if(!rbtree_find(&end_states, &addr_node->rbnode, nfa_compare_edge_addrs)) {
            if(orig_state->charcode == regex_charcode_repeat_zero_any) {
                addr_node = &addr_pool[addr_index++];
                addr_node->addr = &orig_state->edges[0];
                rbtree_insert(&end_states, &addr_node->rbnode, nfa_compare_edge_addrs);
            }
            else if(orig_state->charcode == regex_charcode_split) {
                addr_node = &addr_pool[addr_index++];
                addr_node->addr = &nfa_follow_edge(&orig_state->edges[1])->edges[0];
                rbtree_insert(&end_states, &addr_node->rbnode, nfa_compare_edge_addrs);
            }

            new_state->charcode = orig_state->charcode;
            new_state->flags = orig_state->flags;
            memzero(&new_state->edges[0], sizeof(new_state->edges));

            dqnode = &queue_pool[queue_index++];
            dqnode->orig_state = nfa_follow_edge(&orig_state->edges[0]);
            dqnode->edge_index = 0;
            dqnode->new_prior = new_state;
            queue_enqueue(&queue, &dqnode->queue_node);

            if(!list_empty(&orig_state->edges[1])) {
                dqnode = &queue_pool[queue_index++];
                dqnode->orig_state = nfa_follow_edge(&orig_state->edges[1]);
                dqnode->new_prior = new_state;
                dqnode->edge_index = 1;
                queue_enqueue(&queue, &dqnode->queue_node);
            }
        }
        else {
            --addr_index;
            if(orig_state == orig_segment->tail) {
                new_segment->tail = new_state;
            }
        }
    }

    return 0;
}

static int nfa_append_tail_state(struct nfa_state_pool *state_pool, struct nfa_segment *segment) {
    if(nfa_pool_size_exceeded(state_pool, 1)) {
        return -E2BIG;
    }

    struct nfa_state *tail = nfa_state_pool_dequeue(state_pool);
    memzero(&segment->tail->edges[0], sizeof(segment->tail->edges));

    list_push_back(&segment->tail->edges[0], &tail->edges[0]);

    segment->tail = tail;

    return 0;
}

/* Malloc the state pool of the nfa */
bool nfa_init(struct nfa *nfa) {
    nfa->state_pool = malloc(NFA_STATE_POOL_SIZE * sizeof(*nfa->state_pool));
    nfa->ncaptures = 0;
    return nfa->state_pool;
}

/* Free the state pool of the nfa */
void nfa_free(struct nfa *nfa) {
    free(nfa->state_pool);
}

/* Initialize empty nfa segment
 *
 * @state_pool: pool of nfa states
 * @segment:    pointer to the segment to be initialized
 */
int nfa_init_segment(struct nfa_state_pool *state_pool, struct nfa_segment *segment, regex_capture_type capture_index) {
    if(nfa_pool_size_exceeded(state_pool, 1)) {
        return -E2BIG;
    }

    struct nfa_state *state = nfa_state_pool_dequeue(state_pool);
    segment->entry = state;
    segment->tail = state;
    segment->capture_group = capture_index;

    return 0;
}


/* Construct an atomic (single-node) nfa segment
 *
 * @state_pool: pool of nfa states
 * @segment:    the segment used to refer to the newly constructed state
 * @c:          the charcode to be stored in the state
 * @flags:      flags for the atomic state
 */
int nfa_construct_atomic_segment(struct nfa_state_pool *state_pool, struct nfa_segment *segment, regex_char c, regex_flag_type flags) {
    int status = nfa_init_segment(state_pool, segment, REGEX_CAPTURE_NONE);

    if(status < 0) {
        return status;
    }

    segment->entry->charcode = c;
    segment->entry->flags = flags;

    return 0;
}

/* Construct a non-atomic nfa segment. Consists of entry state whose charcode
 * is set to c and a tail whose charcode is unspecified
 *
 * @state_pool: pool of nfa states
 * @segment:    the segment used to refer to the newly constructed state
 * @c:          the charcode to be stored in the state
 * @flags:      flags for the atomic segment
 */
int nfa_construct_segment(struct nfa_state_pool *state_pool, struct nfa_segment *segment, regex_char c, regex_flag_type flags) {
    int status = nfa_construct_atomic_segment(state_pool, segment, c, flags);

    if(status < 0) {
        return status;
    }

    return nfa_append_tail_state(state_pool, segment);
}

/* Repeat nfa segment zero or one times (?)
 *
 *
 *      epsilon   segment
 *          |   -   |
 *          *->|B|--*
 *      -  /    -    \    -
 * --> |A| -----------*->|C|
 *      -       |         -
 *           epsilon
 *
 * @state_pool: pool of nfa states
 * @segment:    nfa_segment to be repeated (state B)
 */
int nfa_repeat_zero_one(struct nfa_state_pool *state_pool, struct nfa_segment *segment) {
    int status = 0;

    if(nfa_segment_is_atomic(segment)) {
        status = nfa_append_tail_state(state_pool, segment);

        if(status < 0) {
            return status;
        }
    }

    if(nfa_pool_size_exceeded(state_pool, 1)) {
        return -E2BIG;
    }

    /* State A */
    struct nfa_state *entry = nfa_state_pool_dequeue(state_pool);
    entry->charcode = regex_charcode_repeat_zero_one;
    memzero(&entry->edges[0], sizeof(entry->edges));

    /* State C */
    struct nfa_state *end_state = segment->tail;

    /* Link states */
    list_push_back(&entry->edges[0], &segment->entry->edges[0]);
    list_push_back(&entry->edges[1], &segment->tail->edges[0]);

    /* Update the segment */
    segment->entry = entry;
    segment->tail = end_state;

    return 0;
}


/* Repeat nfa segment one or more times (+)
 *
 *    epsilon   segment  epsilon
 *     - |     -   |   -   |   -
 * -->|A|--*->|B| --> |C| --> |D|
 *     -   |   -       -       -
 *         |           |
 *         *-----------*
 *               |
 *            epsilon
 *
 * @state_pool: pool of nfa states
 * @segment:    nfa_segment to be repeated (state B)
 */
int nfa_repeat_one_any(struct nfa_state_pool *state_pool, struct nfa_segment *segment) {
    int status = 0;

    if(nfa_segment_is_atomic(segment)) {
        status = nfa_append_tail_state(state_pool, segment);

        if(status < 0) {
            return status;
        }
    }

    if(nfa_pool_size_exceeded(state_pool, 2)) {
        return -E2BIG;
    }

    /* Unset greedy bits for closed capture groups */
    segment->entry->capture.greedy &= ~segment->entry->capture.groups;
    segment->tail->capture.greedy &= ~segment->tail->capture.groups;

    /* State A */
    struct nfa_state *entry = nfa_state_pool_dequeue(state_pool);
    entry->charcode = regex_charcode_repeat_one_any;
    memzero(&entry->edges[0], sizeof(entry->edges));

    /* State B */
    struct nfa_state *char_state = segment->entry;

    /* State C */
    struct nfa_state *split_state = segment->tail;
    split_state->charcode = regex_charcode_split;
    memzero(&split_state->edges[0], sizeof(split_state->edges));

    /* State D */
    struct nfa_state *end_state = nfa_state_pool_dequeue(state_pool);

    /* Link states */
    list_push_back(&entry->edges[0], &char_state->edges[0]);
    list_push_back(&split_state->edges[0], &end_state->edges[0]);
    list_push_back(&split_state->edges[1], &char_state->edges[0]);

    segment->entry = entry;
    segment->tail = end_state;

    return 0;
}

/* Repeat nfa segment zero or more times (*)
 *
 *          epsilon
 *      *------------*
 *      | epsilon    |
 *      -   |   -    |   -
 * -*->|A| --> |B|   *->|C|
 *  |   -       -        -
 *  |           |
 *  *-----------*
 *        |
 *     segment
 *
 * @state_pool: pool of nfa states
 * @segment:    nfa_segment to be repeated (state B)
 */
int nfa_repeat_zero_any(struct nfa_state_pool *state_pool, struct nfa_segment *segment) {
    int status = 0;

    if(nfa_segment_is_atomic(segment)) {
        status = nfa_append_tail_state(state_pool, segment);

        if(status < 0) {
            return status;
        }
    }

    if(nfa_pool_size_exceeded(state_pool, 1)) {
        return -E2BIG;
    }

    /* Unset greedy bits for closed capture groups */
    segment->entry->capture.greedy &= ~segment->entry->capture.groups;
    segment->tail->capture.greedy &= ~segment->tail->capture.groups;

    /* State A */
    struct nfa_state *entry = segment->tail;
    entry->charcode = regex_charcode_repeat_zero_any;
    memzero(&entry->edges[0], sizeof(entry->edges));

    /* State B entry */
    struct nfa_state *sb_entry = segment->entry;

    /* State D */
    struct nfa_state *end_state = nfa_state_pool_dequeue(state_pool);

    /* Link states */
    list_push_back(&entry->edges[0], &end_state->edges[0]);
    list_push_back(&entry->edges[1], &sb_entry->edges[0]);

    /* Update segment */
    segment->entry = entry;
    segment->tail = end_state;

    return 0;
}

/* Repeat nfa segment n times ({n}). Compiles down to a chain of states meaning that e.g.
 * a{3} is a shorthand for aaa.
 */
int nfa_repeat_n(struct nfa_state_pool *state_pool, struct nfa_segment *segment, unsigned n) {
    int status = 0;

    if(nfa_segment_is_atomic(segment)) {
        status = nfa_append_tail_state(state_pool, segment);

        if(status < 0) {
            return status;
        }
    }

    struct nfa_segment new_segment = {
        .tail = segment->tail
    };

    while(--n) {
        new_segment.entry = new_segment.tail;

        status = nfa_duplicate_segment(state_pool, &new_segment, segment);

        if(status < 0) {
            return status;
        }
    }

    segment->tail = new_segment.tail;
    return 0;
}

/* Repeat nfa segment 0 to m times */
static int nfa_repeat_zero_to_m(struct nfa_state_pool *state_pool, struct nfa_segment *segment, unsigned m) {
    int status = 0;

    status = nfa_repeat_zero_one(state_pool, segment);

    if(status < 0) {
        return status;
    }

    struct nfa_segment msegment = {
        .tail = segment->tail
    };

    while(--m) {
        msegment.entry = msegment.tail;

        status = nfa_duplicate_segment(state_pool, &msegment, segment);

        if(status < 0) {
            return status;
        }
    }

    segment->tail = msegment.tail;
    return 0;
}

/* Repeat nfa segment n to m times ({n,m}). Compiles does to a chain of states followed by zero-one repetitions
 * s.t. e.g. ab{2,4} is shorthand for abbb?b?
 */
int nfa_repeat_n_to_m(struct nfa_state_pool *state_pool, struct nfa_segment *segment, unsigned n, unsigned m) {
    if(!n) {
        return nfa_repeat_zero_to_m(state_pool, segment, m);
    }

    int status = 0;

    if(nfa_segment_is_atomic(segment)) {
        status = nfa_append_tail_state(state_pool, segment);

        if(status < 0) {
            return status;
        }
    }

    /* Repeat n times */
    struct nfa_segment nsegment = *segment;

    status = nfa_repeat_n(state_pool, &nsegment, n);

    if(status < 0) {
        return status;
    }

    /* m - n + 1 zero-one repetitions */
    struct nfa_segment repetition_segment;

    status = nfa_construct_atomic_segment(state_pool, &repetition_segment, (regex_char)'\0', 0);

    if(status < 0) {
        return status;
    }

    status = nfa_duplicate_segment(state_pool, &repetition_segment, segment);

    if(status < 0) {
        return status;
    }

    status = nfa_repeat_zero_one(state_pool, &repetition_segment);

    if(status < 0) {
        return status;
    }

    struct nfa_segment msegment = {
        .tail = nsegment.tail
    };

    while(n < m--) {
        msegment.entry = msegment.tail;

        status = nfa_duplicate_segment(state_pool, &msegment, &repetition_segment);

        if(status < 0) {
            return status;
        }
    }

    segment->tail = msegment.tail;

    return 0;
}

/* Repeat nfa segment at least n times. Compiles to chaing of states followed by a single zero-any repetition
 * s.t. e.g. ab{2,} is shorthand for abbb*
 */
int nfa_repeat_n_to_any(struct nfa_state_pool *state_pool, struct nfa_segment *segment, unsigned n) {
    if(!n) {
        return nfa_repeat_zero_any(state_pool, segment);
    }
    int status = 0;

    if(nfa_segment_is_atomic(segment)) {
        status = nfa_append_tail_state(state_pool, segment);

        if(status < 0) {
            return status;
        }
    }

    /* Repeat n times */
    struct nfa_segment nsegment = *segment;

    status = nfa_repeat_n(state_pool, &nsegment, n);

    if(status < 0) {
        return status;
    }

    struct nfa_segment repetition_segment;

    /* Segment for zero-any repetition */
    status = nfa_construct_atomic_segment(state_pool, &repetition_segment, (regex_char)'\0', 0);

    if(status < 0) {
        return status;
    }

    status = nfa_duplicate_segment(state_pool, &repetition_segment, segment);

    if(status < 0) {
        return status;
    }

    status = nfa_repeat_zero_any(state_pool, &repetition_segment);

    if(status < 0) {
        return status;
    }

    status = nfa_concatenate(state_pool, &nsegment.tail, &repetition_segment);

    if(status < 0) {
        return status;
    }

    /* Update segment */
    segment->tail = nsegment.tail;

    return 0;
}


/* Pass through one of two possible nfa states (|)
 *
 *        epsilon  segment0
 *           |   -   |
 *           *->|B|--*
 *      -   /    -    \    -
 * --> |A|-*           *->|D|
 *      -   \    -    /    -
 *           *->|C|--*
 *           |   -   |
 *        epsilon  segment1
 *
 * @segment0: the segment used in place of state B. The nfa_segment is updated s.t. it,
 *            when the function returns, refers to the entire alternation
 * @segment1: the segment used in place of state C
 */
int nfa_alternate(struct nfa_segment *segment0, struct nfa_segment const *segment1) {

    /* State A */
    struct nfa_state *entry = segment0->tail;
    entry->charcode = regex_charcode_alt;
    memzero(&entry->edges[0], sizeof(entry->edges));

    /* State D */
    struct nfa_state *end_state = segment1->tail;

    /* State C */
    list_init(predecessor_list);
    int npredecessors = nfa_find_predecessors(&predecessor_list, segment0->entry, segment0->tail);

    if(npredecessors < 0) {
        return npredecessors;
    }

    struct list_head *iter;
    struct list_head *hare;

    list_for_each_safe(iter, hare, &predecessor_list) {
        /* Unlink from other predecessors */
        list_clear(iter);
        /* iter is the 0th edge of the predecessor, link it to the end state */
        list_push_back(iter, &end_state->edges[0]);
    }

    /* Link states */
    list_push_back(&entry->edges[0], &segment1->entry->edges[0]);
    list_push_back(&entry->edges[1], &segment0->entry->edges[0]);

    /* Update the segment */
    segment0->entry = entry;
    segment0->tail = end_state;

    return 0;
}

/* Append atomic nfa segment, avoids having to replace states in the pool
 *
 *      segment
 *     -   |   -
 * -->|A| --> |B|
 *     -       -
 *
 * @tail:      the tail of the nfa_segment being constructed (state A)
 * @segment:   atomic nfa_segment to be inserted (in place of state A)
 */
void nfa_concatenate_atomic(struct nfa_state **tail, struct nfa_segment *segment) {

    /* State A */
    (*tail)->charcode = segment->entry->charcode;
    (*tail)->flags = segment->entry->flags | ((*tail)->flags & NFA_TAIL_MASK);
    (*tail)->capture.groups |= segment->entry->capture.groups;
    (*tail)->capture.greedy |= segment->entry->capture.greedy;
    memzero(&(*tail)->edges[0], sizeof((*tail)->edges));

    /* State B */
    struct nfa_state *end_state = segment->entry;

    /* Link states */
    list_push_back(&(*tail)->edges[0], &end_state->edges[0]);

    /* Move segment tail forward */
    *tail = end_state;
}

/* Append nfa state
 *
 *      segment
 *     -   |   -
 * -->|A| --> |B|
 *     -       -
 *
 * @state_pool: pool of nfa states
 * @tail:       the tail of the nfa_segment being constructed (state A)
 * @segment:    nfa_segment to be inserted (in place of state B)
 */
int nfa_concatenate(struct nfa_state_pool *state_pool, struct nfa_state **tail, struct nfa_segment *segment) {
    if(nfa_segment_is_atomic(segment)) {
        nfa_concatenate_atomic(tail, segment);
        return 0;
    }

    /* State A */

    segment->entry->flags |= ((*tail)->flags & NFA_TAIL_MASK);
    segment->entry->capture.groups |= ((*tail)->capture.groups);
    segment->entry->capture.greedy |= ((*tail)->capture.greedy);

    /* Replace segment->entry with *tail */
    **tail = *segment->entry;

    if(segment->entry->charcode == regex_charcode_repeat_zero_any) {
        /* Entries to zero-any repetitions loop back to themselves, make predecessors of
         * segment->entry refer to *tail instead */
        list_init(predecessor_list);

        int npredecessors = nfa_find_predecessors(&predecessor_list, nfa_follow_edge(&segment->entry->edges[1]), segment->entry);

        if(npredecessors < 0) {
            return -EFORMAT;
        }

        struct list_head *iter;
        struct list_head *hare;

        list_for_each_safe(iter, hare, &predecessor_list) {
            /* Unlink from other predecessors */
            list_clear(iter);
            /* iter is the 0th edge of the predecessor, link it to *tail */
            list_push_back(iter, &(*tail)->edges[0]);
        }
    }

    /* Place superfluous node back in queue */
    nfa_state_pool_enqueue(segment->entry, state_pool);

    /* Update segment */
    segment->entry = *tail;

    /* Move segment tail forward */
    *tail = segment->tail;

    return 0;
}

/* Make the tail state an accepting state */
void nfa_accept_state(struct nfa_state *tail) {
    tail->charcode = regex_charcode_accept;
    memzero(&tail->edges[0], sizeof(tail->edges));
}

static inline regex_flag_type nfa_epsilon_closure_late_capture_start(struct nfa_state const *state) {
    struct nfa_state const *tail = nfa_follow_edge(&state->edges[state->charcode == regex_charcode_repeat_zero_one]);

    return REGEX_FLAG_LATE_CAPTURE_START * regex_flag_test(tail->flags, REGEX_FLAG_CAPTURE_START);
}

int nfa_epsilon_closure(struct list_head *closure_list, struct nfa_state *entry, struct nfa_closure_list_node *list_node_pool, unsigned list_pool_size, regex_flag_type *flags, struct regex_capture_info *capture) {
    static_assert(SAFECMP(unsigned, NFA_STATE_POOL_SIZE, <=, QUEUE_STATIC_SIZE), "NFA state pool too large for queue");

    int closure_size = 0;

    list_clear(closure_list);

    struct nfa_state_queue_node {
        struct nfa_state *state;
        struct queue_node node;
    };

    struct nfa_state_queue_node queue_node_pool[NFA_STATE_POOL_SIZE];
    unsigned queue_pool_index = 0;
    unsigned list_pool_index = 0;

    queue_init(queue);

    struct nfa_state *state;
    struct nfa_closure_list_node *clnode;
    struct nfa_state_queue_node *qnode;

    qnode = &queue_node_pool[queue_pool_index++];

    qnode->state = entry;
    queue_enqueue(&queue, &qnode->node);

    *flags = 0;
    capture->groups = 0;
    capture->greedy = 0;

    while(queue_size(&queue)) {
        state = container(queue_dequeue(&queue), struct nfa_state_queue_node, node)->state;
        capture->groups |= state->capture.groups;
        capture->greedy |= state->capture.greedy;

        switch(state->charcode) {
            case regex_charcode_split:
                qnode = &queue_node_pool[queue_pool_index++];

                /* edge 1 */
                qnode->state = nfa_follow_edge(&state->edges[1]);
                queue_enqueue(&queue, &qnode->node);

                qnode = &queue_node_pool[queue_pool_index++];

                /* edge 0 */
                qnode->state = nfa_follow_edge(&state->edges[0]);
                queue_enqueue(&queue, &qnode->node);

                *flags |= state->flags;
                *flags |= nfa_epsilon_closure_late_capture_start(state);

                /* Skip the repetition node */
                continue;
            case regex_charcode_alt:
            case regex_charcode_repeat_zero_one:
            case regex_charcode_repeat_zero_any:
                qnode = &queue_node_pool[queue_pool_index++];

                /* edge 1 */
                qnode->state = nfa_follow_edge(&state->edges[1]);
                queue_enqueue(&queue, &qnode->node);

                *flags |= nfa_epsilon_closure_late_capture_start(state);

                /* fall through */
            case regex_charcode_repeat_one_any:
                qnode = &queue_node_pool[queue_pool_index++];

                /* edge 0 */
                qnode->state = nfa_follow_edge(&state->edges[0]);
                queue_enqueue(&queue, &qnode->node);

                *flags |= state->flags;

                /* Skip the repetition node */
                continue;
            default:
                *flags |= state->flags;
                break;
        }

        if(list_pool_index >= list_pool_size) {
            list_clear(closure_list);
            return -E2BIG;
        }

        clnode = &list_node_pool[list_pool_index++];

        clnode->state = state;
        list_clear(&clnode->list_head);
        list_push_back(closure_list, &clnode->list_head);
        ++closure_size;
    }

    *flags &= DFA_STATE_MASK;

    return closure_size;
}

bool nfa_closure_contains_accept_state(struct list_head const *closure_list) {
    bool accept = false;
    struct list_head *iter;
    struct nfa_state *state;

    list_for_each(iter, closure_list) {
        state = container(iter, struct nfa_closure_list_node, list_head)->state;
        accept |= (state->charcode == regex_charcode_accept);
    }

    return accept;
}

#ifdef TWINM_TEST_BUILD

/* Recursive function converting an nfa segment to regex */
static ssize_t nfa_segment_to_regex(char *dst, struct nfa_state *entry, struct nfa_state *exit, ssize_t n);

/* Check whether the nfa segment identified by start and end has nested nfa segments */
static bool nfa_has_nested_state(struct nfa_state *start, struct nfa_state *end) {
    bool rv = 0;

    switch(start->charcode) {
        case regex_charcode_repeat_zero_one:
            rv = start->edges[0].tail->tail != &end->edges[0];
            break;
        case regex_charcode_repeat_one_any:
            rv = start->edges[0].tail->tail->tail != &end->edges[0];
            break;
        case regex_charcode_repeat_zero_any:
            rv = start->edges[1].tail->tail != &end->edges[0];
            break;
        default:
            rv = 0;
            break;
    }

    return rv;
}

/* Check whether expression has to be enclosed in parentheses */
static inline int nfa_parens_required_for_segment(regex_char nested_state_charcode, struct nfa_state *start, struct nfa_state *end) {
    /* regex_charcode_alt always adds parens */
    return nested_state_charcode != regex_charcode_alt && nfa_has_nested_state(start, end);
}

/* Find the state in which an alternation converges */
static struct nfa_state *nfa_find_converging_state(struct nfa_state *start) {
    struct nfa_addr_node node_pool[NFA_STATE_POOL_SIZE];
    unsigned pool_index = 0;

    struct rbnode *found;
    struct list_head *iter;
    struct nfa_addr_node *node;

    rbtree_init(tree);

    list_for_each(iter, &start->edges[0]) {
        node = &node_pool[pool_index++];
        node->addr = iter;

        rbtree_insert(&tree, &node->rbnode, nfa_compare_edge_addrs);
    }

    list_for_each(iter, &start->edges[1]) {
        node = &node_pool[pool_index++];
        node->addr = iter;

        found = rbtree_find(&tree, &node->rbnode, nfa_compare_edge_addrs);
        if(found) {
            break;
        }
    }

    if(!found) {
        /* No converging state found */
        return 0;
    }

    struct list_head *edge = container(found, struct nfa_addr_node, rbnode)->addr;

    return container(edge, struct nfa_state, edges);
}

/* Find the state whose 1st edge refers to target */
static inline struct nfa_state *nfa_find_looping_state(struct nfa_state *start, struct nfa_state *target) {
    struct nfa_state *end;
    struct list_head *iter;

    list_init(list);
    list_push_back(&list, &start->edges[0]);

    list_for_each(iter, &list) {
        end = nfa_follow_edge(iter);

        if(end->edges[1].tail && nfa_follow_edge(&end->edges[1]) == target) {
            break;
        }
    }

    /* Found no link referring to target */
    if(!iter) {
        return 0;
    }

    return end;
}

/* Convert a zero-one repetition in the nfa to regex */
static ssize_t nfa_repeat_zero_one_segment_to_regex(char *dst, struct nfa_state **entry, ssize_t n) {
    char buffer[REGEX_SEGMENT_SIZE];
    ssize_t sizediff;
    ssize_t size = 0;

    struct nfa_state *start = nfa_follow_edge(&(*entry)->edges[0]);
    struct nfa_state *end = nfa_follow_edge(&(*entry)->edges[1]);

    short parens_required = nfa_parens_required_for_segment(start->charcode, *entry, end);

    if(parens_required) {
        buffer[size++] = '(';
    }

    sizediff = nfa_segment_to_regex(&buffer[size], start, end, sizeof buffer - size);

    if(sizediff < 0) {
        return sizediff;
    }

    size += sizediff;

    if(size + 2 + parens_required >= n) {
        return -E2BIG;
    }

    if(parens_required) {
        buffer[size++] = ')';
    }
    buffer[size++] = '?';
    buffer[size] = '\0';

    if(strscpy(dst, buffer, n) < 0) {
        return -E2BIG;
    }

    *entry = end;
    return size;
}

/* Convert a one-any repetition in the nfa to regex */
static ssize_t nfa_repeat_one_any_segment_to_regex(char *dst, struct nfa_state **entry, ssize_t n) {
    char buffer[REGEX_SEGMENT_SIZE];
    ssize_t sizediff;
    ssize_t size = 0;

    struct nfa_state *start = nfa_follow_edge(&(*entry)->edges[0]);
    struct nfa_state *end = nfa_find_looping_state(start, start);

    if(!end) {
        return -EFORMAT;
    }

    short parens_required = nfa_parens_required_for_segment(start->charcode, *entry, nfa_follow_edge(&end->edges[0]));

    if(parens_required) {
        buffer[size++] = '(';
    }

    sizediff = nfa_segment_to_regex(&buffer[size], start, end, n);

    if(sizediff < 0) {
        return sizediff;
    }

    size += sizediff;

    if(size + 2 + parens_required >= n) {
        return -E2BIG;
    }

    if(parens_required) {
        buffer[size++] = ')';
    }
    buffer[size++] = '+';
    buffer[size] = '\0';

    if(strscpy(dst, buffer, n) < 0) {
        return -E2BIG;
    }

    *entry = nfa_follow_edge(&end->edges[0]);
    return size;
}

/* Convert a zero-any repetition in the nfa to regex */
static ssize_t nfa_repeat_zero_any_segment_to_regex(char *dst, struct nfa_state **entry, ssize_t n) {
    char buffer[REGEX_SEGMENT_SIZE];
    ssize_t sizediff;
    ssize_t size = 0;

    struct nfa_state *start = nfa_follow_edge(&(*entry)->edges[1]);
    struct nfa_state *end = *entry;

    short parens_required = nfa_parens_required_for_segment(start->charcode, end, end);

    if(parens_required) {
        buffer[size++] = '(';
    }

    sizediff = nfa_segment_to_regex(&buffer[size], start, end, n);

    if(sizediff < 0) {
        return sizediff;
    }

    size += sizediff;

    if(size + 2 + parens_required >= n) {
        return sizediff;
    }

    if(parens_required) {
        buffer[size++] = ')';
    }
    buffer[size++] = '*';
    buffer[size] = '\0';

    if(strscpy(dst, buffer, n) < 0) {
        return -E2BIG;
    }

    *entry = nfa_follow_edge(&end->edges[0]);
    return size;
}

/* Convert an alternation segment in the nfa to regex */
static size_t nfa_alternation_segment_to_regex(char *dst, struct nfa_state **entry, ssize_t n) {
    char buffer[REGEX_SEGMENT_SIZE];
    ssize_t sizediff;
    ssize_t size = 0;

    struct nfa_state *start = *entry;
    struct nfa_state *end = nfa_find_converging_state(start);

    if(!end) {
        return -EFORMAT;
    }

    buffer[size++] = '(';

    sizediff = nfa_segment_to_regex(&buffer[size], nfa_follow_edge(&start->edges[0]), end, n - size);

    if(sizediff < 0) {
        return sizediff;
    }

    size += sizediff;

    if(size + 1 >= n) {
        return -E2BIG;
    }

    buffer[size++] = '|';

    sizediff = nfa_segment_to_regex(&buffer[size], nfa_follow_edge(&start->edges[1]), end, n - size);

    if(sizediff < 0) {
        return sizediff;
    }

    size += sizediff;

    if(size + 2 >= n) {
        return -E2BIG;
    }

    buffer[size++] = ')';
    buffer[size] = '\0';

    if(strscpy(dst, buffer, n) < 0) {
        return -E2BIG;
    }

    *entry = end;
    return size;
}

/* Convert a match-any segment in the nfa to regex */
static inline int nfa_match_any_segment_to_regex(char *dst, struct nfa_state **entry, ssize_t n) {
    if(n < 1) {
        return -E2BIG;
    }

    *dst = '.';
    *entry = nfa_follow_edge(&(*entry)->edges[0]);
    return 1;
}

static inline int nfa_digit_segment_to_regex(char *dst, struct nfa_state **entry, ssize_t n) {
    if(n < 2) {
        return -E2BIG;
    }
    dst[0] = '\\';
    dst[1] = 'd' - 32 * regex_flag_test((*entry)->flags, REGEX_FLAG_NEGATE);
    *entry = nfa_follow_edge(&(*entry)->edges[0]);
    return 2;
}

static inline int nfa_alnum_segment_to_regex(char *dst, struct nfa_state **entry, ssize_t n) {
    if(n < 2) {
        return -E2BIG;
    }
    dst[0] = '\\';
    dst[1] = 'w' - 32 * regex_flag_test((*entry)->flags, REGEX_FLAG_NEGATE);
    *entry = nfa_follow_edge(&(*entry)->edges[0]);
    return 2;
}

static inline int nfa_whitespace_segment_to_regex(char *dst, struct nfa_state **entry, ssize_t n) {
    if(n < 2) {
        return -E2BIG;
    }
    dst[0] = '\\';
    dst[1] = 's' - 32 * regex_flag_test((*entry)->flags, REGEX_FLAG_NEGATE);
    *entry = nfa_follow_edge(&(*entry)->edges[0]);
    return 2;
}

/* Convert a char segment in the nfa to regex */
static ssize_t nfa_char_segment_to_regex(char *dst, struct nfa_state **entry, ssize_t n) {
    enum { BUFFER_SIZE = 3 };

    ssize_t size = 0;
    char buffer[BUFFER_SIZE] = { 0 };

    if(regex_is_special_char((*entry)->charcode)) {
        buffer[size++] = '\\';
    }

    buffer[size++] = (*entry)->charcode;

    if(strscpy(dst, buffer, n) < 0) {
        return -E2BIG;
    }

    *entry = nfa_follow_edge(&(*entry)->edges[0]);
    return size;
}

/* Convert an nfa segment to regex */
static ssize_t nfa_segment_to_regex(char *dst, struct nfa_state *entry, struct nfa_state *exit, ssize_t n) {
    ssize_t sizediff;
    ssize_t size = 0;

    do {
        switch(entry->charcode) {
            case regex_charcode_repeat_zero_one:
                sizediff = nfa_repeat_zero_one_segment_to_regex(&dst[size], &entry, n - size);
                break;
            case regex_charcode_repeat_one_any:
                sizediff = nfa_repeat_one_any_segment_to_regex(&dst[size], &entry, n - size);
                break;
            case regex_charcode_repeat_zero_any:
                sizediff = nfa_repeat_zero_any_segment_to_regex(&dst[size], &entry,  n - size);
                break;
            case regex_charcode_alt:
                sizediff = nfa_alternation_segment_to_regex(&dst[size], &entry, n - size);
                break;
            case regex_charcode_match_any:
                sizediff = nfa_match_any_segment_to_regex(&dst[size], &entry, n - size);
                break;
            case regex_charcode_digit:
                sizediff = nfa_digit_segment_to_regex(&dst[size], &entry, n - size);
                break;
            case regex_charcode_alnum:
                sizediff = nfa_alnum_segment_to_regex(&dst[size], &entry, n - size);
                break;
            case regex_charcode_whitespace:
                sizediff = nfa_whitespace_segment_to_regex(&dst[size], &entry, n - size);
                break;
            case regex_charcode_accept:
                /* NOP */
                sizediff = 0;
                entry = nfa_follow_edge(&entry->edges[0]);
                break;
            default:
                sizediff = nfa_char_segment_to_regex(&dst[size], &entry, n - size);
                break;
        }

        if(sizediff < 0) {
            return sizediff;
        }
        size += sizediff;

    } while(entry != exit);

    return size;
}

/* Convert nfa to regex */
ssize_t nfa_to_regex(char *dst, struct nfa *nfa, size_t n) {
    struct nfa_state *end_state = nfa_follow_edge(list_end(&nfa->state_pool[0].edges[0]));
    ssize_t size = nfa_segment_to_regex(dst, &nfa->state_pool[nfa->start_index], end_state, (ssize_t)n);

    if(size < 0) {
        *dst = 0;
    }

    return size;
}
#endif /* ifdef TWINM_TEST_BUILD */
