#ifndef QUEUE_H
#define QUEUE_H

/* struct queue_node
 *
 * @unused: unused (for standard compliance)
 */
struct queue_node {
    char unused;
};

enum { QUEUE_STATIC_SIZE = 256 };

/* struct queue
 *
 * @stat:     identifier for statically allocated queue contents
 * @dynamic:  identifier for dynamically allocated queue contents
 * @start:    index of first populated slot in the queue
 * @end:      index of last populated slot in the queue
 * @capacity: current queue capacity
 */
struct queue {
    union {
        struct queue_node *stat[QUEUE_STATIC_SIZE];
        struct queue_node **dynamic;
    };
    unsigned start, size, capacity;
};

/* Create new queue
 *
 * @name: name of queue instance
 */
#define queue_init(name) \
    struct queue name = { .start = 0, .size = 0, .capacity = QUEUE_STATIC_SIZE };

/* Cleanup dynamically allocated memory (if any)
 *
 * Should be called unless guaranteed that the size of the
 * queue does never exceed QUEUE_STATIC_SIZE
 *
 * @queue: the queue instance
 */
void queue_cleanup(struct queue *queue);

/* Enqueue node
 *
 * @queue: the queue instance
 * @node:  queue_node to be pushed to queue
 */
int queue_enqueue(struct queue *queue, struct queue_node *node);

/* Dequeue and return node
 *
 * @queue: the queue instance
 */
struct queue_node *queue_dequeue(struct queue *queue);

/* Compute queue size
 *
 * @queue: the queue instance
 */
static inline unsigned queue_size(struct queue *queue) {
    return queue->size;
}

/* Flush the queue
 *
 * @queue: the queue instance
 */
static inline void queue_flush(struct queue *queue) {
    queue->start = 0;
    queue->size = 0;
}


#endif /* QUEUE_H */
