#include "rbtree.h"
#include "stack.h"

#include <string.h>

enum rbtree_direction {
    rbtree_direction_left,
    rbtree_direction_right
};

/* struct rbstack_node
 *
 * @tree_node:  node used to identify position in the tree
 * @dir:        last traversal direction
 * @stack_node: node used to identify position in the stack
 */
struct rbstack_node {
    struct rbnode *tree_node;
    enum rbtree_direction dir;
    struct stack_node stack_node;
};

/* Get rbnode stored in rbstack_node node
 *
 * @node: stack_node used to identify the corresponding rbstack_node
 */
static inline struct rbnode *rbstack_get_tree_node(struct stack_node const *node) {
    return container(node, struct rbstack_node, stack_node, const)->tree_node;
}

/* Get direction stored in rbstack_node
 *
 * @node. stack_node used to identify the corresponding rbtree_direction
 */
static inline enum rbtree_direction rbstack_get_direction(struct stack_node const *node) {
    return container(node, struct rbstack_node, stack_node, const)->dir;
}

/* Color node red
 *
 * @node: the node to be colored
 */
static inline void rbnode_make_red(struct rbnode *node) {
    node->color = rbnode_color_red;
}

/* Color node black
 *
 * @node. the node to be colored
 */
static inline void rbnode_make_black(struct rbnode *node) {
    node->color = rbnode_color_black;
}

/* Check whether node is red
 *
 * @node: the node whose color is to be checked
 */
static inline int rbnode_is_red(struct rbnode const *node) {
    return node->color == rbnode_color_red;
}

/* Like rbnode_is_red but with NULL check
 *
 * @node: the node whose color is to be checked
 */
static inline int rbnode_is_red_safe(struct rbnode const *node) {
    return node && rbnode_is_red(node);
}

/* Single rotation
 *
 * @root: root of the (sub)tree to be rotated
 * @dir:  the direction in which the (sub)tree is to be rotated
 */
static struct rbnode *rbtree_rotate_single(struct rbnode *root, enum rbtree_direction dir) {

    struct rbnode *n = root->links[!dir];

    root->links[!dir] = n->links[dir];
    n->links[dir] = root;

    rbnode_make_red(root);
    rbnode_make_black(n);

    return n;
}

/* Double rotation
 *
 * @root: root of the (sub)tree to be rotated
 * @dir:  the direction in which the (sub)tree is to be rotated
 */
static inline struct rbnode *rbtree_rotate_double(struct rbnode *root, enum rbtree_direction dir) {
    root->links[!dir] = rbtree_rotate_single(root->links[!dir], !dir);
    return rbtree_rotate_single(root, dir);
}

/* Find parent node and return it, or 0 if not found
 *
 * @tree:    the root of the tree
 * @node:    the node whose parent is to be found
 * @compare: pointer to function comparing nodes
 */
static struct rbnode *rbtree_find_parent_of(struct rbnode *tree, struct rbnode *node, int(*compare)(struct rbnode const *, struct rbnode const *)) {

    int relation;

    struct rbnode *parent = tree;
    struct rbnode *n = tree->left;

    enum rbtree_direction dir;

    while(parent && n != node) {
        relation = compare(n, node);

        dir = (enum rbtree_direction) (relation > 0);
        parent = n;
        n = n->links[dir];
    }

    return parent;
}

/* Balance (sub)tree during insertion
 *
 * @node: root of the (sub)tree
 * @dir:  direction in which the insertion took place (i.e. rbtree_direction_left would indicate
 *        that the value was inserted in the left subtree of node, and vice versa)
 */
static struct rbnode *rbtree_balance_insertion(struct rbnode *node, enum rbtree_direction dir) {
    if(rbnode_is_red_safe(node->links[dir])) {
        if(rbnode_is_red_safe(node->links[!dir])) {
            rbnode_make_red(node);
            rbnode_make_black(node->left);
            rbnode_make_black(node->right);
        }
        else {
            if(rbnode_is_red_safe(node->links[dir]->links[dir])) {
                node = rbtree_rotate_single(node, !dir);
            }
            else if(rbnode_is_red_safe(node->links[dir]->links[!dir])) {
                node = rbtree_rotate_double(node, !dir);
            }
        }
    }
    return node;
}

/* Balance (sub)tree during removal
 *
 * @n:           current node
 * @parent:      parentn node of n
 * @grandparent: grandparent node of n
 * @cdir:        next direction to follow
 */
static void rbtree_balance_removal(struct rbnode *n, struct rbnode **parent, struct rbnode **grandparent, enum rbtree_direction cdir)
{
    enum rbtree_direction pdir = (enum rbtree_direction) ((*parent)->right == n);
    enum rbtree_direction gpdir = (enum rbtree_direction) ((*grandparent)->right == *parent);

    struct rbnode *sibling = (*parent)->links[!pdir];

    if(rbnode_is_red_safe(n->links[!cdir])) {
        (*parent)->links[pdir] = rbtree_rotate_single(n, cdir);
        *grandparent = *parent;
        *parent = (*parent)->links[pdir];
    }
    else if(sibling) {
        if(rbnode_is_red_safe(sibling->left) || rbnode_is_red_safe(sibling->right)) {
            if(rbnode_is_red_safe(sibling->links[pdir])) {
                (*grandparent)->links[gpdir] = rbtree_rotate_double(*parent, pdir);
            }
            else if(rbnode_is_red_safe(sibling->links[!pdir])) {
                (*grandparent)->links[gpdir] = rbtree_rotate_single(*parent, pdir);
            }

            rbnode_make_red(n);
            rbnode_make_red((*grandparent)->links[gpdir]);
            rbnode_make_black((*grandparent)->links[gpdir]->left);
            rbnode_make_black((*grandparent)->links[gpdir]->right);

            *grandparent = (*grandparent)->links[gpdir];
        }
        else {
            rbnode_make_red(n);
            rbnode_make_red(sibling);
            rbnode_make_black(*parent);
        }
    }
}

/* Find node in the tree and return it, or return 0 when not in tree
 *
 * @tree:    root of the tree
 * @node:    pointer to node used to identify the struct holding the value to be inserted
 * @compare: pointer to function comparing two nodes
 */
struct rbnode *rbtree_find(struct rbnode const *tree, struct rbnode const *node, int(*compare)(struct rbnode const *, struct rbnode const *)) {
    int relation;
    struct rbnode *n = tree->left;

    while(n) {
        relation = compare(n, node);

        if(!relation) {
            return n;
        }
        n = n->links[relation > 0];
    }

    return n;
}

/* Insert node in the tree. Return 1 if value is inserter and 0 otherwise
 *
 * @tree:    root of the tree
 * @node:    pointer to node used to identify the struct holding the value to be inserted
 * @compare: pointer to function comparing two nodes
 */
int rbtree_insert(struct rbnode *tree, struct rbnode *node, int(*compare)(struct rbnode const *, struct rbnode const *)) {

    enum { MAX_TREE_DEPTH = 1024 };

    enum rbtree_direction dir;
    struct rbnode *child;
    int relation;

    memset(node, 0, sizeof(*node));

    if(!tree->left) {
        tree->left = node;
        rbnode_make_black(node);
        return 1;
    }

    struct rbstack_node node_pool[MAX_TREE_DEPTH];

    stack_init(stack);

    struct rbnode *n = tree->left;

    while(n) {
        relation = compare(n, node);

        if(!relation) {
            /* Value already in tree */
            stack_cleanup(&stack);
            return 0;
        }
        dir = (enum rbtree_direction) (relation > 0);

        node_pool[stack.size].tree_node = n;
        node_pool[stack.size].dir = dir;
        stack_push(&stack, &node_pool[stack.size].stack_node);
        n = n->links[dir];
    }

    rbnode_make_red(node);
    n = rbstack_get_tree_node(stack_pop(&stack));
    n->links[relation > 0] = node;

    while(stack.size) {
        child = rbtree_balance_insertion(n, dir);

        dir = rbstack_get_direction(stack_peek(&stack));
        n = rbstack_get_tree_node(stack_pop(&stack));

        n->links[dir] = child;
    }

    tree->left = rbtree_balance_insertion(n, dir);
    rbnode_make_black(tree->left);

    stack_cleanup(&stack);

    return 1;
}

/* Remove node from tree and return it. Return 0 if not found
 *
 * @tree:    root of the tree
 * @node:    pointer to node used to identify the node to be removed
 * @compare: pointer to function comparing two nodes
 */
struct rbnode *rbtree_remove(struct rbnode *tree, struct rbnode const *node, int(*compare)(struct rbnode const *, struct rbnode const *)) {
    if(!tree->left) {
        return 0;
    }

    /* Faux root allowing grandparent->left to refer to parent */
    struct rbnode faux_root = { .left = tree };

    struct rbnode *grandparent = &faux_root;
    struct rbnode *parent = tree;
    struct rbnode *n = tree->left;

    struct rbnode *found = 0;
    struct rbnode *found_parent;

    enum rbtree_direction dir = rbtree_direction_left;
    enum rbtree_direction fdir;
    enum rbtree_direction gpdir;

    int relation;

    while(n) {
        relation = compare(n, node);

        if(relation == 0) {
            found = n;
        }

        dir = (enum rbtree_direction) (relation > 0);

        if(!rbnode_is_red(n) && !rbnode_is_red_safe(n->links[dir])) {
            rbtree_balance_removal(n, &parent, &grandparent, dir);
        }

        grandparent = parent;
        parent = n;
        n = n->links[dir];
    }

    if(found) {
        /* @todo: find a more elegant way instead of traversing the
         * entire tree again */
        found_parent = rbtree_find_parent_of(tree, found, compare);

        fdir = (enum rbtree_direction) (found_parent->right == found);
        gpdir = (enum rbtree_direction) (grandparent->right == parent);

        /* Replace found with parent */
        parent->left = found->left;
        parent->right = found->right;
        parent->color = found->color;
        found_parent->links[fdir] = parent;
        grandparent->links[gpdir] = parent->links[dir];

        memset(&found->links[0], 0, sizeof(found->links));
    }

    if(tree->left) {
        rbnode_make_black(tree->left);
    }

    return found;
}
