#ifndef RBTREE_H
#define RBTREE_H

#include "macro.h"

enum rbnode_color {
    rbnode_color_black,
    rbnode_color_red
};

/* struct rbnode
 *
 * @left:     left child
 * @right:    right child
 * @links[0]: alias for left child
 * @links[1]: alias for right child
 * @color:    color of the node
 */
struct rbnode {
    union {
        #pragma pack(push, 1)
        struct { struct rbnode *left, *right; };
        #pragma pack(pop)
        struct rbnode *links[2];
    };
    enum rbnode_color color;
};

/* Create new rbtree instance
 *
 * @name: the name of the instance
 */
#define rbtree_init(name) \
    struct rbnode name = { .left = 0, .right = 0, .color = rbnode_color_black };

/* Insert node in the tree
 *
 * @tree:    root of the tree
 * @node:    pointer to node used to identify the struct holding the value to be inserted
 * @compare: pointer to function comparing two nodes
 */
int rbtree_insert(struct rbnode *tree, struct rbnode *node, int(*compare)(struct rbnode const *, struct rbnode const *));

/* Find node in the tree
 *
 * @tree:    root of the tree
 * @node:    pointer to node used to identify the struct holding the value to be inserted
 * @compare: pointer to function comparing two nodes
 */
struct rbnode *rbtree_find(struct rbnode const *tree, struct rbnode const *node, int(*compare)(struct rbnode const *, struct rbnode const *));

/* Remove node from tree and return it
 *
 * @tree:    root of the tree
 * @node:    pointer to node used to identify the node to be removed
 * @compare: pointer to function comparing two nodes
 */
struct rbnode *rbtree_remove(struct rbnode *tree, struct rbnode const *node, int(*compare)(struct rbnode const *, struct rbnode const *));

#endif /* RBTREE_H */
