#include "queue.h"
#include "twinm_types.h"

#include <stdlib.h>
#include <string.h>

/* Transition from using statically allocated stack space to dynamically allocated ditto */
static inline void queue_transition_static_to_dynamic(struct queue *queue) {
    struct queue_node **tmp = malloc(queue->capacity * sizeof(struct queue_node *));
    memcpy(tmp, queue->stat, QUEUE_STATIC_SIZE * sizeof(struct queue_node *));
    queue->dynamic = tmp;
}

/* Move data to larger dynamic array */
static inline void queue_realloc(struct queue *queue) {
    queue->dynamic = realloc(queue->dynamic, queue->capacity * sizeof(struct queue_node *));
}

/* Check whether storage for the queue has been allocated dynamically */
static inline int queue_is_dynamically_allocated(struct queue *queue) {
    return queue->capacity > QUEUE_STATIC_SIZE;
}

/* Get the initialized member of the value-holding union */
static inline struct queue_node **queue_get_union_member(struct queue *queue) {
    return queue_is_dynamically_allocated(queue) ? queue->dynamic : queue->stat;
}

/* Cleanup dynamically allocated memory (if any) */
void queue_cleanup(struct queue *queue) {
    if(queue_is_dynamically_allocated(queue)) {
        free(queue->dynamic);
    }
}

/* Enqueue node
 *
 * @queue: the queue instance
 * @node:  queue_node to be pushed to queue
 */
int queue_enqueue(struct queue *queue, struct queue_node *node) {
    if(queue_size(queue) + 1 > queue->capacity) {
        queue->capacity *= 2;

        if(queue->capacity == 2 * QUEUE_STATIC_SIZE) {
            queue_transition_static_to_dynamic(queue);
        }
        else {
            queue_realloc(queue);
        }

        if(!queue->dynamic) {
            return -EHEAP_ALLOC;
        }
    }

    queue_get_union_member(queue)[(queue->start + queue->size++) % queue->capacity] = node;

    return 0;
}

/* Dequeue and return node */
struct queue_node *queue_dequeue(struct queue *queue) {
    struct queue_node *node = queue_get_union_member(queue)[queue->start % queue->capacity];
    --queue->size;
    queue->start = (queue->start + 1) % queue->capacity;
    return node;
}
