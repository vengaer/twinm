#include "stack.h"
#include "twinm_types.h"

#include <stdlib.h>
#include <string.h>

/* Transition from using statically allocated stack space to dynamically allocated ditto */
static inline void stack_transition_static_to_dynamic(struct stack *stack) {
    struct stack_node **tmp = malloc(stack->capacity * sizeof(struct stack_node *));
    memcpy(tmp, stack->stat, STACK_STATIC_SIZE * sizeof(struct stack_node *));
    stack->dynamic = tmp;
}

/* Move data to larger dynamic array */
static inline void stack_realloc(struct stack *stack) {
    stack->dynamic = realloc(stack->dynamic, stack->capacity * sizeof(struct stack_node *));
}

/* Check whether storage for the stack has been allocated dynamically */
static inline int stack_is_dynamically_allocated(struct stack const *stack) {
    return stack->capacity > STACK_STATIC_SIZE;
}

/* Get the initialized member of the value-holding union */
static inline struct stack_node **stack_get_union_member(struct stack *stack) {
    return stack_is_dynamically_allocated(stack) ? stack->dynamic : stack->stat;
}

/* Cleanup dynamically allocated memory (if any) */
void stack_cleanup(struct stack *stack) {
    if(stack_is_dynamically_allocated(stack)) {
        free(stack->dynamic);
    }
}

/* Push node to the stack
 *
 * @stack: the stack instance
 * @node:  stack_node to be pushed to stack
 */
int stack_push(struct stack *stack, struct stack_node *node) {
    if(stack->size + 1 > stack->capacity) {
        stack->capacity *= 2;

        if(stack->capacity == 2 * STACK_STATIC_SIZE) {
            stack_transition_static_to_dynamic(stack);
        }
        else {
            stack_realloc(stack);
        }

        if(!stack->dynamic) {
            return -EHEAP_ALLOC;
        }
    }

    stack_get_union_member(stack)[stack->size++] = node;

    return 0;
}

/* Pop and return the top element of the stack */
struct stack_node *stack_pop(struct stack *stack) {
    return stack_get_union_member(stack)[--stack->size];
}

/* Peek at the top element without popping it from the stack */
struct stack_node *stack_peek(struct stack *stack) {
    return stack_get_union_member(stack)[stack->size - 1];
}
