#ifndef VECTOR_H
#define VECTOR_H

#include "macro.h"
#include "strutils.h"
#include "twinm_types.h"

#include <assert.h>
#include <stdalign.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

/* Generic, type safe vector
 *
 * Implemented as a single heap-allocated fat pointer where
 * the first 4 bytes store the capacity (excluding capacity and
 * size bytes), the subsequent 4 bytes store the size,
 * the remaining bytes (save for potential padding used to
 * align the pointer) are used for the data.
 *
 * The pointers MUST be set to 0 before the first call for any
 * of the vec macros.
 *
 * Usage:
 * {
 *     int *vec0 = 0;
 *
 *     vec_push(vec0, 1);
 *     vec_push(vec0, 2);
 *     // ...
 *     vec_free(vec0)
 *
 *     float *vec1 = 0;
 *     vec_push(vec1, 1.f);
 *     vec_push(vec1, 12.4f);
 *     // ...
 *     vec_free(vec1);
 * }
 *
 * The pointer will be automatically advanced s.t. it refers to the
 * start of the data buffer. Hence, vec_free or vec_guard must be
 * used to properly free the memory.
 */

/* Initial vector capacity */
enum { VEC_INITIAL_CAPACITY = 32 };
/* Number of bytes used for size and capacity */
enum { VEC_DATA_OFFSET = 2 * sizeof(unsigned) };

/* Get capacity of the vector, assumes the vector
 * has been allocated through calls to on of either vec_reserve,
 * vec_resize or vec_push */
#define vec_capacity(vec)   \
    *(unsigned *)((char *)(vec) - vec_align(VEC_DATA_OFFSET))

/* Get size of the vector, assumes the vector
 * has been allocated throug calls to one of either vec_reserve,
 * vec_resize, or vec_push */
#define vec_size(vec)   \
    *(unsigned *)((char *)(vec) - vec_align(VEC_DATA_OFFSET) + sizeof(unsigned))

/* Compute start offset to aligned address for data pointer */
#define vec_align(offset)   \
    (((offset) + (_Alignof(max_align_t) - 1)) & ~(_Alignof(max_align_t) - 1))

/* Internal free, used in vec_guard */
#define vec_internal_free(vec)   \
    (vec ? (vec_retreat(vec), free(vec), 1) : 1)

/* Deallocate the vector */
#define vec_free(vec)   \
    (void)vec_internal_free(vec)

/* Realloc the vector */
#define vec_realloc(vec)                    \
    ((vec ? vec_retreat(vec) : 0),          \
     !!(vec = realloc(vec, sizeof(*vec) *   \
        (vec ? 2 * ((unsigned *)vec)[0] :   \
          VEC_INITIAL_CAPACITY) +           \
        vec_align(VEC_DATA_OFFSET))))

/* Realloc the vector, ensuring it has a
 * capacity equal to the size parameter */
#define vec_realloc_fixed(vec, size)        \
    ((vec ? vec_retreat(vec) : 0),          \
     !!(vec = realloc(vec, sizeof(*vec) *   \
        size + vec_align(VEC_DATA_OFFSET))))

/* Ensure there is space for at least one more element
 * in the data buffer */
#define vec_ensure_capacity(vec)                        \
    ((vec && vec_size(vec) < vec_capacity(vec)) ||      \
     (vec_realloc(vec) &&                               \
     (vec_advance(vec),                                 \
     (vec_capacity(vec) *= 2), 1)))

/* Ensure there is space for at least size elements
 * in total */
#define vec_ensure_capacity_fixed(vec, size)    \
    ((vec && size <= vec_capacity(vec)) ||      \
     (vec_realloc_fixed(vec, size) &&           \
     (vec_advance(vec),                         \
     (vec_capacity(vec) = size), 1)))

/* Advance the vec pointer beyond the meta data */
#define vec_advance(vec)    \
    (vec = (void *)((char *)vec + vec_align(VEC_DATA_OFFSET)))

/* Retreat the vec pointer from the data buffer to
 * the start of the meta data */
#define vec_retreat(vec)    \
    (vec = (void *)((char *)vec - vec_align(VEC_DATA_OFFSET)))

/* Push value to the back of the vector */
#define vec_push(vec, value)                            \
    ((vec ? vec_ensure_capacity(vec) :                  \
       (vec_ensure_capacity(vec) &&                     \
        ((vec_capacity(vec) = VEC_INITIAL_CAPACITY),    \
         (vec_size(vec) = 0), 1))) &&                   \
     ((vec[vec_size(vec)] = (value)), 1) &&             \
     ((vec_size(vec) += 1), 1))

/* Pop the last element */
#define vec_pop(vec)            \
    do {                        \
        assert(vec);            \
        assert(vec_size(vec));  \
        --vec_size(vec);        \
    } while (0);

/* Get the last element */
#define vec_back(vec) \
    vec[vec_size(vec) - 1]

/* Ensure there is space for at least size elements
 * in the vector */
#define vec_reserve(vec, size)                      \
    ((vec ? vec_ensure_capacity_fixed(vec, size) :  \
        (vec_ensure_capacity_fixed(vec, size) &&    \
        ((vec_size(vec) = 0), 1))))

/* Resize the vector to contain size elements. If this
 * requires allocating new memory, unused bytes are set to 0 */
#define vec_resize(vec, size)                                   \
    (vec ? (vec_ensure_capacity_fixed(vec, size) &&             \
        (vec_size(vec) < size ?                                 \
            (memzero(&vec[vec_size(vec)],                       \
                (size - vec_size(vec)) * sizeof(*vec)), 1) :    \
            1) && ((vec_size(vec) = size), 1)) :                \
        (vec_ensure_capacity_fixed(vec, size) &&                \
            (memzero(&vec[0], size * sizeof(*vec)), 1) &&       \
                ((vec_size(vec) = size), 1)))

/* Internal guard overload taking both a vector and
 * pointer to int for reporting status */
#define vec_guard_overload_2(vec, status)  \
    guard(0, vec_internal_free(vec), status, -EHEAP_ALLOC)

/* Internal guard overload taking no status flag */
#define vec_guard_overload_1(vec)   \
    vec_guard_overload_2(vec, 0)

/* Automatically deallocate vector at end of scope, e.g.
 *
 * int *vec = 0;
 * int status;
 *
 * vec_guard(vec, &status) {
 *
 *     vec_push(vec, 10);
 *     // ...
 *
 * } // vec deallocated
 *
 * @vec:    pointer to vector
 * @status: (optional) pointer to int, set to non-zero on
 *          malloc failure
 */
#define vec_guard(...)  \
    overload(vec_guard_overload_, __VA_ARGS__)

#endif /* VECTOR_H */
