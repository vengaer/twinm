#ifndef LIST_H
#define LIST_H

#include "macro.h"

/* struct list_head
 *
 * @tail: the head of the list's tail
 */
struct list_head {
    struct list_head *tail;
};

/* Create new list
 *
 * @name: name of list instance
 */
#define list_init(name) \
    struct list_head name = { .tail = 0 }

/* Iterate over list. *iter must not be modified
 *
 * @iter: pointer to list_head, used as iterator
 * @list: pointer to head of the list
 */
#define list_for_each(iter, list) \
    for(iter = (list)->tail; iter; iter = iter->tail)

/* Iterate over list, *iter may be modified
 *
 * @iter: pointer to list_head, used as iterator
 * @hare: pointer to list_head, must not be modified
 * @list: pointer to head of the list
 */
#define list_for_each_safe(iter, hare, list) \
    for(iter = (list)->tail, hare = iter->tail; iter; iter = hare, hare = hare ? hare->tail : 0)

/* Iterate over 2 lists, at most min(list_size(list0), list_size(list1))
 * iterations. Neither *iter0 nor *iter1 may be modified
 *
 * @iter0: pointer to list_head used as iterator for list0
 * @iter1: pointer to list_head used as iterator for list1
 * @list0: pointer to head of the first list to iterate over
 * @list1: pointer to head of the second list to iterate over
 */
#define list_for_each_zip(iter0, iter1, list0, list1) \
    for(iter0 = (list0)->tail, iter1 = (list1)->tail; iter0 && iter1; iter0 = iter0->tail, iter1 = iter1->tail)

/* Check if list is empty
 *
 * @list: pointer to head of list
 */
static inline int list_empty(struct list_head const *list) {
    return list->tail == 0;
}

/* Find last node in list
 *
 * @list: pointer to head of the list
 */
static inline struct list_head *list_end(struct list_head *list) {
    while(list->tail) {
        list = list->tail;
    }
    return list;
}

/* Push element onto list, just after *list
 *
 * @elem: pointer to new element to be pushed to list
 * @list: pointer to node after which elem is to be inserted
 */
static inline void list_push_front(struct list_head *list, struct list_head *elem) {
    elem->tail = list->tail;
    list->tail = elem;
}

/* Push element onto the very end of the list
 *
 * @elem: pointer to new element to be pushed to list
 * @list: pointer to head of list
 */
static inline void list_push_back(struct list_head *list, struct list_head *elem) {
    struct list_head *end = list_end(list);
    end->tail = elem;
}

/* Extract node from list
 *
 * @list: pointer to node whose tail is to be extracted
 */
static inline struct list_head *list_extract(struct list_head *list) {
    struct list_head *extracted = list->tail;
    list->tail = extracted->tail;
    extracted->tail = 0;

    return extracted;
}

/* Get size of list
 *
 * @list: pointer to head of list
 */
static inline unsigned list_size(struct list_head const *list) {
    unsigned size = 0;
    while(list->tail) {
        size++;
        list = list->tail;
    }
    return size;
}

/* Clear the list
 *
 * @list: pointer to head of list
 */
static inline void list_clear(struct list_head *list) {
    list->tail = 0;
}

static inline struct list_head *list_find(struct list_head *list, void const *data, int(*compare)(struct list_head const *node, void const *data)) {
    struct list_head *iter;
    list_for_each(iter, list) {
        if(compare(iter, data) == 0) {
            break;
        }
    }
    return iter;
}

#endif /* LIST_H */
