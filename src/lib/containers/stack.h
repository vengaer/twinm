#ifndef STACK_H
#define STACK_H

/* struct stack_node
 *
 * @unused: unused (required for standard compliance)
 */
struct stack_node {
    char unused;
};

enum { STACK_STATIC_SIZE = 100 };

/* struct stack
 *
 * @stat:     identifier for statically allocated stack contents
 * @dynamic:  identifier for dynamically allocated stack contents
 * @size:     current stack size
 * @capacity: current stack capacity
 */
struct stack {
    union {
        struct stack_node *stat[STACK_STATIC_SIZE];
        struct stack_node **dynamic;
    };
    unsigned size, capacity;
};

/* Create new stack
 *
 * @name: name of stack instance
 */
#define stack_init(name) \
    struct stack name = { .size = 0, .capacity = STACK_STATIC_SIZE };

/* Cleanup dynamically allocated memory (if any)
 *
 * Should be called unless guaranteed that the size of the
 * stack does never exceed STACK_STATIC_SIZE
 *
 * @stack: the stack instance
 */
void stack_cleanup(struct stack *stack);

/* Push node to the stack
 *
 * @stack: the stack instance
 * @node:  stack_node to be pushed to stack
 */
int stack_push(struct stack *stack, struct stack_node *node);

/* Pop and return the top element of the stack
 *
 * @stack: the stack instance
 */
struct stack_node *stack_pop(struct stack *stack);

/* Peek at the top element without popping it from the stack
 *
 * @stack: the stack instance
 */
struct stack_node *stack_peek(struct stack *stack);

/* Get size of the stack
 *
 * @stack: the stack instance
 */
static inline unsigned stack_size(struct stack const *stack) {
    return stack->size;
}

static inline void stack_lazy_init(struct stack *stack) {
    stack->size = 0;
    stack->capacity = STACK_STATIC_SIZE;
}

#endif /* STACK_H */
