    section .rodata
    avx:            db "simd_support:=avx",0xa
    avx_len:        equ $-avx
    sse:            db "simd_support:=sse",0xa
    sse_len:        equ $-sse
    unknown:        db "simd_support:=",0xa
    unknown_len:    equ $-unknown
    no_file:        db "No filename supplied",0xa
    no_file_len:    equ $-no_file

    section .text

    global _start

_start:
.argc       equ 0
.argv1      equ 16

    mov     r9, rbx                     ; preserve rbx
    mov     r8d, 1                      ; exit status in r8d

    mov     r10d, dword [rsp + .argc]   ; argc to r10d

    cmp     r10d, 1                     ; require filename on command line
    jle     .no_file

    xor     r8d, r8d                    ; exit status 0 for now
    mov     r11, qword [rsp + .argv1]   ; argv[1] to r11

.avx:
    mov     eax, 7
    xor     ecx, ecx
    cpuid

    test    ebx, 0x20                   ; AVX2 flag
    jz      .sse

    lea     rbx, [rel avx]              ; string to rbx
    mov     r10, avx_len                ; length to r10

    jmp     .open_file

.sse:
    mov     eax, 1
    xor     ecx, ecx
    cpuid

    test    ecx, 0x100000               ; SSE4.2 flag
    jz      .unknown

    lea     rbx, [rel sse]              ; string to rbx
    mov     r10, sse_len                ; length to r10

    jmp     .open_file

.unknown:
    lea     rbx, [rel unknown]          ; string to rbx
    mov     r10, unknown_len            ; length to r10

    inc     r8d                         ; exit status 1

.open_file:
    mov     eax, 2                      ; open syscall
    mov     rdi, r11                    ; filename to rdi
    mov     esi, 0102o                  ; O_CREAT
    mov     edx, 0644o                  ; mode 644
    syscall

    cmp     rax, 0                      ; open failed?
    jg      .prep_write
    inc     r8d                         ; ensure exit status != 0
    jmp     .epi

.prep_write:
    mov     rdi, rax                    ; file handle to rdi

    mov     rsi, rbx                    ; string to rsi
    mov     rdx, r10                    ; length to rdx

    jmp     .write

.no_file:
    lea     rsi, [rel no_file]          ; string to rsi
    mov     rdx, no_file_len            ; length to rdx

    xor     edi, edi                    ; no file to close

    mov     edi, 1                      ; stdout

.write:
    mov     eax, 1                      ; syscall write
    syscall

    cmp     rdi, 0                      ; file has to be closed?
    je      .epi

    mov     eax, 3                      ; close syscall
    syscall

.epi:
    mov     rbx, r9                     ; restore rbx
    mov     eax, 60                     ; syscall exit
    mov     edi, r8d                    ; exit status
    syscall
