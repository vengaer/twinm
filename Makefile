CC               ?= gcc
AS               ?= yasm
LD               ?= ld

TARGET           ?= twinm
TEST_TARGET      ?= $(TARGET)_test

SIMD             := y

QUIET            := @
MKDIR            := mkdir -p
RM               := rm -rf
UNAME            := uname -s
UNAME_ARCH       := uname -m
TOUCH            := touch

cflags           := -std=c11 -Wall -Wextra -Wpedantic -Waggregate-return -Wbad-function-cast \
                    -Wcast-qual -Wfloat-equal -Wmissing-include-dirs -Wnested-externs -Wpointer-arith \
                    -Wredundant-decls -Wshadow -Wunknown-pragmas -Wswitch -Wundef -Wunused -Wwrite-strings \
                    -MD -MP -c

cppflags         :=

asflags          := -felf64

ldflags          :=
ldlibs           := -lm

root             := $(abspath $(CURDIR))

builddir         := $(root)/build
srcdir           := $(root)/src

cext             := c
asext            := asm
oext             := o
depext           := d

objs             :=

default_language := c

# $(call host-arch, host_os)
define host-arch
$(strip
  $(if $(findstring Windows,$(1)),
      $(if $(or $(findstring AMD64,$(PROCESSOR_ARCHITEW6432)), $(findstring AMD64,$(PROCESSOR_ARCHITECTURE))),
          x86_64,
        $(if $(findstring x86,$(PROCESSOR_ARCHITECTURE)),
            x86)),
    $(shell $(UNAME_ARCH))))
endef

# $(call host-os)
define host-os
$(strip
  $(if $(findstring Windows_NT,$(OS)),
      Windows,
    $(shell $(UNAME))))
endef

# $(call mk-module-build-dir)
define mk-module-build-dir
$(shell $(MKDIR) $(patsubst $(srcdir)/%,$(builddir)/%,$(module_path)))
endef

# $(call mk-module-build-subdir, dirname)
define mk-module-build-subdir
$(shell $(MKDIR) $(patsubst $(srcdir)/%,$(builddir)/%,$(module_path))/$(1))
endef

# $(call stack-top, stack_name)
define stack-top
$(subst :,,$(firstword $($(1))))
endef

# $(call stack-push, stack_name, value)
define stack-push
$(eval $(1) := $(stack_top_symb)$(strip $(2)) $(subst $(stack_top_symb),,$($(1))))
endef

# $(call stack-pop, stack_name)
define stack-pop
$(eval __top := $(call stack-top,$(1)))
$(eval $(1) := $(stack_top_symb)$(subst $(stack_top_symb)$(__top) ,,$($(1))))
endef

# $(call include-module-prologue, module_name)
define include-module-prologue
$(eval module_name := $(strip $(1)))
$(call stack-push,module_stack,$(module_name))
$(call stack-push,trivial_stack,$(trivial_module))
$(eval module_path := $(module_path)/$(module_name))
$(call mk-module-build-dir)
$(eval module_language := $(default_language))
$(eval trivial_module := n)
endef

# $(call include-module-epilogue)
define include-module-epilogue
$(if $(findstring y,$(trivial_module)),
    $(call declare-trivial-$(module_language)-module))
$(eval trivial_module := $(call stack-top,trivial_stack))
$(eval module_path := $(patsubst %/$(module_name),%,$(module_path)))
$(call stack-pop,module_stack)
$(call stack-pop,trivial_stack)
$(eval module_name := $(call stack-top,module_stack))
endef

# $(call include-module, module_path)
define include-module
$(call include-module-prologue,$(1))
$(eval include $(module_path)/$(module_mk))
$(call include-module-epilogue)
endef

# $(call dirname, path)
define dirname
$(lastword $(subst /, ,$(1)))
endef

# $(call calling-convention)
define calling-convention
$(strip
    $(if $(findstring x86_64,$(host_arch)),
        $(if $(findstring Windows,$(host_os)),microsoft_x64,
            systemv_amd64),
      unknown))
endef

# $(call add-prepare-dep, dependency)
define add-prepare-dep
$(eval prepare_deps += $(1))
endef

# $(call add-build-dep, dependency)
define add-build-dep
$(eval build_deps += $(1))
endef

# $(call add-target-dep, dependency)
define add-target-dep
$(eval target_deps += $(1))
endef

# $(call module-src-to-obj, srcext)
define module-src-to-obj
$(patsubst $(srcdir)/%,$(builddir)/%,$($(module_name)_src:.$(1)=.$(oext)))
endef

# $(call prepend-build-root, filename)
define prepend-build-root
$(builddir)/$(1)
endef

# $(call invoke-unary, binary, arg)
define invoke-unary
$(QUIET)$(1) $(2)
endef

# $(call query-simd-support)
define query-simd-support
$(strip
  $(if $(and $(findstring y,$(SIMD)),
             $(findstring x86_64,$(host_arch)),
             $(or $(findstring Linux,$(host_os)),$(findstring Windows,$(host_os)))),
      y))
endef

# $(call build-configuration)
define build-configuration
$(strip
  $(if $(MAKECMDGOALS),
      $(if $(or $(findstring all,$(MAKECMDGOALS)), $(findstring $(TARGET),$(MAKECMDGOALS))),
          core,
        $(if $(or $(findstring test,$(MAKECMDGOALS)), $(findstring $(TEST_TARGET),$(MAKECMDGOALS))),
            test,
          $(if $(findstring release,$(MAKECMDGOALS)),
              release))),
    core))
endef

# $(call set-build-defines)
define set-build-defines
$(if $(findstring release,$(configuration)),
    $(eval cppflags += -DTWINM_RELEASE_BUILD),
  $(if $(findstring test,$(configuration)),
      $(eval cppflags += -DTWINM_TEST_BUILD)))

$(if $(findstring avx,$(simd_support)),
    $(eval cppflags += -DTWINM_SIMD_AVX)
  $(if $(findstring sse,$(simd_support)),
      $(eval cppflags += -DTWINM_SIMD_SSE)))

$(if $(findstring Linux,$(host_os)),
    $(eval cppflags += -DTWINM_HOST_LINUX),
  $(if $(findstring Windows,$(host_os)),
      $(eval cppflags += -DTWINM_HOST_WINDOWS),
    $(error Unable to detect host OS)))

$(if $(findstring x86_64,$(host_arch)),
    $(eval cppflags += -DTWINM_HOST_ARCH_X64),
  $(if $(findstring x86,$(host_arch)),
      $(eval cppflags += -DTWINM_HOST_ARCH_X86),
    $(error Unable to detect host architecture)))
endef

# $(call set-build-libs)
define set-build-libs
$(if $(findstring Linux,$(host_os)),
    $(eval ldlibs += -lpthread))
endef

# $(call set-configuration-specific-flags)
define set-configuration-specific-flags
$(if $(findstring release,$(configuration)),
    $(eval cppflags += -DNDEBUG)
    $(eval cflags   += -O3),
  $(eval cflags   += -g)
  $(if $(findstring yasm,$(AS)),
      $(eval asflags  += -g dwarf2)))
endef

# $(call override-implicit-vars)
define override-implicit-vars
$(eval override CFLAGS   += $(cflags))
$(eval override CPPFLAGS += $(cppflags))
$(eval override ASFLAGS  += $(asflags))
$(eval override LDFLAGS  += $(ldflags))
$(eval override LDLIBS   += $(ldlibs))
endef

# $(call compile-core)
define compile-core
$(strip
  $(if $(or $(findstring core,$(configuration)),
            $(findstring release,$(configuration))),
      y))
endef

# $(call declare-trivial-c-module)
define declare-trivial-c-module
$(eval __src    := $(wildcard $(module_path)/*.$(cext)))
$(eval objs     += $(patsubst $(srcdir)/%,$(builddir)/%,$(__src:.$(cext)=.$(oext))))
$(eval cppflags += -I$(module_path))
endef

# $(call declare-trivial-asm-module)
define declare-trivial-asm-module
$(eval __srcdir := $(calling_convention))
$(call mk-module-build-subdir,$(__srcdir))
$(if $(findstring test,$(configuration)),,
    $(eval __wildcard_prefix := $(simd_support)))
$(eval __src := $(wildcard $(module_path)/$(__srcdir)/$(__wildcard_prefix)*.$(asext)))
$(eval objs  += $(patsubst $(srcdir)/%,$(builddir)/%,$(__src:.$(asext)=.$(oext))))
endef

# $(call echo-build-step, program, file)
define echo-build-step
$(info [$(1)] $(notdir $(2)))
endef

# $(call echo-as, file)
define echo-as
$(call echo-build-step,AS,$(1))
endef

# $(call echo-cc, file)
define echo-cc
$(call echo-build-step,CC,$(1))
endef

# $(call echo-ld, file)
define echo-ld
$(call echo-build-step,LD,$(1))
endef

mk-build-root      := $(shell $(MKDIR) $(builddir))

host_os            := $(call host-os)
host_arch          := $(call host-arch, $(host_os))
calling_convention := $(call calling-convention)

simd_cache         := $(builddir)/simd_cache.mk

configuration      := $(call build-configuration)

module_mk          := Makefile
module_path        := $(root)

stack_top_symb     := :

prepare_deps       :=
build_deps         :=
target_deps        :=

prepare            := $(builddir)/.prepare.stamp

$(call mk-build-root)
$(call module-stack-push,$(call dirname,$(root)))
$(call set-configuration-specific-flags)

.PHONY: all
all: $(TARGET)

ifneq ($(configuration),)
  $(call include-module,src)
endif

$(call set-build-defines)
$(call set-build-libs)
$(call override-implicit-vars)

$(prepare): $(prepare_deps)
	$(QUIET)$(TOUCH) $@

$(TARGET): $(target_deps) $(objs) | $(prepare)
	$(call echo-ld,$@)
	$(QUIET)$(CC) -o $@ $^ $(LDFLAGS) $(LDLIBS)

$(TEST_TARGET): $(target_deps) $(objs) | $(prepare)
	$(call echo-ld,$@)
	$(QUIET)$(CC) -o $@ $^ $(LDFLAGS) $(LDLIBS)

$(builddir)/%.$(oext): $(srcdir)/%.$(cext) $(build_deps)
	$(call echo-cc,$@)
	$(QUIET)$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ $<

$(builddir)/%.$(oext): $(srcdir)/%.$(asext) $(build_deps)
	$(call echo-as,$@)
	$(QUIET)$(AS) $(ASFLAGS) -o $@ $<

.PHONY: prepare
prepare: $(prepare)

.PHONY: test
test: $(TEST_TARGET)

.PHONY: release
release: $(TARGET)

.PHONY: clean
clean:
	$(QUIET)$(RM) $(builddir)
	$(QUIET)$(RM) $(TARGET)
	$(QUIET)$(RM) $(TEST_TARGET)

.PHONY: test_asan
test_asan: CFLAGS += -fsanitize=address
test_asan: LDLIBS := -lasan $(LDLIBS)
test_asan: $(TEST_TARGET)

.PHONY: test_ubsan
test_ubsan: CFLAGS += -fsanitize=undefined
test_ubsan: LDLIBS := -lubsan $(LDLIBS)
test_ubsan: $(TEST_TARGET)

-include $(objs:.$(oext)=.$(depext))
